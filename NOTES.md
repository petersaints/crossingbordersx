To Do List
================================================================================
* Put it online -- In Progress
* Multiple Gallery Layouts (up to 2 by 2) -- DONE
* Add "Dropdowns" to Table Field Type - DONE
* Test all supported data widget types -- DONE
  * Boolean   -- OK
  * Choice    -- OK
  * Date      -- OK
  * Date Time -- OK
  * Time      -- OK
  * Float     -- OK
  * Integer   -- OK
  * String    -- OK
  * Text      -- OK
  * Table     -- OK
  * Record    -- OK
  * File      -- OK
  * Image     -- OK
  * Video     -- OK
* Audio     -- OK
* Test all search/gallery scenarios -- Mostly OK (I guess)
  * Simple Search -- OK
  * Advanced Search
    * Shown Fields - DONE
    * Search Options - DONE
  * Simple Gallery -- OK
  * Advanced Gallery -- OK
* User Guide -- DONE
* Paginate favorites -- DONE
* User management -- DONE
* Saving searches and galleries as reports to be easily retrievable later for reviewing and printing -- DONE
* Add comments to the records -- DONE
* Print support -- DONE
* Export to PDF -- DONE

Future Ideas that may be implemented (preferably by someone else)
================================================================================
* Milestone: 1.x (most likely 1.1 or 1.2 if these improvements are phased)
  * More advanced search options such as sorting
    * NOTE that due to limitation in the way that is structure (the entity-attribute-value model) the best that can be done is to sort the results by one of attributes of the searched record type. I'm not saying that it is impossible to due more advanced searching than that, what I'm saying is that it requires way more work and it is probably not worth it.
    * Full-text indexing