-- phpMyAdmin SQL Dump
-- version 4.2.6deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 04, 2014 at 10:46 PM
-- Server version: 5.5.40-0ubuntu1
-- PHP Version: 5.5.12-2ubuntu4.1

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `crossingbordersx`
--
CREATE DATABASE IF NOT EXISTS `crossingbordersx` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `crossingbordersx`;

-- --------------------------------------------------------

--
-- Table structure for table `acl_classes`
--
-- Creation: Oct 26, 2014 at 08:31 PM
--

DROP TABLE IF EXISTS `acl_classes`;
CREATE TABLE IF NOT EXISTS `acl_classes` (
`id` int(10) unsigned NOT NULL,
  `class_type` varchar(200) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `acl_classes`
--

INSERT INTO `acl_classes` (`id`, `class_type`) VALUES
(2, 'CrossingBorders\\XBundle\\Entity\\Comment'),
(1, 'CrossingBorders\\XBundle\\Entity\\Thread'),
(3, 'CrossingBorders\\XBundle\\Entity\\Vote');

-- --------------------------------------------------------

--
-- Table structure for table `acl_entries`
--
-- Creation: Oct 26, 2014 at 08:31 PM
--

DROP TABLE IF EXISTS `acl_entries`;
CREATE TABLE IF NOT EXISTS `acl_entries` (
`id` int(10) unsigned NOT NULL,
  `class_id` int(10) unsigned NOT NULL,
  `object_identity_id` int(10) unsigned DEFAULT NULL,
  `security_identity_id` int(10) unsigned NOT NULL,
  `field_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ace_order` smallint(5) unsigned NOT NULL,
  `mask` int(11) NOT NULL,
  `granting` tinyint(1) NOT NULL,
  `granting_strategy` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `audit_success` tinyint(1) NOT NULL,
  `audit_failure` tinyint(1) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=17 ;

--
-- RELATIONS FOR TABLE `acl_entries`:
--   `object_identity_id`
--       `acl_object_identities` -> `id`
--   `security_identity_id`
--       `acl_security_identities` -> `id`
--   `class_id`
--       `acl_classes` -> `id`
--

--
-- Dumping data for table `acl_entries`
--

INSERT INTO `acl_entries` (`id`, `class_id`, `object_identity_id`, `security_identity_id`, `field_name`, `ace_order`, `mask`, `granting`, `granting_strategy`, `audit_success`, `audit_failure`) VALUES
(1, 1, NULL, 1, NULL, 0, 3, 1, 'all', 0, 0),
(2, 1, NULL, 2, NULL, 1, 3, 1, 'all', 0, 0),
(3, 1, NULL, 3, NULL, 2, 1073741823, 1, 'all', 0, 0),
(4, 2, NULL, 1, NULL, 0, 3, 1, 'all', 0, 0),
(5, 2, NULL, 2, NULL, 1, 1, 1, 'all', 0, 0),
(6, 2, NULL, 4, NULL, 2, 1073741823, 1, 'all', 0, 0),
(7, 2, NULL, 3, NULL, 3, 1073741823, 1, 'all', 0, 0),
(8, 3, NULL, 1, NULL, 0, 3, 1, 'all', 0, 0),
(9, 3, NULL, 2, NULL, 1, 3, 1, 'all', 0, 0),
(10, 3, NULL, 3, NULL, 2, 1073741823, 1, 'all', 0, 0),
(11, 2, 5, 5, NULL, 0, 128, 1, 'all', 0, 0),
(12, 2, 7, 5, NULL, 0, 128, 1, 'all', 0, 0),
(13, 2, 9, 5, NULL, 0, 128, 1, 'all', 0, 0),
(14, 2, 10, 5, NULL, 0, 128, 1, 'all', 0, 0),
(15, 2, 12, 5, NULL, 0, 128, 1, 'all', 0, 0),
(16, 2, 13, 5, NULL, 0, 128, 1, 'all', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `acl_object_identities`
--
-- Creation: Oct 26, 2014 at 08:31 PM
--

DROP TABLE IF EXISTS `acl_object_identities`;
CREATE TABLE IF NOT EXISTS `acl_object_identities` (
`id` int(10) unsigned NOT NULL,
  `parent_object_identity_id` int(10) unsigned DEFAULT NULL,
  `class_id` int(10) unsigned NOT NULL,
  `object_identifier` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `entries_inheriting` tinyint(1) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

--
-- RELATIONS FOR TABLE `acl_object_identities`:
--   `parent_object_identity_id`
--       `acl_object_identities` -> `id`
--

--
-- Dumping data for table `acl_object_identities`
--

INSERT INTO `acl_object_identities` (`id`, `parent_object_identity_id`, `class_id`, `object_identifier`, `entries_inheriting`) VALUES
(1, NULL, 1, 'class', 1),
(2, NULL, 2, 'class', 1),
(3, NULL, 3, 'class', 1),
(4, NULL, 1, 'record1290', 1),
(5, NULL, 2, '9', 1),
(6, NULL, 1, 'record2571', 1),
(7, NULL, 2, '10', 1),
(8, NULL, 1, 'record2572', 1),
(9, NULL, 2, '11', 1),
(10, NULL, 2, '12', 1),
(11, NULL, 1, 'record2573', 1),
(12, NULL, 2, '13', 1),
(13, NULL, 2, '14', 1),
(14, NULL, 1, 'record3856', 1),
(15, NULL, 1, 'record5142', 1);

-- --------------------------------------------------------

--
-- Table structure for table `acl_object_identity_ancestors`
--
-- Creation: Oct 26, 2014 at 08:31 PM
--

DROP TABLE IF EXISTS `acl_object_identity_ancestors`;
CREATE TABLE IF NOT EXISTS `acl_object_identity_ancestors` (
  `object_identity_id` int(10) unsigned NOT NULL,
  `ancestor_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `acl_object_identity_ancestors`:
--   `object_identity_id`
--       `acl_object_identities` -> `id`
--   `ancestor_id`
--       `acl_object_identities` -> `id`
--

--
-- Dumping data for table `acl_object_identity_ancestors`
--

INSERT INTO `acl_object_identity_ancestors` (`object_identity_id`, `ancestor_id`) VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 4),
(5, 5),
(6, 6),
(7, 7),
(8, 8),
(9, 9),
(10, 10),
(11, 11),
(12, 12),
(13, 13),
(14, 14),
(15, 15);

-- --------------------------------------------------------

--
-- Table structure for table `acl_security_identities`
--
-- Creation: Oct 26, 2014 at 08:31 PM
--

DROP TABLE IF EXISTS `acl_security_identities`;
CREATE TABLE IF NOT EXISTS `acl_security_identities` (
`id` int(10) unsigned NOT NULL,
  `identifier` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `username` tinyint(1) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `acl_security_identities`
--

INSERT INTO `acl_security_identities` (`id`, `identifier`, `username`) VALUES
(5, 'CrossingBorders\\SecurityBundle\\Entity\\User-admin', 1),
(2, 'IS_AUTHENTICATED_ANONYMOUSLY', 0),
(4, 'ROLE_ADMIN', 0),
(3, 'ROLE_SUPER_ADMIN', 0),
(1, 'ROLE_USER', 0);

-- --------------------------------------------------------

--
-- Table structure for table `Choice`
--
-- Creation: Oct 26, 2014 at 08:31 PM
--

DROP TABLE IF EXISTS `Choice`;
CREATE TABLE IF NOT EXISTS `Choice` (
`id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `displayOrder` int(11) NOT NULL,
  `recordTypeField_id` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- RELATIONS FOR TABLE `Choice`:
--   `recordTypeField_id`
--       `RecordTypeField` -> `id`
--

--
-- Dumping data for table `Choice`
--

INSERT INTO `Choice` (`id`, `name`, `displayOrder`, `recordTypeField_id`) VALUES
(4, 'Option 1', 0, 33),
(5, 'Option 2', 1, 33),
(6, 'Option 3', 2, 33),
(7, 'Option 4', 3, 33);

-- --------------------------------------------------------

--
-- Table structure for table `Comment`
--
-- Creation: Oct 26, 2014 at 08:31 PM
--

DROP TABLE IF EXISTS `Comment`;
CREATE TABLE IF NOT EXISTS `Comment` (
`id` int(11) NOT NULL,
  `thread_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `author_id` int(11) DEFAULT NULL,
  `body` longtext COLLATE utf8_unicode_ci NOT NULL,
  `ancestors` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `depth` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `state` int(11) NOT NULL,
  `score` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- RELATIONS FOR TABLE `Comment`:
--   `thread_id`
--       `Thread` -> `id`
--   `author_id`
--       `fos_user` -> `id`
--

-- --------------------------------------------------------

--
-- Table structure for table `Favorite`
--
-- Creation: Oct 26, 2014 at 08:31 PM
--

DROP TABLE IF EXISTS `Favorite`;
CREATE TABLE IF NOT EXISTS `Favorite` (
`id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- RELATIONS FOR TABLE `Favorite`:
--   `user_id`
--       `fos_user` -> `id`
--   `type_id`
--       `FavoriteType` -> `id`
--

--
-- Dumping data for table `Favorite`
--

INSERT INTO `Favorite` (`id`, `user_id`, `type_id`, `name`, `url`) VALUES
(3, 21, 21, 'Muahaha', '/record/page');

-- --------------------------------------------------------

--
-- Table structure for table `FavoriteType`
--
-- Creation: Oct 26, 2014 at 08:31 PM
--

DROP TABLE IF EXISTS `FavoriteType`;
CREATE TABLE IF NOT EXISTS `FavoriteType` (
`id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=26 ;

--
-- Dumping data for table `FavoriteType`
--

INSERT INTO `FavoriteType` (`id`, `name`) VALUES
(25, 'Advanced Gallery'),
(23, 'Advanced Search'),
(21, 'Generic'),
(24, 'Simple Gallery'),
(22, 'Simple Search');

-- --------------------------------------------------------

--
-- Table structure for table `FieldType`
--
-- Creation: Oct 26, 2014 at 08:31 PM
--

DROP TABLE IF EXISTS `FieldType`;
CREATE TABLE IF NOT EXISTS `FieldType` (
`id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `versionNumber` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=76 ;

--
-- Dumping data for table `FieldType`
--

INSERT INTO `FieldType` (`id`, `created`, `updated`, `versionNumber`, `name`) VALUES
(61, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Boolean'),
(62, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Choice'),
(63, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Date'),
(64, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Date Time'),
(65, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Float'),
(66, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Integer'),
(67, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record'),
(68, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'String'),
(69, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Text'),
(70, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Time'),
(71, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Table'),
(72, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'File'),
(73, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Image'),
(74, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Video'),
(75, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Audio');

-- --------------------------------------------------------

--
-- Table structure for table `FieldValue`
--
-- Creation: Oct 26, 2014 at 08:31 PM
--

DROP TABLE IF EXISTS `FieldValue`;
CREATE TABLE IF NOT EXISTS `FieldValue` (
`id` int(11) NOT NULL,
  `record_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `versionNumber` int(11) NOT NULL,
  `recordTypeField_id` int(11) NOT NULL,
  `discr` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1451 ;

--
-- RELATIONS FOR TABLE `FieldValue`:
--   `record_id`
--       `Record` -> `id`
--   `recordTypeField_id`
--       `RecordTypeField` -> `id`
--

--
-- Dumping data for table `FieldValue`
--

INSERT INTO `FieldValue` (`id`, `record_id`, `created`, `updated`, `versionNumber`, `recordTypeField_id`, `discr`) VALUES
(1166, 5143, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1167, 5144, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1168, 5145, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1169, 5146, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1170, 5147, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1171, 5148, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1172, 5149, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1173, 5150, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1174, 5151, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1175, 5152, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1176, 5153, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1177, 5154, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1178, 5155, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1179, 5156, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1180, 5157, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1181, 5158, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1182, 5159, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1183, 5160, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1184, 5161, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1185, 5162, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1186, 5163, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1187, 5164, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1188, 5165, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1189, 5166, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1190, 5167, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1191, 5168, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1192, 5169, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1193, 5170, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1194, 5171, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1195, 5172, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1196, 5173, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1197, 5174, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1198, 5175, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1199, 5176, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1200, 5177, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1201, 5178, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1202, 5179, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1203, 5180, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1204, 5181, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1205, 5182, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1206, 5183, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1207, 5184, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1208, 5185, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1209, 5186, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1210, 5187, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1211, 5188, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1212, 5189, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1213, 5190, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1214, 5191, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1215, 5192, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1216, 5193, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1217, 5194, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1218, 5195, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1219, 5196, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1220, 5197, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1221, 5198, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1222, 5199, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1223, 5200, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1224, 5201, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1225, 5202, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1226, 5203, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1227, 5204, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1228, 5205, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1229, 5206, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1230, 5207, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1231, 5208, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1232, 5209, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1233, 5210, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1234, 5211, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1235, 5212, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1236, 5213, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1237, 5214, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1238, 5215, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1239, 5216, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1240, 5217, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1241, 5218, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1242, 5219, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1243, 5220, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1244, 5221, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1245, 5222, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1246, 5223, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1247, 5224, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1248, 5225, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1249, 5226, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1250, 5227, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1251, 5228, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1252, 5229, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1253, 5230, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1254, 5231, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1255, 5232, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1256, 5233, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1257, 5234, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1258, 5235, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1259, 5236, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1260, 5237, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1261, 5238, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1262, 5239, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1263, 5240, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1264, 5241, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1265, 5242, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1266, 5243, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1267, 5244, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1268, 5245, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1269, 5246, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1270, 5247, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1271, 5248, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1272, 5249, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1273, 5250, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1274, 5251, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1275, 5252, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1276, 5253, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1277, 5254, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1278, 5255, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1279, 5256, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1280, 5257, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1281, 5258, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1282, 5259, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1283, 5260, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1284, 5261, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1285, 5262, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1286, 5263, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1287, 5264, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1288, 5265, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1289, 5266, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1290, 5267, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1291, 5268, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1292, 5269, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1293, 5270, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1294, 5271, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1295, 5272, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1296, 5273, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1297, 5274, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1298, 5275, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1299, 5276, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1300, 5277, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1301, 5278, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1302, 5279, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1303, 5280, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1304, 5281, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1305, 5282, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1306, 5283, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1307, 5284, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1308, 5285, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1309, 5286, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1310, 5287, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1311, 5288, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1312, 5289, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1313, 5290, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1314, 5291, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1315, 5292, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1316, 5293, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1317, 5294, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1318, 5295, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1319, 5296, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1320, 5297, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1321, 5298, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1322, 5299, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1323, 5300, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1324, 5301, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1325, 5302, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1326, 5303, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1327, 5304, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1328, 5305, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1329, 5306, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1330, 5307, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1331, 5308, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1332, 5309, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1333, 5310, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1334, 5311, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1335, 5312, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1336, 5313, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1337, 5314, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1338, 5315, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1339, 5316, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1340, 5317, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1341, 5318, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1342, 5319, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1343, 5320, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1344, 5321, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1345, 5322, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1346, 5323, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1347, 5324, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1348, 5325, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1349, 5326, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1350, 5327, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1351, 5328, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1352, 5329, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1353, 5330, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1354, 5331, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1355, 5332, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1356, 5333, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1357, 5334, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1358, 5335, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1359, 5336, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1360, 5337, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1361, 5338, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1362, 5339, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1363, 5340, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1364, 5341, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1365, 5342, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1366, 5343, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1367, 5344, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1368, 5345, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1369, 5346, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1370, 5347, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1371, 5348, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1372, 5349, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1373, 5350, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1374, 5351, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1375, 5352, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1376, 5353, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1377, 5354, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1378, 5355, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1379, 5356, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1380, 5357, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1381, 5358, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1382, 5359, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1383, 5360, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1384, 5361, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1385, 5362, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1386, 5363, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1387, 5364, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1388, 5365, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1389, 5366, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1390, 5367, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1391, 5368, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1392, 5369, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1393, 5370, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1394, 5371, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1395, 5372, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1396, 5373, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1397, 5374, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1398, 5375, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1399, 5376, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1400, 5377, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1401, 5378, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1402, 5379, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1403, 5380, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1404, 5381, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1405, 5382, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1406, 5383, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1407, 5384, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1408, 5385, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1409, 5386, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1410, 5387, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 30, 'String'),
(1411, 5388, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 31, 'File'),
(1412, 5389, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 31, 'File'),
(1413, 5390, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 31, 'File'),
(1414, 5391, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 31, 'File'),
(1415, 5392, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 31, 'File'),
(1416, 5393, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 31, 'File'),
(1417, 5394, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 31, 'File'),
(1418, 5395, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 31, 'File'),
(1419, 5396, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 31, 'File'),
(1420, 5397, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 31, 'File'),
(1421, 5398, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 31, 'File'),
(1422, 5399, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 31, 'File'),
(1423, 5400, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 31, 'File'),
(1424, 5401, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 31, 'File'),
(1425, 5402, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 31, 'File'),
(1426, 5403, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 31, 'File'),
(1427, 5404, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 31, 'File'),
(1428, 5405, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 31, 'File'),
(1429, 5406, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 31, 'File'),
(1430, 5407, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 31, 'File'),
(1431, 5408, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 31, 'File'),
(1432, 5409, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 31, 'File'),
(1433, 5410, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 31, 'File'),
(1434, 5411, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 31, 'File'),
(1435, 5412, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 31, 'File'),
(1436, 5413, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 31, 'File'),
(1437, 5414, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 31, 'File'),
(1438, 5415, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 31, 'File'),
(1439, 5416, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 31, 'File'),
(1440, 5417, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 31, 'File'),
(1441, 5418, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 31, 'File'),
(1442, 5419, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 31, 'File'),
(1443, 5420, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 31, 'File'),
(1444, 5421, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 31, 'File'),
(1445, 5422, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 31, 'File'),
(1446, 5423, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 31, 'File'),
(1447, 5424, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 31, 'File'),
(1448, 5425, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 31, 'File'),
(1449, 5426, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 31, 'File'),
(1450, 5427, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 31, 'File');

-- --------------------------------------------------------

--
-- Table structure for table `FieldValueBoolean`
--
-- Creation: Oct 26, 2014 at 08:31 PM
--

DROP TABLE IF EXISTS `FieldValueBoolean`;
CREATE TABLE IF NOT EXISTS `FieldValueBoolean` (
  `id` int(11) NOT NULL,
  `value` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `FieldValueBoolean`:
--   `id`
--       `FieldValue` -> `id`
--

-- --------------------------------------------------------

--
-- Table structure for table `FieldValueChoice`
--
-- Creation: Oct 26, 2014 at 08:31 PM
--

DROP TABLE IF EXISTS `FieldValueChoice`;
CREATE TABLE IF NOT EXISTS `FieldValueChoice` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `FieldValueChoice`:
--   `id`
--       `FieldValue` -> `id`
--

-- --------------------------------------------------------

--
-- Table structure for table `fieldvaluechoice_choice`
--
-- Creation: Oct 26, 2014 at 08:31 PM
--

DROP TABLE IF EXISTS `fieldvaluechoice_choice`;
CREATE TABLE IF NOT EXISTS `fieldvaluechoice_choice` (
  `fieldvaluechoice_id` int(11) NOT NULL,
  `choice_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `fieldvaluechoice_choice`:
--   `choice_id`
--       `Choice` -> `id`
--   `fieldvaluechoice_id`
--       `FieldValueChoice` -> `id`
--

-- --------------------------------------------------------

--
-- Table structure for table `FieldValueDate`
--
-- Creation: Oct 26, 2014 at 08:31 PM
--

DROP TABLE IF EXISTS `FieldValueDate`;
CREATE TABLE IF NOT EXISTS `FieldValueDate` (
  `id` int(11) NOT NULL,
  `value` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `FieldValueDate`:
--   `id`
--       `FieldValue` -> `id`
--

-- --------------------------------------------------------

--
-- Table structure for table `FieldValueDateTime`
--
-- Creation: Oct 26, 2014 at 08:31 PM
--

DROP TABLE IF EXISTS `FieldValueDateTime`;
CREATE TABLE IF NOT EXISTS `FieldValueDateTime` (
  `id` int(11) NOT NULL,
  `value` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `FieldValueDateTime`:
--   `id`
--       `FieldValue` -> `id`
--

-- --------------------------------------------------------

--
-- Table structure for table `FieldValueFile`
--
-- Creation: Oct 26, 2014 at 08:31 PM
--

DROP TABLE IF EXISTS `FieldValueFile`;
CREATE TABLE IF NOT EXISTS `FieldValueFile` (
  `id` int(11) NOT NULL,
  `storedFile_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `FieldValueFile`:
--   `storedFile_id`
--       `StoredFile` -> `id`
--   `id`
--       `FieldValue` -> `id`
--

--
-- Dumping data for table `FieldValueFile`
--

INSERT INTO `FieldValueFile` (`id`, `storedFile_id`) VALUES
(1411, 165),
(1412, 166),
(1413, 167),
(1414, 168),
(1415, 169),
(1416, 170),
(1417, 171),
(1418, 172),
(1419, 173),
(1420, 174),
(1421, 175),
(1422, 176),
(1423, 177),
(1424, 178),
(1425, 179),
(1426, 180),
(1427, 181),
(1428, 182),
(1429, 183),
(1430, 184),
(1431, 185),
(1432, 186),
(1433, 187),
(1434, 188),
(1435, 189),
(1436, 190),
(1437, 191),
(1438, 192),
(1439, 193),
(1440, 194),
(1441, 195),
(1442, 196),
(1443, 197),
(1444, 198),
(1445, 199),
(1446, 200),
(1447, 201),
(1448, 202),
(1449, 203),
(1450, 204);

-- --------------------------------------------------------

--
-- Table structure for table `FieldValueFileGallery`
--
-- Creation: Oct 26, 2014 at 08:31 PM
--

DROP TABLE IF EXISTS `FieldValueFileGallery`;
CREATE TABLE IF NOT EXISTS `FieldValueFileGallery` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `FieldValueFileGallery`:
--   `id`
--       `FieldValue` -> `id`
--

-- --------------------------------------------------------

--
-- Table structure for table `fieldvaluefilegallery_storedfile`
--
-- Creation: Oct 26, 2014 at 08:31 PM
--

DROP TABLE IF EXISTS `fieldvaluefilegallery_storedfile`;
CREATE TABLE IF NOT EXISTS `fieldvaluefilegallery_storedfile` (
  `fieldvaluefilegallery_id` int(11) NOT NULL,
  `storedfile_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `fieldvaluefilegallery_storedfile`:
--   `storedfile_id`
--       `StoredFile` -> `id`
--   `fieldvaluefilegallery_id`
--       `FieldValueFileGallery` -> `id`
--

-- --------------------------------------------------------

--
-- Table structure for table `FieldValueFloat`
--
-- Creation: Oct 26, 2014 at 08:31 PM
--

DROP TABLE IF EXISTS `FieldValueFloat`;
CREATE TABLE IF NOT EXISTS `FieldValueFloat` (
  `id` int(11) NOT NULL,
  `value` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `FieldValueFloat`:
--   `id`
--       `FieldValue` -> `id`
--

-- --------------------------------------------------------

--
-- Table structure for table `FieldValueInteger`
--
-- Creation: Oct 26, 2014 at 08:31 PM
--

DROP TABLE IF EXISTS `FieldValueInteger`;
CREATE TABLE IF NOT EXISTS `FieldValueInteger` (
  `id` int(11) NOT NULL,
  `value` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `FieldValueInteger`:
--   `id`
--       `FieldValue` -> `id`
--

-- --------------------------------------------------------

--
-- Table structure for table `FieldValueRecords`
--
-- Creation: Oct 26, 2014 at 08:31 PM
--

DROP TABLE IF EXISTS `FieldValueRecords`;
CREATE TABLE IF NOT EXISTS `FieldValueRecords` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `FieldValueRecords`:
--   `id`
--       `FieldValue` -> `id`
--

-- --------------------------------------------------------

--
-- Table structure for table `fieldvaluerecords_record`
--
-- Creation: Oct 26, 2014 at 08:31 PM
--

DROP TABLE IF EXISTS `fieldvaluerecords_record`;
CREATE TABLE IF NOT EXISTS `fieldvaluerecords_record` (
  `fieldvaluerecords_id` int(11) NOT NULL,
  `record_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `fieldvaluerecords_record`:
--   `fieldvaluerecords_id`
--       `FieldValueRecords` -> `id`
--   `record_id`
--       `Record` -> `id`
--

-- --------------------------------------------------------

--
-- Table structure for table `FieldValueString`
--
-- Creation: Oct 26, 2014 at 08:31 PM
--

DROP TABLE IF EXISTS `FieldValueString`;
CREATE TABLE IF NOT EXISTS `FieldValueString` (
  `id` int(11) NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `FieldValueString`:
--   `id`
--       `FieldValue` -> `id`
--

--
-- Dumping data for table `FieldValueString`
--

INSERT INTO `FieldValueString` (`id`, `value`) VALUES
(1166, 'AF'),
(1167, 'AX'),
(1168, 'AL'),
(1169, 'DZ'),
(1170, 'AS'),
(1171, 'AD'),
(1172, 'AO'),
(1173, 'AI'),
(1174, 'AQ'),
(1175, 'AG'),
(1176, 'AR'),
(1177, 'AM'),
(1178, 'AW'),
(1179, 'AU'),
(1180, 'AT'),
(1181, 'AZ'),
(1182, 'BS'),
(1183, 'BH'),
(1184, 'BD'),
(1185, 'BB'),
(1186, 'BY'),
(1187, 'BE'),
(1188, 'BZ'),
(1189, 'BJ'),
(1190, 'BM'),
(1191, 'BT'),
(1192, 'BO'),
(1193, 'BA'),
(1194, 'BW'),
(1195, 'BV'),
(1196, 'BR'),
(1197, 'IO'),
(1198, 'BN'),
(1199, 'BG'),
(1200, 'BF'),
(1201, 'BI'),
(1202, 'KH'),
(1203, 'CM'),
(1204, 'CA'),
(1205, 'CV'),
(1206, 'KY'),
(1207, 'CF'),
(1208, 'TD'),
(1209, 'CL'),
(1210, 'CN'),
(1211, 'CX'),
(1212, 'CC'),
(1213, 'CO'),
(1214, 'KM'),
(1215, 'CG'),
(1216, 'CD'),
(1217, 'CK'),
(1218, 'CR'),
(1219, 'CI'),
(1220, 'HR'),
(1221, 'CU'),
(1222, 'CY'),
(1223, 'CZ'),
(1224, 'DK'),
(1225, 'DJ'),
(1226, 'DM'),
(1227, 'DO'),
(1228, 'EC'),
(1229, 'EG'),
(1230, 'SV'),
(1231, 'GQ'),
(1232, 'ER'),
(1233, 'EE'),
(1234, 'ET'),
(1235, 'FK'),
(1236, 'FO'),
(1237, 'FJ'),
(1238, 'FI'),
(1239, 'FR'),
(1240, 'GF'),
(1241, 'PF'),
(1242, 'TF'),
(1243, 'GA'),
(1244, 'GM'),
(1245, 'GE'),
(1246, 'DE'),
(1247, 'GH'),
(1248, 'GI'),
(1249, 'GR'),
(1250, 'GL'),
(1251, 'GD'),
(1252, 'GP'),
(1253, 'GU'),
(1254, 'GT'),
(1255, 'GG'),
(1256, 'GN'),
(1257, 'GW'),
(1258, 'GY'),
(1259, 'HT'),
(1260, 'HM'),
(1261, 'VA'),
(1262, 'HN'),
(1263, 'HK'),
(1264, 'HU'),
(1265, 'IS'),
(1266, 'IN'),
(1267, 'ID'),
(1268, 'IR'),
(1269, 'IQ'),
(1270, 'IE'),
(1271, 'IM'),
(1272, 'IL'),
(1273, 'IT'),
(1274, 'JM'),
(1275, 'JP'),
(1276, 'JE'),
(1277, 'JO'),
(1278, 'KZ'),
(1279, 'KE'),
(1280, 'KI'),
(1281, 'KR'),
(1282, 'KW'),
(1283, 'KG'),
(1284, 'LA'),
(1285, 'LV'),
(1286, 'LB'),
(1287, 'LS'),
(1288, 'LR'),
(1289, 'LY'),
(1290, 'LI'),
(1291, 'LT'),
(1292, 'LU'),
(1293, 'MO'),
(1294, 'MK'),
(1295, 'MG'),
(1296, 'MW'),
(1297, 'MY'),
(1298, 'MV'),
(1299, 'ML'),
(1300, 'MT'),
(1301, 'MH'),
(1302, 'MQ'),
(1303, 'MR'),
(1304, 'MU'),
(1305, 'YT'),
(1306, 'MX'),
(1307, 'FM'),
(1308, 'MD'),
(1309, 'MC'),
(1310, 'MN'),
(1311, 'ME'),
(1312, 'MS'),
(1313, 'MA'),
(1314, 'MZ'),
(1315, 'MM'),
(1316, 'NA'),
(1317, 'NR'),
(1318, 'NP'),
(1319, 'NL'),
(1320, 'AN'),
(1321, 'NC'),
(1322, 'NZ'),
(1323, 'NI'),
(1324, 'NE'),
(1325, 'NG'),
(1326, 'NU'),
(1327, 'NF'),
(1328, 'MP'),
(1329, 'NO'),
(1330, 'OM'),
(1331, 'PK'),
(1332, 'PW'),
(1333, 'PS'),
(1334, 'PA'),
(1335, 'PG'),
(1336, 'PY'),
(1337, 'PE'),
(1338, 'PH'),
(1339, 'PN'),
(1340, 'PL'),
(1341, 'PT'),
(1342, 'PR'),
(1343, 'QA'),
(1344, 'RE'),
(1345, 'RO'),
(1346, 'RU'),
(1347, 'RW'),
(1348, 'BL'),
(1349, 'SH'),
(1350, 'KN'),
(1351, 'LC'),
(1352, 'MF'),
(1353, 'PM'),
(1354, 'VC'),
(1355, 'WS'),
(1356, 'SM'),
(1357, 'ST'),
(1358, 'SA'),
(1359, 'SN'),
(1360, 'RS'),
(1361, 'SC'),
(1362, 'SL'),
(1363, 'SG'),
(1364, 'SK'),
(1365, 'SI'),
(1366, 'SB'),
(1367, 'SO'),
(1368, 'ZA'),
(1369, 'GS'),
(1370, 'ES'),
(1371, 'LK'),
(1372, 'SD'),
(1373, 'SR'),
(1374, 'SJ'),
(1375, 'SZ'),
(1376, 'SE'),
(1377, 'CH'),
(1378, 'SY'),
(1379, 'TW'),
(1380, 'TJ'),
(1381, 'TZ'),
(1382, 'TH'),
(1383, 'TL'),
(1384, 'TG'),
(1385, 'TK'),
(1386, 'TO'),
(1387, 'TT'),
(1388, 'TN'),
(1389, 'TR'),
(1390, 'TM'),
(1391, 'TC'),
(1392, 'TV'),
(1393, 'UG'),
(1394, 'UA'),
(1395, 'AE'),
(1396, 'GB'),
(1397, 'US'),
(1398, 'UM'),
(1399, 'UY'),
(1400, 'UZ'),
(1401, 'VU'),
(1402, 'VE'),
(1403, 'VN'),
(1404, 'VG'),
(1405, 'VI'),
(1406, 'WF'),
(1407, 'EH'),
(1408, 'YE'),
(1409, 'ZM'),
(1410, 'ZW');

-- --------------------------------------------------------

--
-- Table structure for table `FieldValueTable`
--
-- Creation: Oct 26, 2014 at 08:31 PM
--

DROP TABLE IF EXISTS `FieldValueTable`;
CREATE TABLE IF NOT EXISTS `FieldValueTable` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `FieldValueTable`:
--   `id`
--       `FieldValue` -> `id`
--

-- --------------------------------------------------------

--
-- Table structure for table `FieldValueText`
--
-- Creation: Oct 26, 2014 at 08:31 PM
--

DROP TABLE IF EXISTS `FieldValueText`;
CREATE TABLE IF NOT EXISTS `FieldValueText` (
  `id` int(11) NOT NULL,
  `value` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `FieldValueText`:
--   `id`
--       `FieldValue` -> `id`
--

-- --------------------------------------------------------

--
-- Table structure for table `FieldValueTime`
--
-- Creation: Oct 26, 2014 at 08:31 PM
--

DROP TABLE IF EXISTS `FieldValueTime`;
CREATE TABLE IF NOT EXISTS `FieldValueTime` (
  `id` int(11) NOT NULL,
  `value` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `FieldValueTime`:
--   `id`
--       `FieldValue` -> `id`
--

-- --------------------------------------------------------

--
-- Table structure for table `fos_group`
--
-- Creation: Oct 26, 2014 at 08:31 PM
--

DROP TABLE IF EXISTS `fos_group`;
CREATE TABLE IF NOT EXISTS `fos_group` (
`id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=24 ;

--
-- Dumping data for table `fos_group`
--

INSERT INTO `fos_group` (`id`, `name`, `roles`) VALUES
(21, 'User', 'a:2:{i:0;s:9:"ROLE_USER";i:1;s:11:"ROLE_VIEWER";}'),
(22, 'Editor', 'a:3:{i:0;s:9:"ROLE_USER";i:1;s:11:"ROLE_VIEWER";i:2;s:11:"ROLE_EDITOR";}'),
(23, 'Administrator', 'a:5:{i:0;s:9:"ROLE_USER";i:1;s:11:"ROLE_VIEWER";i:2;s:11:"ROLE_EDITOR";i:3;s:10:"ROLE_ADMIN";i:4;s:16:"ROLE_SUPER_ADMIN";}');

-- --------------------------------------------------------

--
-- Table structure for table `fos_user`
--
-- Creation: Oct 26, 2014 at 08:31 PM
--

DROP TABLE IF EXISTS `fos_user`;
CREATE TABLE IF NOT EXISTS `fos_user` (
`id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=23 ;

--
-- Dumping data for table `fos_user`
--

INSERT INTO `fos_user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `locked`, `expired`, `expires_at`, `confirmation_token`, `password_requested_at`, `roles`, `credentials_expired`, `credentials_expire_at`) VALUES
(17, 'user', 'user', 'user@fct.unl.pt', 'user@fct.unl.pt', 1, '4kmz487p3gcgo0sookgg0c08wkgogsk', 'cCBgtQHH2kEQJnF+kz2FaH+375wbXHLIbIZDQt3RkWKZaYRZEfqrz6A3xgXJN0DcAzJGkPmCrQYVa8ncM7VaXg==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL),
(18, 'user2', 'user2', 'user2@fct.unl.pt', 'user2@fct.unl.pt', 1, 'iryqtsfra6g4c0o8wcksoc4ooso4k0', '7lENjQuTJnBs067bnAgpjAkfey+xI91SWWjH9Jh+ZoR+pvnLdVypVlYA1wkVOq5GOpwsGp1SZH0ro2gLVgPeSw==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL),
(19, 'editor', 'editor', 'editor@fct.unl.pt', 'editor@fct.unl.pt', 1, '84sjovxel6o0ggw0g88kw88og4kk4kg', 'CHBprYB6XhKwbZTKt2h9+3YqDPet55hVNZhc5k5VTMyheuDpgx5R5TuzIMVmxVR2tS5AMyWstuCxOxvhntjDTw==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL),
(20, 'editor2', 'editor2', 'editor2@fct.unl.pt', 'editor2@fct.unl.pt', 1, 'mcwgzxyfry84wwsgs84cgg0w4kcsgsk', 't8CWwromRjFhlTTvK2svkVLgjqqLP6K2nyMdiZgd+PbfkxgsSX/S3kvCHkucXxa6p2XmZMBsss8uH7QOO8HtFg==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL),
(21, 'admin', 'admin', 'admin@fct.unl.pt', 'admin@fct.unl.pt', 1, '2r39j3q4uakggcwc0o4ww84gw8gk084', '8fkS5/AtiCAPynarfV9ToUQEhCD/Jnuchp/7ExOGqLROZA7X38aFVAIsX7GtyFwDu5cIUvK/Y5rThRNIJ3ZzkQ==', '2014-11-04 19:14:45', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL),
(22, 'admin2', 'admin2', 'admin2@fct.unl.pt', 'admin2@fct.unl.pt', 1, 'tvdhs3qkti8kwwwgo0cocgswckkskso', 'bmVKum9qG/HnLfYHDnezZI4px6GHAbNetCkNEdLtFcvxpicMp72zCjD2GXI4eEzXWiyWRJSY4rccVhbSQKydwQ==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fos_user_user_group`
--
-- Creation: Oct 26, 2014 at 08:31 PM
--

DROP TABLE IF EXISTS `fos_user_user_group`;
CREATE TABLE IF NOT EXISTS `fos_user_user_group` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `fos_user_user_group`:
--   `user_id`
--       `fos_user` -> `id`
--   `group_id`
--       `fos_group` -> `id`
--

--
-- Dumping data for table `fos_user_user_group`
--

INSERT INTO `fos_user_user_group` (`user_id`, `group_id`) VALUES
(17, 21),
(18, 21),
(19, 22),
(20, 22),
(21, 23),
(22, 23);

-- --------------------------------------------------------

--
-- Table structure for table `Record`
--
-- Creation: Oct 26, 2014 at 08:31 PM
--

DROP TABLE IF EXISTS `Record`;
CREATE TABLE IF NOT EXISTS `Record` (
`id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `versionNumber` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `recordType_id` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6428 ;

--
-- RELATIONS FOR TABLE `Record`:
--   `recordType_id`
--       `RecordType` -> `id`
--

--
-- Dumping data for table `Record`
--

INSERT INTO `Record` (`id`, `created`, `updated`, `versionNumber`, `name`, `recordType_id`) VALUES
(5143, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Afghanistan', 170),
(5144, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Aland Islands', 170),
(5145, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Albania', 170),
(5146, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Algeria', 170),
(5147, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'American Samoa', 170),
(5148, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Andorra', 170),
(5149, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Angola', 170),
(5150, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Anguilla', 170),
(5151, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Antarctica', 170),
(5152, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Antigua And Barbuda', 170),
(5153, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Argentina', 170),
(5154, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Armenia', 170),
(5155, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Aruba', 170),
(5156, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Australia', 170),
(5157, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Austria', 170),
(5158, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Azerbaijan', 170),
(5159, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Bahamas', 170),
(5160, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Bahrain', 170),
(5161, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Bangladesh', 170),
(5162, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Barbados', 170),
(5163, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Belarus', 170),
(5164, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Belgium', 170),
(5165, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Belize', 170),
(5166, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Benin', 170),
(5167, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Bermuda', 170),
(5168, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Bhutan', 170),
(5169, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Bolivia', 170),
(5170, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Bosnia And Herzegovina', 170),
(5171, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Botswana', 170),
(5172, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Bouvet Island', 170),
(5173, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Brazil', 170),
(5174, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'British Indian Ocean Territory', 170),
(5175, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Brunei Darussalam', 170),
(5176, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Bulgaria', 170),
(5177, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Burkina Faso', 170),
(5178, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Burundi', 170),
(5179, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Cambodia', 170),
(5180, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Cameroon', 170),
(5181, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Canada', 170),
(5182, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Cape Verde', 170),
(5183, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Cayman Islands', 170),
(5184, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Central African Republic', 170),
(5185, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Chad', 170),
(5186, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Chile', 170),
(5187, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'China', 170),
(5188, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Christmas Island', 170),
(5189, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Cocos (Keeling) Islands', 170),
(5190, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Colombia', 170),
(5191, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Comoros', 170),
(5192, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Congo', 170),
(5193, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Congo, Democratic Republic', 170),
(5194, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Cook Islands', 170),
(5195, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Costa Rica', 170),
(5196, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Cote D''Ivoire', 170),
(5197, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Croatia', 170),
(5198, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Cuba', 170),
(5199, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Cyprus', 170),
(5200, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Czech Republic', 170),
(5201, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Denmark', 170),
(5202, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Djibouti', 170),
(5203, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Dominica', 170),
(5204, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Dominican Republic', 170),
(5205, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Ecuador', 170),
(5206, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Egypt', 170),
(5207, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'El Salvador', 170),
(5208, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Equatorial Guinea', 170),
(5209, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Eritrea', 170),
(5210, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Estonia', 170),
(5211, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Ethiopia', 170),
(5212, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Falkland Islands (Malvinas)', 170),
(5213, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Faroe Islands', 170),
(5214, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Fiji', 170),
(5215, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Finland', 170),
(5216, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'France', 170),
(5217, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'French Guiana', 170),
(5218, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'French Polynesia', 170),
(5219, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'French Southern Territories', 170),
(5220, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Gabon', 170),
(5221, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Gambia', 170),
(5222, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Georgia', 170),
(5223, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Germany', 170),
(5224, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Ghana', 170),
(5225, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Gibraltar', 170),
(5226, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Greece', 170),
(5227, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Greenland', 170),
(5228, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Grenada', 170),
(5229, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Guadeloupe', 170),
(5230, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Guam', 170),
(5231, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Guatemala', 170),
(5232, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Guernsey', 170),
(5233, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Guinea', 170),
(5234, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Guinea-Bissau', 170),
(5235, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Guyana', 170),
(5236, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Haiti', 170),
(5237, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Heard Island & Mcdonald Islands', 170),
(5238, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Holy See (Vatican City State)', 170),
(5239, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Honduras', 170),
(5240, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Hong Kong', 170),
(5241, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Hungary', 170),
(5242, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Iceland', 170),
(5243, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'India', 170),
(5244, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Indonesia', 170),
(5245, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Iran, Islamic Republic Of', 170),
(5246, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Iraq', 170),
(5247, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Ireland', 170),
(5248, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Isle Of Man', 170),
(5249, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Israel', 170),
(5250, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Italy', 170),
(5251, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Jamaica', 170),
(5252, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Japan', 170),
(5253, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Jersey', 170),
(5254, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Jordan', 170),
(5255, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Kazakhstan', 170),
(5256, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Kenya', 170),
(5257, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Kiribati', 170),
(5258, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Korea', 170),
(5259, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Kuwait', 170),
(5260, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Kyrgyzstan', 170),
(5261, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Lao People''s Democratic Republic', 170),
(5262, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Latvia', 170),
(5263, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Lebanon', 170),
(5264, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Lesotho', 170),
(5265, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Liberia', 170),
(5266, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Libyan Arab Jamahiriya', 170),
(5267, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Liechtenstein', 170),
(5268, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Lithuania', 170),
(5269, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Luxembourg', 170),
(5270, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Macao', 170),
(5271, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Macedonia', 170),
(5272, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Madagascar', 170),
(5273, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Malawi', 170),
(5274, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Malaysia', 170),
(5275, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Maldives', 170),
(5276, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Mali', 170),
(5277, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Malta', 170),
(5278, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Marshall Islands', 170),
(5279, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Martinique', 170),
(5280, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Mauritania', 170),
(5281, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Mauritius', 170),
(5282, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Mayotte', 170),
(5283, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Mexico', 170),
(5284, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Micronesia, Federated States Of', 170),
(5285, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Moldova', 170),
(5286, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Monaco', 170),
(5287, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Mongolia', 170),
(5288, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Montenegro', 170),
(5289, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Montserrat', 170),
(5290, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Morocco', 170),
(5291, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Mozambique', 170),
(5292, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Myanmar', 170),
(5293, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Namibia', 170),
(5294, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Nauru', 170),
(5295, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Nepal', 170),
(5296, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Netherlands', 170),
(5297, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Netherlands Antilles', 170),
(5298, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'New Caledonia', 170),
(5299, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'New Zealand', 170),
(5300, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Nicaragua', 170),
(5301, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Niger', 170),
(5302, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Nigeria', 170),
(5303, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Niue', 170),
(5304, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Norfolk Island', 170),
(5305, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Northern Mariana Islands', 170),
(5306, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Norway', 170),
(5307, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Oman', 170),
(5308, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Pakistan', 170),
(5309, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Palau', 170),
(5310, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Palestinian Territory, Occupied', 170),
(5311, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Panama', 170),
(5312, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Papua New Guinea', 170),
(5313, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Paraguay', 170),
(5314, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Peru', 170),
(5315, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Philippines', 170),
(5316, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Pitcairn', 170),
(5317, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Poland', 170),
(5318, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Portugal', 170),
(5319, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Puerto Rico', 170),
(5320, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Qatar', 170),
(5321, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Reunion', 170),
(5322, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Romania', 170),
(5323, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Russian Federation', 170),
(5324, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Rwanda', 170),
(5325, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Saint Barthelemy', 170),
(5326, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Saint Helena', 170),
(5327, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Saint Kitts And Nevis', 170),
(5328, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Saint Lucia', 170),
(5329, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Saint Martin', 170),
(5330, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Saint Pierre And Miquelon', 170),
(5331, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Saint Vincent And Grenadines', 170),
(5332, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Samoa', 170),
(5333, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'San Marino', 170),
(5334, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Sao Tome And Principe', 170),
(5335, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Saudi Arabia', 170),
(5336, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Senegal', 170),
(5337, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Serbia', 170),
(5338, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Seychelles', 170),
(5339, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Sierra Leone', 170),
(5340, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Singapore', 170),
(5341, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Slovakia', 170),
(5342, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Slovenia', 170),
(5343, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Solomon Islands', 170),
(5344, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Somalia', 170),
(5345, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'South Africa', 170),
(5346, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'South Georgia And Sandwich Isl.', 170),
(5347, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Spain', 170),
(5348, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Sri Lanka', 170),
(5349, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Sudan', 170),
(5350, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Suriname', 170),
(5351, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Svalbard And Jan Mayen', 170),
(5352, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Swaziland', 170),
(5353, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Sweden', 170),
(5354, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Switzerland', 170),
(5355, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Syrian Arab Republic', 170),
(5356, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Taiwan', 170),
(5357, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Tajikistan', 170),
(5358, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Tanzania', 170),
(5359, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Thailand', 170),
(5360, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Timor-Leste', 170),
(5361, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Togo', 170),
(5362, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Tokelau', 170),
(5363, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Tonga', 170),
(5364, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Trinidad And Tobago', 170),
(5365, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Tunisia', 170),
(5366, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Turkey', 170),
(5367, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Turkmenistan', 170),
(5368, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Turks And Caicos Islands', 170),
(5369, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Tuvalu', 170),
(5370, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Uganda', 170),
(5371, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Ukraine', 170),
(5372, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'United Arab Emirates', 170),
(5373, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'United Kingdom', 170),
(5374, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'United States', 170),
(5375, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'United States Outlying Islands', 170),
(5376, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Uruguay', 170),
(5377, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Uzbekistan', 170),
(5378, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Vanuatu', 170),
(5379, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Venezuela', 170),
(5380, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Viet Nam', 170),
(5381, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Virgin Islands, British', 170),
(5382, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Virgin Islands, U.S.', 170),
(5383, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Wallis And Futuna', 170),
(5384, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Western Sahara', 170),
(5385, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Yemen', 170),
(5386, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Zambia', 170),
(5387, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Zimbabwe', 170),
(5388, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Sample 0', 171),
(5389, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Sample 1', 171),
(5390, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Sample 2', 171),
(5391, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Sample 3', 171),
(5392, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Sample 4', 171),
(5393, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Sample 5', 171),
(5394, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Sample 6', 171),
(5395, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Sample 7', 171),
(5396, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Sample 8', 171),
(5397, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Sample 9', 171),
(5398, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Sample 10', 171),
(5399, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Sample 11', 171),
(5400, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Sample 12', 171),
(5401, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Sample 13', 171),
(5402, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Sample 14', 171),
(5403, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Sample 15', 171),
(5404, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Sample 16', 171),
(5405, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Sample 17', 171),
(5406, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Sample 18', 171),
(5407, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Sample 19', 171),
(5408, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Sample 20', 171),
(5409, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Sample 21', 171),
(5410, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Sample 22', 171),
(5411, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Sample 23', 171),
(5412, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Sample 24', 171),
(5413, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Sample 25', 171),
(5414, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Sample 26', 171),
(5415, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Sample 27', 171),
(5416, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Sample 28', 171),
(5417, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Sample 29', 171),
(5418, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Sample 30', 171),
(5419, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Sample 31', 171),
(5420, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Sample 32', 171),
(5421, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Sample 33', 171),
(5422, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Sample 34', 171),
(5423, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Sample 35', 171),
(5424, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Sample 36', 171),
(5425, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Sample 37', 171),
(5426, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Sample 38', 171),
(5427, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Sample 39', 171),
(5428, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 0-0', 172),
(5429, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 0-1', 172),
(5430, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 0-2', 172),
(5431, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 0-3', 172),
(5432, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 0-4', 172),
(5433, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 0-5', 172),
(5434, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 0-6', 172),
(5435, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 0-7', 172),
(5436, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 0-8', 172),
(5437, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 0-9', 172),
(5438, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 0-10', 172),
(5439, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 0-11', 172),
(5440, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 0-12', 172),
(5441, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 0-13', 172),
(5442, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 0-14', 172),
(5443, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 0-15', 172),
(5444, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 0-16', 172),
(5445, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 0-17', 172),
(5446, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 0-18', 172),
(5447, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 0-19', 172),
(5448, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 0-20', 172),
(5449, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 0-21', 172),
(5450, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 0-22', 172),
(5451, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 0-23', 172),
(5452, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 0-24', 172),
(5453, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 1-0', 173),
(5454, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 1-1', 173),
(5455, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 1-2', 173),
(5456, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 1-3', 173),
(5457, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 1-4', 173),
(5458, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 1-5', 173),
(5459, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 1-6', 173),
(5460, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 1-7', 173),
(5461, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 1-8', 173),
(5462, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 1-9', 173),
(5463, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 1-10', 173),
(5464, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 1-11', 173),
(5465, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 1-12', 173),
(5466, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 1-13', 173),
(5467, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 1-14', 173),
(5468, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 1-15', 173),
(5469, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 1-16', 173),
(5470, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 1-17', 173),
(5471, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 1-18', 173),
(5472, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 1-19', 173),
(5473, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 1-20', 173),
(5474, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 1-21', 173),
(5475, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 1-22', 173),
(5476, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 1-23', 173),
(5477, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 1-24', 173),
(5478, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 2-0', 174),
(5479, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 2-1', 174),
(5480, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 2-2', 174),
(5481, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 2-3', 174),
(5482, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 2-4', 174),
(5483, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 2-5', 174),
(5484, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 2-6', 174),
(5485, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 2-7', 174),
(5486, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 2-8', 174),
(5487, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 2-9', 174),
(5488, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 2-10', 174),
(5489, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 2-11', 174),
(5490, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 2-12', 174),
(5491, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 2-13', 174),
(5492, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 2-14', 174),
(5493, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 2-15', 174),
(5494, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 2-16', 174),
(5495, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 2-17', 174),
(5496, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 2-18', 174),
(5497, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 2-19', 174),
(5498, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 2-20', 174),
(5499, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 2-21', 174),
(5500, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 2-22', 174),
(5501, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 2-23', 174),
(5502, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 2-24', 174),
(5503, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 3-0', 175),
(5504, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 3-1', 175),
(5505, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 3-2', 175),
(5506, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 3-3', 175),
(5507, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 3-4', 175),
(5508, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 3-5', 175),
(5509, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 3-6', 175),
(5510, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 3-7', 175),
(5511, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 3-8', 175),
(5512, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 3-9', 175),
(5513, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 3-10', 175),
(5514, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 3-11', 175),
(5515, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 3-12', 175),
(5516, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 3-13', 175),
(5517, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 3-14', 175),
(5518, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 3-15', 175),
(5519, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 3-16', 175),
(5520, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 3-17', 175),
(5521, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 3-18', 175),
(5522, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 3-19', 175),
(5523, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 3-20', 175),
(5524, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 3-21', 175),
(5525, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 3-22', 175),
(5526, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 3-23', 175),
(5527, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 3-24', 175),
(5528, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 4-0', 176),
(5529, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 4-1', 176),
(5530, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 4-2', 176),
(5531, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 4-3', 176),
(5532, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 4-4', 176),
(5533, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 4-5', 176),
(5534, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 4-6', 176),
(5535, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 4-7', 176),
(5536, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 4-8', 176),
(5537, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 4-9', 176),
(5538, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 4-10', 176),
(5539, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 4-11', 176),
(5540, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 4-12', 176),
(5541, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 4-13', 176),
(5542, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 4-14', 176),
(5543, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 4-15', 176),
(5544, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 4-16', 176),
(5545, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 4-17', 176),
(5546, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 4-18', 176),
(5547, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 4-19', 176),
(5548, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 4-20', 176),
(5549, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 4-21', 176),
(5550, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 4-22', 176),
(5551, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 4-23', 176),
(5552, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 4-24', 176),
(5553, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 5-0', 177),
(5554, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 5-1', 177),
(5555, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 5-2', 177),
(5556, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 5-3', 177),
(5557, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 5-4', 177),
(5558, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 5-5', 177),
(5559, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 5-6', 177),
(5560, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 5-7', 177),
(5561, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 5-8', 177),
(5562, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 5-9', 177),
(5563, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 5-10', 177),
(5564, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 5-11', 177),
(5565, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 5-12', 177),
(5566, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 5-13', 177),
(5567, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 5-14', 177),
(5568, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 5-15', 177),
(5569, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 5-16', 177),
(5570, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 5-17', 177),
(5571, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 5-18', 177),
(5572, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 5-19', 177),
(5573, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 5-20', 177),
(5574, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 5-21', 177),
(5575, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 5-22', 177),
(5576, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 5-23', 177),
(5577, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 5-24', 177),
(5578, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 6-0', 178),
(5579, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 6-1', 178),
(5580, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 6-2', 178),
(5581, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 6-3', 178),
(5582, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 6-4', 178),
(5583, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 6-5', 178),
(5584, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 6-6', 178),
(5585, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 6-7', 178),
(5586, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 6-8', 178),
(5587, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 6-9', 178),
(5588, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 6-10', 178),
(5589, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 6-11', 178),
(5590, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 6-12', 178),
(5591, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 6-13', 178),
(5592, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 6-14', 178),
(5593, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 6-15', 178),
(5594, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 6-16', 178),
(5595, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 6-17', 178),
(5596, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 6-18', 178),
(5597, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 6-19', 178),
(5598, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 6-20', 178),
(5599, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 6-21', 178),
(5600, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 6-22', 178),
(5601, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 6-23', 178),
(5602, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 6-24', 178),
(5603, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 7-0', 179),
(5604, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 7-1', 179),
(5605, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 7-2', 179),
(5606, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 7-3', 179),
(5607, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 7-4', 179),
(5608, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 7-5', 179),
(5609, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 7-6', 179),
(5610, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 7-7', 179),
(5611, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 7-8', 179),
(5612, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 7-9', 179),
(5613, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 7-10', 179),
(5614, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 7-11', 179),
(5615, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 7-12', 179),
(5616, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 7-13', 179),
(5617, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 7-14', 179),
(5618, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 7-15', 179),
(5619, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 7-16', 179),
(5620, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 7-17', 179),
(5621, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 7-18', 179),
(5622, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 7-19', 179),
(5623, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 7-20', 179),
(5624, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 7-21', 179),
(5625, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 7-22', 179),
(5626, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 7-23', 179),
(5627, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 7-24', 179),
(5628, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 8-0', 180),
(5629, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 8-1', 180),
(5630, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 8-2', 180),
(5631, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 8-3', 180),
(5632, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 8-4', 180),
(5633, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 8-5', 180),
(5634, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 8-6', 180),
(5635, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 8-7', 180),
(5636, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 8-8', 180),
(5637, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 8-9', 180),
(5638, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 8-10', 180),
(5639, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 8-11', 180),
(5640, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 8-12', 180),
(5641, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 8-13', 180),
(5642, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 8-14', 180),
(5643, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 8-15', 180),
(5644, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 8-16', 180),
(5645, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 8-17', 180),
(5646, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 8-18', 180),
(5647, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 8-19', 180),
(5648, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 8-20', 180),
(5649, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 8-21', 180),
(5650, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 8-22', 180),
(5651, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 8-23', 180),
(5652, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 8-24', 180),
(5653, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 9-0', 181),
(5654, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 9-1', 181),
(5655, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 9-2', 181),
(5656, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 9-3', 181),
(5657, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 9-4', 181),
(5658, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 9-5', 181),
(5659, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 9-6', 181),
(5660, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 9-7', 181),
(5661, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 9-8', 181),
(5662, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 9-9', 181),
(5663, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 9-10', 181),
(5664, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 9-11', 181),
(5665, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 9-12', 181),
(5666, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 9-13', 181),
(5667, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 9-14', 181),
(5668, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 9-15', 181),
(5669, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 9-16', 181),
(5670, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 9-17', 181),
(5671, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 9-18', 181),
(5672, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 9-19', 181),
(5673, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 9-20', 181),
(5674, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 9-21', 181),
(5675, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 9-22', 181),
(5676, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 9-23', 181),
(5677, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 9-24', 181),
(5678, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 10-0', 182),
(5679, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 10-1', 182),
(5680, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 10-2', 182),
(5681, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 10-3', 182),
(5682, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 10-4', 182),
(5683, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 10-5', 182),
(5684, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 10-6', 182),
(5685, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 10-7', 182),
(5686, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 10-8', 182),
(5687, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 10-9', 182),
(5688, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 10-10', 182),
(5689, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 10-11', 182),
(5690, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 10-12', 182),
(5691, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 10-13', 182),
(5692, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 10-14', 182),
(5693, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 10-15', 182),
(5694, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 10-16', 182),
(5695, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 10-17', 182),
(5696, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 10-18', 182),
(5697, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 10-19', 182),
(5698, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 10-20', 182),
(5699, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 10-21', 182),
(5700, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 10-22', 182),
(5701, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 10-23', 182),
(5702, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 10-24', 182),
(5703, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 11-0', 183),
(5704, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 11-1', 183),
(5705, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 11-2', 183),
(5706, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 11-3', 183),
(5707, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 11-4', 183),
(5708, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 11-5', 183),
(5709, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 11-6', 183),
(5710, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 11-7', 183),
(5711, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 11-8', 183),
(5712, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 11-9', 183),
(5713, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 11-10', 183),
(5714, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 11-11', 183),
(5715, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 11-12', 183),
(5716, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 11-13', 183),
(5717, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 11-14', 183),
(5718, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 11-15', 183),
(5719, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 11-16', 183),
(5720, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 11-17', 183),
(5721, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 11-18', 183),
(5722, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 11-19', 183),
(5723, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 11-20', 183),
(5724, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 11-21', 183),
(5725, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 11-22', 183),
(5726, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 11-23', 183),
(5727, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 11-24', 183),
(5728, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 12-0', 184),
(5729, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 12-1', 184),
(5730, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 12-2', 184),
(5731, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 12-3', 184),
(5732, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 12-4', 184),
(5733, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 12-5', 184),
(5734, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 12-6', 184),
(5735, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 12-7', 184),
(5736, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 12-8', 184),
(5737, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 12-9', 184),
(5738, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 12-10', 184),
(5739, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 12-11', 184),
(5740, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 12-12', 184),
(5741, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 12-13', 184),
(5742, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 12-14', 184),
(5743, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 12-15', 184),
(5744, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 12-16', 184),
(5745, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 12-17', 184),
(5746, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 12-18', 184),
(5747, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 12-19', 184),
(5748, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 12-20', 184),
(5749, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 12-21', 184),
(5750, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 12-22', 184),
(5751, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 12-23', 184),
(5752, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 12-24', 184),
(5753, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 13-0', 185),
(5754, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 13-1', 185),
(5755, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 13-2', 185),
(5756, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 13-3', 185),
(5757, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 13-4', 185),
(5758, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 13-5', 185),
(5759, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 13-6', 185),
(5760, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 13-7', 185),
(5761, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 13-8', 185),
(5762, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 13-9', 185),
(5763, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 13-10', 185),
(5764, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 13-11', 185),
(5765, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 13-12', 185),
(5766, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 13-13', 185),
(5767, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 13-14', 185),
(5768, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 13-15', 185),
(5769, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 13-16', 185),
(5770, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 13-17', 185),
(5771, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 13-18', 185),
(5772, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 13-19', 185),
(5773, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 13-20', 185),
(5774, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 13-21', 185),
(5775, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 13-22', 185),
(5776, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 13-23', 185),
(5777, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 13-24', 185),
(5778, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 14-0', 186),
(5779, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 14-1', 186),
(5780, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 14-2', 186),
(5781, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 14-3', 186),
(5782, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 14-4', 186),
(5783, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 14-5', 186),
(5784, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 14-6', 186),
(5785, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 14-7', 186),
(5786, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 14-8', 186),
(5787, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 14-9', 186),
(5788, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 14-10', 186),
(5789, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 14-11', 186),
(5790, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 14-12', 186),
(5791, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 14-13', 186),
(5792, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 14-14', 186),
(5793, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 14-15', 186),
(5794, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 14-16', 186),
(5795, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 14-17', 186),
(5796, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 14-18', 186),
(5797, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 14-19', 186),
(5798, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 14-20', 186),
(5799, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 14-21', 186),
(5800, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 14-22', 186),
(5801, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 14-23', 186),
(5802, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 14-24', 186),
(5803, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 15-0', 187),
(5804, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 15-1', 187),
(5805, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 15-2', 187),
(5806, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 15-3', 187),
(5807, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 15-4', 187),
(5808, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 15-5', 187),
(5809, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 15-6', 187),
(5810, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 15-7', 187);
INSERT INTO `Record` (`id`, `created`, `updated`, `versionNumber`, `name`, `recordType_id`) VALUES
(5811, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 15-8', 187),
(5812, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 15-9', 187),
(5813, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 15-10', 187),
(5814, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 15-11', 187),
(5815, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 15-12', 187),
(5816, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 15-13', 187),
(5817, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 15-14', 187),
(5818, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 15-15', 187),
(5819, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 15-16', 187),
(5820, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 15-17', 187),
(5821, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 15-18', 187),
(5822, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 15-19', 187),
(5823, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 15-20', 187),
(5824, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 15-21', 187),
(5825, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 15-22', 187),
(5826, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 15-23', 187),
(5827, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 15-24', 187),
(5828, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 16-0', 188),
(5829, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 16-1', 188),
(5830, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 16-2', 188),
(5831, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 16-3', 188),
(5832, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 16-4', 188),
(5833, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 16-5', 188),
(5834, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 16-6', 188),
(5835, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 16-7', 188),
(5836, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 16-8', 188),
(5837, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 16-9', 188),
(5838, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 16-10', 188),
(5839, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 16-11', 188),
(5840, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 16-12', 188),
(5841, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 16-13', 188),
(5842, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 16-14', 188),
(5843, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 16-15', 188),
(5844, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 16-16', 188),
(5845, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 16-17', 188),
(5846, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 16-18', 188),
(5847, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 16-19', 188),
(5848, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 16-20', 188),
(5849, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 16-21', 188),
(5850, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 16-22', 188),
(5851, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 16-23', 188),
(5852, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 16-24', 188),
(5853, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 17-0', 189),
(5854, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 17-1', 189),
(5855, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 17-2', 189),
(5856, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 17-3', 189),
(5857, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 17-4', 189),
(5858, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 17-5', 189),
(5859, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 17-6', 189),
(5860, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 17-7', 189),
(5861, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 17-8', 189),
(5862, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 17-9', 189),
(5863, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 17-10', 189),
(5864, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 17-11', 189),
(5865, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 17-12', 189),
(5866, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 17-13', 189),
(5867, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 17-14', 189),
(5868, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 17-15', 189),
(5869, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 17-16', 189),
(5870, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 17-17', 189),
(5871, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 17-18', 189),
(5872, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 17-19', 189),
(5873, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 17-20', 189),
(5874, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 17-21', 189),
(5875, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 17-22', 189),
(5876, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 17-23', 189),
(5877, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 17-24', 189),
(5878, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 18-0', 190),
(5879, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 18-1', 190),
(5880, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 18-2', 190),
(5881, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 18-3', 190),
(5882, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 18-4', 190),
(5883, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 18-5', 190),
(5884, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 18-6', 190),
(5885, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 18-7', 190),
(5886, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 18-8', 190),
(5887, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 18-9', 190),
(5888, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 18-10', 190),
(5889, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 18-11', 190),
(5890, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 18-12', 190),
(5891, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 18-13', 190),
(5892, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 18-14', 190),
(5893, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 18-15', 190),
(5894, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 18-16', 190),
(5895, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 18-17', 190),
(5896, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 18-18', 190),
(5897, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 18-19', 190),
(5898, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 18-20', 190),
(5899, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 18-21', 190),
(5900, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 18-22', 190),
(5901, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 18-23', 190),
(5902, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 18-24', 190),
(5903, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 19-0', 191),
(5904, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 19-1', 191),
(5905, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 19-2', 191),
(5906, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 19-3', 191),
(5907, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 19-4', 191),
(5908, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 19-5', 191),
(5909, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 19-6', 191),
(5910, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 19-7', 191),
(5911, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 19-8', 191),
(5912, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 19-9', 191),
(5913, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 19-10', 191),
(5914, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 19-11', 191),
(5915, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 19-12', 191),
(5916, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 19-13', 191),
(5917, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 19-14', 191),
(5918, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 19-15', 191),
(5919, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 19-16', 191),
(5920, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 19-17', 191),
(5921, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 19-18', 191),
(5922, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 19-19', 191),
(5923, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 19-20', 191),
(5924, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 19-21', 191),
(5925, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 19-22', 191),
(5926, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 19-23', 191),
(5927, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 19-24', 191),
(5928, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 20-0', 192),
(5929, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 20-1', 192),
(5930, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 20-2', 192),
(5931, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 20-3', 192),
(5932, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 20-4', 192),
(5933, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 20-5', 192),
(5934, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 20-6', 192),
(5935, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 20-7', 192),
(5936, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 20-8', 192),
(5937, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 20-9', 192),
(5938, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 20-10', 192),
(5939, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 20-11', 192),
(5940, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 20-12', 192),
(5941, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 20-13', 192),
(5942, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 20-14', 192),
(5943, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 20-15', 192),
(5944, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 20-16', 192),
(5945, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 20-17', 192),
(5946, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 20-18', 192),
(5947, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 20-19', 192),
(5948, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 20-20', 192),
(5949, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 20-21', 192),
(5950, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 20-22', 192),
(5951, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 20-23', 192),
(5952, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 20-24', 192),
(5953, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 21-0', 193),
(5954, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 21-1', 193),
(5955, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 21-2', 193),
(5956, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 21-3', 193),
(5957, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 21-4', 193),
(5958, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 21-5', 193),
(5959, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 21-6', 193),
(5960, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 21-7', 193),
(5961, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 21-8', 193),
(5962, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 21-9', 193),
(5963, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 21-10', 193),
(5964, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 21-11', 193),
(5965, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 21-12', 193),
(5966, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 21-13', 193),
(5967, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 21-14', 193),
(5968, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 21-15', 193),
(5969, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 21-16', 193),
(5970, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 21-17', 193),
(5971, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 21-18', 193),
(5972, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 21-19', 193),
(5973, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 21-20', 193),
(5974, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 21-21', 193),
(5975, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 21-22', 193),
(5976, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 21-23', 193),
(5977, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 21-24', 193),
(5978, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 22-0', 194),
(5979, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 22-1', 194),
(5980, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 22-2', 194),
(5981, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 22-3', 194),
(5982, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 22-4', 194),
(5983, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 22-5', 194),
(5984, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 22-6', 194),
(5985, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 22-7', 194),
(5986, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 22-8', 194),
(5987, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 22-9', 194),
(5988, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 22-10', 194),
(5989, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 22-11', 194),
(5990, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 22-12', 194),
(5991, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 22-13', 194),
(5992, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 22-14', 194),
(5993, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 22-15', 194),
(5994, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 22-16', 194),
(5995, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 22-17', 194),
(5996, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 22-18', 194),
(5997, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 22-19', 194),
(5998, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 22-20', 194),
(5999, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 22-21', 194),
(6000, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 22-22', 194),
(6001, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 22-23', 194),
(6002, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 22-24', 194),
(6003, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 23-0', 195),
(6004, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 23-1', 195),
(6005, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 23-2', 195),
(6006, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 23-3', 195),
(6007, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 23-4', 195),
(6008, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 23-5', 195),
(6009, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 23-6', 195),
(6010, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 23-7', 195),
(6011, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 23-8', 195),
(6012, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 23-9', 195),
(6013, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 23-10', 195),
(6014, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 23-11', 195),
(6015, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 23-12', 195),
(6016, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 23-13', 195),
(6017, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 23-14', 195),
(6018, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 23-15', 195),
(6019, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 23-16', 195),
(6020, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 23-17', 195),
(6021, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 23-18', 195),
(6022, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 23-19', 195),
(6023, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 23-20', 195),
(6024, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 23-21', 195),
(6025, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 23-22', 195),
(6026, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 23-23', 195),
(6027, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 23-24', 195),
(6028, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 24-0', 196),
(6029, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 24-1', 196),
(6030, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 24-2', 196),
(6031, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 24-3', 196),
(6032, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 24-4', 196),
(6033, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 24-5', 196),
(6034, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 24-6', 196),
(6035, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 24-7', 196),
(6036, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 24-8', 196),
(6037, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 24-9', 196),
(6038, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 24-10', 196),
(6039, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 24-11', 196),
(6040, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 24-12', 196),
(6041, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 24-13', 196),
(6042, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 24-14', 196),
(6043, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 24-15', 196),
(6044, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 24-16', 196),
(6045, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 24-17', 196),
(6046, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 24-18', 196),
(6047, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 24-19', 196),
(6048, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 24-20', 196),
(6049, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 24-21', 196),
(6050, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 24-22', 196),
(6051, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 24-23', 196),
(6052, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 24-24', 196),
(6053, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 25-0', 197),
(6054, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 25-1', 197),
(6055, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 25-2', 197),
(6056, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 25-3', 197),
(6057, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 25-4', 197),
(6058, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 25-5', 197),
(6059, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 25-6', 197),
(6060, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 25-7', 197),
(6061, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 25-8', 197),
(6062, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 25-9', 197),
(6063, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 25-10', 197),
(6064, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 25-11', 197),
(6065, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 25-12', 197),
(6066, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 25-13', 197),
(6067, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 25-14', 197),
(6068, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 25-15', 197),
(6069, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 25-16', 197),
(6070, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 25-17', 197),
(6071, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 25-18', 197),
(6072, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 25-19', 197),
(6073, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 25-20', 197),
(6074, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 25-21', 197),
(6075, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 25-22', 197),
(6076, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 25-23', 197),
(6077, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 25-24', 197),
(6078, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 26-0', 198),
(6079, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 26-1', 198),
(6080, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 26-2', 198),
(6081, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 26-3', 198),
(6082, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 26-4', 198),
(6083, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 26-5', 198),
(6084, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 26-6', 198),
(6085, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 26-7', 198),
(6086, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 26-8', 198),
(6087, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 26-9', 198),
(6088, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 26-10', 198),
(6089, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 26-11', 198),
(6090, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 26-12', 198),
(6091, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 26-13', 198),
(6092, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 26-14', 198),
(6093, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 26-15', 198),
(6094, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 26-16', 198),
(6095, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 26-17', 198),
(6096, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 26-18', 198),
(6097, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 26-19', 198),
(6098, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 26-20', 198),
(6099, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 26-21', 198),
(6100, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 26-22', 198),
(6101, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 26-23', 198),
(6102, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 26-24', 198),
(6103, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 27-0', 199),
(6104, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 27-1', 199),
(6105, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 27-2', 199),
(6106, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 27-3', 199),
(6107, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 27-4', 199),
(6108, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 27-5', 199),
(6109, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 27-6', 199),
(6110, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 27-7', 199),
(6111, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 27-8', 199),
(6112, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 27-9', 199),
(6113, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 27-10', 199),
(6114, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 27-11', 199),
(6115, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 27-12', 199),
(6116, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 27-13', 199),
(6117, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 27-14', 199),
(6118, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 27-15', 199),
(6119, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 27-16', 199),
(6120, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 27-17', 199),
(6121, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 27-18', 199),
(6122, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 27-19', 199),
(6123, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 27-20', 199),
(6124, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 27-21', 199),
(6125, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 27-22', 199),
(6126, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 27-23', 199),
(6127, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 27-24', 199),
(6128, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 28-0', 200),
(6129, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 28-1', 200),
(6130, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 28-2', 200),
(6131, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 28-3', 200),
(6132, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 28-4', 200),
(6133, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 28-5', 200),
(6134, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 28-6', 200),
(6135, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 28-7', 200),
(6136, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 28-8', 200),
(6137, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 28-9', 200),
(6138, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 28-10', 200),
(6139, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 28-11', 200),
(6140, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 28-12', 200),
(6141, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 28-13', 200),
(6142, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 28-14', 200),
(6143, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 28-15', 200),
(6144, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 28-16', 200),
(6145, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 28-17', 200),
(6146, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 28-18', 200),
(6147, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 28-19', 200),
(6148, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 28-20', 200),
(6149, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 28-21', 200),
(6150, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 28-22', 200),
(6151, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 28-23', 200),
(6152, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 28-24', 200),
(6153, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 29-0', 201),
(6154, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 29-1', 201),
(6155, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 29-2', 201),
(6156, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 29-3', 201),
(6157, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 29-4', 201),
(6158, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 29-5', 201),
(6159, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 29-6', 201),
(6160, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 29-7', 201),
(6161, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 29-8', 201),
(6162, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 29-9', 201),
(6163, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 29-10', 201),
(6164, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 29-11', 201),
(6165, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 29-12', 201),
(6166, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 29-13', 201),
(6167, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 29-14', 201),
(6168, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 29-15', 201),
(6169, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 29-16', 201),
(6170, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 29-17', 201),
(6171, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 29-18', 201),
(6172, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 29-19', 201),
(6173, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 29-20', 201),
(6174, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 29-21', 201),
(6175, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 29-22', 201),
(6176, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 29-23', 201),
(6177, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 29-24', 201),
(6178, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 30-0', 202),
(6179, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 30-1', 202),
(6180, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 30-2', 202),
(6181, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 30-3', 202),
(6182, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 30-4', 202),
(6183, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 30-5', 202),
(6184, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 30-6', 202),
(6185, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 30-7', 202),
(6186, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 30-8', 202),
(6187, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 30-9', 202),
(6188, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 30-10', 202),
(6189, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 30-11', 202),
(6190, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 30-12', 202),
(6191, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 30-13', 202),
(6192, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 30-14', 202),
(6193, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 30-15', 202),
(6194, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 30-16', 202),
(6195, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 30-17', 202),
(6196, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 30-18', 202),
(6197, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 30-19', 202),
(6198, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 30-20', 202),
(6199, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 30-21', 202),
(6200, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 30-22', 202),
(6201, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 30-23', 202),
(6202, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 30-24', 202),
(6203, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 31-0', 203),
(6204, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 31-1', 203),
(6205, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 31-2', 203),
(6206, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 31-3', 203),
(6207, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 31-4', 203),
(6208, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 31-5', 203),
(6209, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 31-6', 203),
(6210, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 31-7', 203),
(6211, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 31-8', 203),
(6212, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 31-9', 203),
(6213, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 31-10', 203),
(6214, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 31-11', 203),
(6215, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 31-12', 203),
(6216, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 31-13', 203),
(6217, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 31-14', 203),
(6218, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 31-15', 203),
(6219, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 31-16', 203),
(6220, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 31-17', 203),
(6221, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 31-18', 203),
(6222, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 31-19', 203),
(6223, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 31-20', 203),
(6224, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 31-21', 203),
(6225, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 31-22', 203),
(6226, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 31-23', 203),
(6227, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 31-24', 203),
(6228, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 32-0', 204),
(6229, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 32-1', 204),
(6230, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 32-2', 204),
(6231, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 32-3', 204),
(6232, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 32-4', 204),
(6233, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 32-5', 204),
(6234, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 32-6', 204),
(6235, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 32-7', 204),
(6236, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 32-8', 204),
(6237, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 32-9', 204),
(6238, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 32-10', 204),
(6239, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 32-11', 204),
(6240, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 32-12', 204),
(6241, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 32-13', 204),
(6242, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 32-14', 204),
(6243, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 32-15', 204),
(6244, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 32-16', 204),
(6245, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 32-17', 204),
(6246, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 32-18', 204),
(6247, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 32-19', 204),
(6248, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 32-20', 204),
(6249, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 32-21', 204),
(6250, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 32-22', 204),
(6251, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 32-23', 204),
(6252, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 32-24', 204),
(6253, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 33-0', 205),
(6254, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 33-1', 205),
(6255, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 33-2', 205),
(6256, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 33-3', 205),
(6257, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 33-4', 205),
(6258, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 33-5', 205),
(6259, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 33-6', 205),
(6260, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 33-7', 205),
(6261, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 33-8', 205),
(6262, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 33-9', 205),
(6263, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 33-10', 205),
(6264, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 33-11', 205),
(6265, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 33-12', 205),
(6266, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 33-13', 205),
(6267, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 33-14', 205),
(6268, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 33-15', 205),
(6269, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 33-16', 205),
(6270, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 33-17', 205),
(6271, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 33-18', 205),
(6272, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 33-19', 205),
(6273, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 33-20', 205),
(6274, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 33-21', 205),
(6275, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 33-22', 205),
(6276, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 33-23', 205),
(6277, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 33-24', 205),
(6278, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 34-0', 206),
(6279, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 34-1', 206),
(6280, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 34-2', 206),
(6281, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 34-3', 206),
(6282, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 34-4', 206),
(6283, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 34-5', 206),
(6284, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 34-6', 206),
(6285, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 34-7', 206),
(6286, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 34-8', 206),
(6287, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 34-9', 206),
(6288, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 34-10', 206),
(6289, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 34-11', 206),
(6290, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 34-12', 206),
(6291, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 34-13', 206),
(6292, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 34-14', 206),
(6293, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 34-15', 206),
(6294, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 34-16', 206),
(6295, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 34-17', 206),
(6296, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 34-18', 206),
(6297, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 34-19', 206),
(6298, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 34-20', 206),
(6299, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 34-21', 206),
(6300, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 34-22', 206),
(6301, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 34-23', 206),
(6302, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 34-24', 206),
(6303, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 35-0', 207),
(6304, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 35-1', 207),
(6305, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 35-2', 207),
(6306, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 35-3', 207),
(6307, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 35-4', 207),
(6308, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 35-5', 207),
(6309, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 35-6', 207),
(6310, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 35-7', 207),
(6311, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 35-8', 207),
(6312, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 35-9', 207),
(6313, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 35-10', 207),
(6314, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 35-11', 207),
(6315, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 35-12', 207),
(6316, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 35-13', 207),
(6317, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 35-14', 207),
(6318, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 35-15', 207),
(6319, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 35-16', 207),
(6320, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 35-17', 207),
(6321, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 35-18', 207),
(6322, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 35-19', 207),
(6323, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 35-20', 207),
(6324, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 35-21', 207),
(6325, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 35-22', 207),
(6326, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 35-23', 207),
(6327, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 35-24', 207),
(6328, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 36-0', 208),
(6329, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 36-1', 208),
(6330, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 36-2', 208),
(6331, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 36-3', 208),
(6332, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 36-4', 208),
(6333, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 36-5', 208),
(6334, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 36-6', 208),
(6335, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 36-7', 208),
(6336, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 36-8', 208),
(6337, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 36-9', 208),
(6338, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 36-10', 208),
(6339, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 36-11', 208),
(6340, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 36-12', 208),
(6341, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 36-13', 208),
(6342, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 36-14', 208),
(6343, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 36-15', 208),
(6344, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 36-16', 208),
(6345, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 36-17', 208),
(6346, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 36-18', 208),
(6347, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 36-19', 208),
(6348, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 36-20', 208),
(6349, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 36-21', 208),
(6350, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 36-22', 208),
(6351, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 36-23', 208),
(6352, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 36-24', 208),
(6353, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 37-0', 209),
(6354, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 37-1', 209),
(6355, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 37-2', 209),
(6356, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 37-3', 209),
(6357, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 37-4', 209),
(6358, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 37-5', 209),
(6359, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 37-6', 209),
(6360, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 37-7', 209),
(6361, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 37-8', 209),
(6362, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 37-9', 209),
(6363, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 37-10', 209),
(6364, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 37-11', 209),
(6365, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 37-12', 209),
(6366, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 37-13', 209),
(6367, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 37-14', 209),
(6368, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 37-15', 209),
(6369, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 37-16', 209),
(6370, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 37-17', 209),
(6371, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 37-18', 209),
(6372, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 37-19', 209),
(6373, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 37-20', 209),
(6374, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 37-21', 209),
(6375, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 37-22', 209),
(6376, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 37-23', 209),
(6377, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 37-24', 209),
(6378, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 38-0', 210),
(6379, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 38-1', 210),
(6380, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 38-2', 210),
(6381, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 38-3', 210),
(6382, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 38-4', 210),
(6383, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 38-5', 210),
(6384, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 38-6', 210),
(6385, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 38-7', 210),
(6386, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 38-8', 210),
(6387, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 38-9', 210),
(6388, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 38-10', 210),
(6389, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 38-11', 210),
(6390, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 38-12', 210),
(6391, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 38-13', 210),
(6392, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 38-14', 210),
(6393, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 38-15', 210),
(6394, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 38-16', 210),
(6395, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 38-17', 210),
(6396, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 38-18', 210),
(6397, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 38-19', 210),
(6398, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 38-20', 210),
(6399, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 38-21', 210),
(6400, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 38-22', 210),
(6401, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 38-23', 210),
(6402, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 38-24', 210),
(6403, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 39-0', 211),
(6404, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 39-1', 211),
(6405, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 39-2', 211),
(6406, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 39-3', 211),
(6407, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 39-4', 211),
(6408, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 39-5', 211),
(6409, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 39-6', 211),
(6410, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 39-7', 211),
(6411, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 39-8', 211),
(6412, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 39-9', 211),
(6413, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 39-10', 211),
(6414, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 39-11', 211),
(6415, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 39-12', 211),
(6416, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 39-13', 211),
(6417, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 39-14', 211),
(6418, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 39-15', 211),
(6419, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 39-16', 211),
(6420, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 39-17', 211),
(6421, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 39-18', 211),
(6422, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 39-19', 211),
(6423, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 39-20', 211),
(6424, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 39-21', 211),
(6425, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 39-22', 211),
(6426, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 39-23', 211),
(6427, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Record 39-24', 211);

-- --------------------------------------------------------

--
-- Table structure for table `RecordTag`
--
-- Creation: Oct 26, 2014 at 08:31 PM
--

DROP TABLE IF EXISTS `RecordTag`;
CREATE TABLE IF NOT EXISTS `RecordTag` (
`id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `record_id` int(11) NOT NULL,
  `displayOrder` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=26 ;

--
-- RELATIONS FOR TABLE `RecordTag`:
--   `record_id`
--       `Record` -> `id`
--   `tag_id`
--       `tag` -> `id`
--

-- --------------------------------------------------------

--
-- Table structure for table `RecordType`
--
-- Creation: Oct 26, 2014 at 08:31 PM
--

DROP TABLE IF EXISTS `RecordType`;
CREATE TABLE IF NOT EXISTS `RecordType` (
`id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `versionNumber` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=213 ;

--
-- Dumping data for table `RecordType`
--

INSERT INTO `RecordType` (`id`, `created`, `updated`, `versionNumber`, `name`) VALUES
(170, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Country'),
(171, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Sample'),
(172, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'RecordType 0'),
(173, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'RecordType 1'),
(174, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'RecordType 2'),
(175, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'RecordType 3'),
(176, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'RecordType 4'),
(177, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'RecordType 5'),
(178, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'RecordType 6'),
(179, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'RecordType 7'),
(180, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'RecordType 8'),
(181, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'RecordType 9'),
(182, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'RecordType 10'),
(183, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'RecordType 11'),
(184, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'RecordType 12'),
(185, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'RecordType 13'),
(186, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'RecordType 14'),
(187, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'RecordType 15'),
(188, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'RecordType 16'),
(189, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'RecordType 17'),
(190, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'RecordType 18'),
(191, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'RecordType 19'),
(192, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'RecordType 20'),
(193, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'RecordType 21'),
(194, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'RecordType 22'),
(195, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'RecordType 23'),
(196, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'RecordType 24'),
(197, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'RecordType 25'),
(198, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'RecordType 26'),
(199, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'RecordType 27'),
(200, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'RecordType 28'),
(201, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'RecordType 29'),
(202, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'RecordType 30'),
(203, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'RecordType 31'),
(204, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'RecordType 32'),
(205, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'RecordType 33'),
(206, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'RecordType 34'),
(207, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'RecordType 35'),
(208, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'RecordType 36'),
(209, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'RecordType 37'),
(210, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'RecordType 38'),
(211, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'RecordType 39'),
(212, '2014-11-04 20:04:34', '2014-11-04 20:04:34', 0, 'All Fields RecordType');

-- --------------------------------------------------------

--
-- Table structure for table `RecordTypeField`
--
-- Creation: Oct 26, 2014 at 08:31 PM
--

DROP TABLE IF EXISTS `RecordTypeField`;
CREATE TABLE IF NOT EXISTS `RecordTypeField` (
`id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `versionNumber` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `displayOrder` int(11) NOT NULL,
  `multipleSelection` tinyint(1) NOT NULL,
  `required` tinyint(1) NOT NULL,
  `recordType_id` int(11) NOT NULL,
  `fieldType_id` int(11) NOT NULL,
  `referencedRecordType_id` int(11) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=47 ;

--
-- RELATIONS FOR TABLE `RecordTypeField`:
--   `recordType_id`
--       `RecordType` -> `id`
--   `fieldType_id`
--       `FieldType` -> `id`
--   `referencedRecordType_id`
--       `RecordType` -> `id`
--

--
-- Dumping data for table `RecordTypeField`
--

INSERT INTO `RecordTypeField` (`id`, `created`, `updated`, `versionNumber`, `name`, `displayOrder`, `multipleSelection`, `required`, `recordType_id`, `fieldType_id`, `referencedRecordType_id`) VALUES
(30, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Code', 0, 0, 1, 170, 68, NULL),
(31, '2014-11-04 15:48:17', '2014-11-04 15:48:17', 0, 'Image', 0, 0, 1, 171, 73, NULL),
(32, '2014-11-04 20:04:35', '2014-11-04 20:04:35', 0, 'Boolean', 0, 0, 1, 212, 61, NULL),
(33, '2014-11-04 20:04:35', '2014-11-04 20:04:35', 0, 'Choice', 1, 1, 1, 212, 62, NULL),
(34, '2014-11-04 20:04:35', '2014-11-04 20:04:35', 0, 'Date', 2, 0, 1, 212, 63, NULL),
(35, '2014-11-04 20:04:35', '2014-11-04 20:04:35', 0, 'Date Time', 3, 0, 1, 212, 64, NULL),
(36, '2014-11-04 20:04:35', '2014-11-04 20:04:35', 0, 'Float', 4, 0, 1, 212, 65, NULL),
(37, '2014-11-04 20:04:35', '2014-11-04 20:04:35', 0, 'Integer', 5, 0, 1, 212, 66, NULL),
(38, '2014-11-04 20:04:35', '2014-11-04 20:04:35', 0, 'Record', 6, 1, 1, 212, 67, 170),
(39, '2014-11-04 20:04:35', '2014-11-04 20:04:35', 0, 'String', 7, 0, 1, 212, 68, NULL),
(40, '2014-11-04 20:04:35', '2014-11-04 20:04:35', 0, 'Text', 8, 0, 1, 212, 69, NULL),
(41, '2014-11-04 20:04:35', '2014-11-04 20:04:35', 0, 'Time', 9, 0, 1, 212, 70, NULL),
(42, '2014-11-04 20:04:35', '2014-11-04 20:04:35', 0, 'Table', 10, 0, 1, 212, 71, NULL),
(43, '2014-11-04 20:04:35', '2014-11-04 20:04:35', 0, 'File', 11, 0, 1, 212, 72, NULL),
(44, '2014-11-04 20:04:35', '2014-11-04 20:04:35', 0, 'Image', 12, 0, 1, 212, 73, NULL),
(45, '2014-11-04 20:04:35', '2014-11-04 20:04:35', 0, 'Video', 13, 0, 1, 212, 74, NULL),
(46, '2014-11-04 20:04:35', '2014-11-04 20:04:35', 0, 'Audio', 14, 0, 1, 212, 75, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `StoredFile`
--
-- Creation: Oct 26, 2014 at 08:31 PM
--

DROP TABLE IF EXISTS `StoredFile`;
CREATE TABLE IF NOT EXISTS `StoredFile` (
`id` int(11) NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `originalName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=205 ;

--
-- Dumping data for table `StoredFile`
--

INSERT INTO `StoredFile` (`id`, `path`, `originalName`) VALUES
(165, '0.tif', '0.tif'),
(166, '1.tif', '1.tif'),
(167, '2.tif', '2.tif'),
(168, '3.tif', '3.tif'),
(169, '4.tif', '4.tif'),
(170, '5.tif', '5.tif'),
(171, '6.tif', '6.tif'),
(172, '7.tif', '7.tif'),
(173, '8.tif', '8.tif'),
(174, '9.tif', '9.tif'),
(175, '0.tif', '0.tif'),
(176, '1.tif', '1.tif'),
(177, '2.tif', '2.tif'),
(178, '3.tif', '3.tif'),
(179, '4.tif', '4.tif'),
(180, '5.tif', '5.tif'),
(181, '6.tif', '6.tif'),
(182, '7.tif', '7.tif'),
(183, '8.tif', '8.tif'),
(184, '9.tif', '9.tif'),
(185, '0.tif', '0.tif'),
(186, '1.tif', '1.tif'),
(187, '2.tif', '2.tif'),
(188, '3.tif', '3.tif'),
(189, '4.tif', '4.tif'),
(190, '5.tif', '5.tif'),
(191, '6.tif', '6.tif'),
(192, '7.tif', '7.tif'),
(193, '8.tif', '8.tif'),
(194, '9.tif', '9.tif'),
(195, '0.tif', '0.tif'),
(196, '1.tif', '1.tif'),
(197, '2.tif', '2.tif'),
(198, '3.tif', '3.tif'),
(199, '4.tif', '4.tif'),
(200, '5.tif', '5.tif'),
(201, '6.tif', '6.tif'),
(202, '7.tif', '7.tif'),
(203, '8.tif', '8.tif'),
(204, '9.tif', '9.tif');

-- --------------------------------------------------------

--
-- Table structure for table `TableCell`
--
-- Creation: Oct 26, 2014 at 08:31 PM
--

DROP TABLE IF EXISTS `TableCell`;
CREATE TABLE IF NOT EXISTS `TableCell` (
`id` int(11) NOT NULL,
  `row_id` int(11) NOT NULL,
  `column_id` int(11) NOT NULL,
  `value` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=573 ;

--
-- RELATIONS FOR TABLE `TableCell`:
--   `row_id`
--       `TableRow` -> `id`
--   `column_id`
--       `TableColumn` -> `id`
--

-- --------------------------------------------------------

--
-- Table structure for table `TableColumn`
--
-- Creation: Oct 26, 2014 at 08:31 PM
--

DROP TABLE IF EXISTS `TableColumn`;
CREATE TABLE IF NOT EXISTS `TableColumn` (
`id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `displayOrder` int(11) NOT NULL,
  `recordTypeField_id` int(11) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- RELATIONS FOR TABLE `TableColumn`:
--   `recordTypeField_id`
--       `RecordTypeField` -> `id`
--

--
-- Dumping data for table `TableColumn`
--

INSERT INTO `TableColumn` (`id`, `name`, `displayOrder`, `recordTypeField_id`) VALUES
(4, 'Column 1', 0, 42),
(5, 'Column 2', 1, 42),
(6, 'Column 3', 2, 42),
(7, 'Column 4', 3, 42);

-- --------------------------------------------------------

--
-- Table structure for table `TableRow`
--
-- Creation: Oct 26, 2014 at 08:31 PM
--

DROP TABLE IF EXISTS `TableRow`;
CREATE TABLE IF NOT EXISTS `TableRow` (
`id` int(11) NOT NULL,
  `fieldValue_id` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=287 ;

--
-- RELATIONS FOR TABLE `TableRow`:
--   `fieldValue_id`
--       `FieldValueTable` -> `id`
--

-- --------------------------------------------------------

--
-- Table structure for table `tag`
--
-- Creation: Oct 26, 2014 at 08:31 PM
--

DROP TABLE IF EXISTS `tag`;
CREATE TABLE IF NOT EXISTS `tag` (
`id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15 ;

-- --------------------------------------------------------

--
-- Table structure for table `Thread`
--
-- Creation: Oct 26, 2014 at 08:31 PM
--

DROP TABLE IF EXISTS `Thread`;
CREATE TABLE IF NOT EXISTS `Thread` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permalink` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_commentable` tinyint(1) NOT NULL,
  `num_comments` int(11) NOT NULL,
  `last_comment_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Vote`
--
-- Creation: Oct 26, 2014 at 08:31 PM
--

DROP TABLE IF EXISTS `Vote`;
CREATE TABLE IF NOT EXISTS `Vote` (
`id` int(11) NOT NULL,
  `comment_id` int(11) DEFAULT NULL,
  `voter_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `value` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- RELATIONS FOR TABLE `Vote`:
--   `voter_id`
--       `fos_user` -> `id`
--   `comment_id`
--       `Comment` -> `id`
--

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acl_classes`
--
ALTER TABLE `acl_classes`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_69DD750638A36066` (`class_type`);

--
-- Indexes for table `acl_entries`
--
ALTER TABLE `acl_entries`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_46C8B806EA000B103D9AB4A64DEF17BCE4289BF4` (`class_id`,`object_identity_id`,`field_name`,`ace_order`), ADD KEY `IDX_46C8B806EA000B103D9AB4A6DF9183C9` (`class_id`,`object_identity_id`,`security_identity_id`), ADD KEY `IDX_46C8B806EA000B10` (`class_id`), ADD KEY `IDX_46C8B8063D9AB4A6` (`object_identity_id`), ADD KEY `IDX_46C8B806DF9183C9` (`security_identity_id`);

--
-- Indexes for table `acl_object_identities`
--
ALTER TABLE `acl_object_identities`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_9407E5494B12AD6EA000B10` (`object_identifier`,`class_id`), ADD KEY `IDX_9407E54977FA751A` (`parent_object_identity_id`);

--
-- Indexes for table `acl_object_identity_ancestors`
--
ALTER TABLE `acl_object_identity_ancestors`
 ADD PRIMARY KEY (`object_identity_id`,`ancestor_id`), ADD KEY `IDX_825DE2993D9AB4A6` (`object_identity_id`), ADD KEY `IDX_825DE299C671CEA1` (`ancestor_id`);

--
-- Indexes for table `acl_security_identities`
--
ALTER TABLE `acl_security_identities`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_8835EE78772E836AF85E0677` (`identifier`,`username`);

--
-- Indexes for table `Choice`
--
ALTER TABLE `Choice`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_C6075FA46D5405D6` (`recordTypeField_id`);

--
-- Indexes for table `Comment`
--
ALTER TABLE `Comment`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_5BC96BF0E2904019` (`thread_id`), ADD KEY `IDX_5BC96BF0F675F31B` (`author_id`);

--
-- Indexes for table `Favorite`
--
ALTER TABLE `Favorite`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_91B3EC8FA76ED395` (`user_id`), ADD KEY `IDX_91B3EC8FC54C8C93` (`type_id`);

--
-- Indexes for table `FavoriteType`
--
ALTER TABLE `FavoriteType`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_71CE29175E237E06` (`name`);

--
-- Indexes for table `FieldType`
--
ALTER TABLE `FieldType`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_43878F565E237E06` (`name`);

--
-- Indexes for table `FieldValue`
--
ALTER TABLE `FieldValue`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_DD026D416D5405D6` (`recordTypeField_id`), ADD KEY `IDX_DD026D414DFD750C` (`record_id`);

--
-- Indexes for table `FieldValueBoolean`
--
ALTER TABLE `FieldValueBoolean`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `FieldValueChoice`
--
ALTER TABLE `FieldValueChoice`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fieldvaluechoice_choice`
--
ALTER TABLE `fieldvaluechoice_choice`
 ADD PRIMARY KEY (`fieldvaluechoice_id`,`choice_id`), ADD KEY `IDX_5F1AFA29C95ED4EB` (`fieldvaluechoice_id`), ADD KEY `IDX_5F1AFA29998666D1` (`choice_id`);

--
-- Indexes for table `FieldValueDate`
--
ALTER TABLE `FieldValueDate`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `FieldValueDateTime`
--
ALTER TABLE `FieldValueDateTime`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `FieldValueFile`
--
ALTER TABLE `FieldValueFile`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_855E32EE9DD92E3C` (`storedFile_id`);

--
-- Indexes for table `FieldValueFileGallery`
--
ALTER TABLE `FieldValueFileGallery`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fieldvaluefilegallery_storedfile`
--
ALTER TABLE `fieldvaluefilegallery_storedfile`
 ADD PRIMARY KEY (`fieldvaluefilegallery_id`,`storedfile_id`), ADD KEY `IDX_F1678E13A2BFAE0E` (`fieldvaluefilegallery_id`), ADD KEY `IDX_F1678E13526417A0` (`storedfile_id`);

--
-- Indexes for table `FieldValueFloat`
--
ALTER TABLE `FieldValueFloat`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `FieldValueInteger`
--
ALTER TABLE `FieldValueInteger`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `FieldValueRecords`
--
ALTER TABLE `FieldValueRecords`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fieldvaluerecords_record`
--
ALTER TABLE `fieldvaluerecords_record`
 ADD PRIMARY KEY (`fieldvaluerecords_id`,`record_id`), ADD KEY `IDX_1ECA109C28D60D7B` (`fieldvaluerecords_id`), ADD KEY `IDX_1ECA109C4DFD750C` (`record_id`);

--
-- Indexes for table `FieldValueString`
--
ALTER TABLE `FieldValueString`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `FieldValueTable`
--
ALTER TABLE `FieldValueTable`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `FieldValueText`
--
ALTER TABLE `FieldValueText`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `FieldValueTime`
--
ALTER TABLE `FieldValueTime`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fos_group`
--
ALTER TABLE `fos_group`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_4B019DDB5E237E06` (`name`);

--
-- Indexes for table `fos_user`
--
ALTER TABLE `fos_user`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_957A647992FC23A8` (`username_canonical`), ADD UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`);

--
-- Indexes for table `fos_user_user_group`
--
ALTER TABLE `fos_user_user_group`
 ADD PRIMARY KEY (`user_id`,`group_id`), ADD KEY `IDX_B3C77447A76ED395` (`user_id`), ADD KEY `IDX_B3C77447FE54D947` (`group_id`);

--
-- Indexes for table `Record`
--
ALTER TABLE `Record`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_9C989AA7919E14F2` (`recordType_id`);

--
-- Indexes for table `RecordTag`
--
ALTER TABLE `RecordTag`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `recordtag_unique_record_tag_idx` (`record_id`,`tag_id`), ADD KEY `IDX_9289C3FCBAD26311` (`tag_id`), ADD KEY `IDX_9289C3FC4DFD750C` (`record_id`);

--
-- Indexes for table `RecordType`
--
ALTER TABLE `RecordType`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_4CF53BF05E237E06` (`name`);

--
-- Indexes for table `RecordTypeField`
--
ALTER TABLE `RecordTypeField`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `record_type_field_unique_record_type_field_type_name_idx` (`recordType_id`,`fieldType_id`,`name`), ADD KEY `IDX_1AE9F493919E14F2` (`recordType_id`), ADD KEY `IDX_1AE9F493D986DE8E` (`fieldType_id`), ADD KEY `IDX_1AE9F493E5F61522` (`referencedRecordType_id`);

--
-- Indexes for table `StoredFile`
--
ALTER TABLE `StoredFile`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `TableCell`
--
ALTER TABLE `TableCell`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_ADCA39E183A269F2` (`row_id`), ADD KEY `IDX_ADCA39E1BE8E8ED5` (`column_id`);

--
-- Indexes for table `TableColumn`
--
ALTER TABLE `TableColumn`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_7AA72C7B6D5405D6` (`recordTypeField_id`);

--
-- Indexes for table `TableRow`
--
ALTER TABLE `TableRow`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_CFB53F019A3A1D29` (`fieldValue_id`);

--
-- Indexes for table `tag`
--
ALTER TABLE `tag`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `tag_name_unique_idx` (`name`);

--
-- Indexes for table `Thread`
--
ALTER TABLE `Thread`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Vote`
--
ALTER TABLE `Vote`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_FA222A5AF8697D13` (`comment_id`), ADD KEY `IDX_FA222A5AEBB4B8AD` (`voter_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acl_classes`
--
ALTER TABLE `acl_classes`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `acl_entries`
--
ALTER TABLE `acl_entries`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `acl_object_identities`
--
ALTER TABLE `acl_object_identities`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `acl_security_identities`
--
ALTER TABLE `acl_security_identities`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `Choice`
--
ALTER TABLE `Choice`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `Comment`
--
ALTER TABLE `Comment`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Favorite`
--
ALTER TABLE `Favorite`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `FavoriteType`
--
ALTER TABLE `FavoriteType`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `FieldType`
--
ALTER TABLE `FieldType`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=76;
--
-- AUTO_INCREMENT for table `FieldValue`
--
ALTER TABLE `FieldValue`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1451;
--
-- AUTO_INCREMENT for table `fos_group`
--
ALTER TABLE `fos_group`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `fos_user`
--
ALTER TABLE `fos_user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `Record`
--
ALTER TABLE `Record`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6428;
--
-- AUTO_INCREMENT for table `RecordTag`
--
ALTER TABLE `RecordTag`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `RecordType`
--
ALTER TABLE `RecordType`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=213;
--
-- AUTO_INCREMENT for table `RecordTypeField`
--
ALTER TABLE `RecordTypeField`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `StoredFile`
--
ALTER TABLE `StoredFile`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=205;
--
-- AUTO_INCREMENT for table `TableCell`
--
ALTER TABLE `TableCell`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=573;
--
-- AUTO_INCREMENT for table `TableColumn`
--
ALTER TABLE `TableColumn`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `TableRow`
--
ALTER TABLE `TableRow`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=287;
--
-- AUTO_INCREMENT for table `tag`
--
ALTER TABLE `tag`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `Vote`
--
ALTER TABLE `Vote`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `acl_entries`
--
ALTER TABLE `acl_entries`
ADD CONSTRAINT `FK_46C8B8063D9AB4A6` FOREIGN KEY (`object_identity_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `FK_46C8B806DF9183C9` FOREIGN KEY (`security_identity_id`) REFERENCES `acl_security_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `FK_46C8B806EA000B10` FOREIGN KEY (`class_id`) REFERENCES `acl_classes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `acl_object_identities`
--
ALTER TABLE `acl_object_identities`
ADD CONSTRAINT `FK_9407E54977FA751A` FOREIGN KEY (`parent_object_identity_id`) REFERENCES `acl_object_identities` (`id`);

--
-- Constraints for table `acl_object_identity_ancestors`
--
ALTER TABLE `acl_object_identity_ancestors`
ADD CONSTRAINT `FK_825DE2993D9AB4A6` FOREIGN KEY (`object_identity_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `FK_825DE299C671CEA1` FOREIGN KEY (`ancestor_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `Choice`
--
ALTER TABLE `Choice`
ADD CONSTRAINT `FK_C6075FA46D5405D6` FOREIGN KEY (`recordTypeField_id`) REFERENCES `RecordTypeField` (`id`);

--
-- Constraints for table `Comment`
--
ALTER TABLE `Comment`
ADD CONSTRAINT `FK_5BC96BF0E2904019` FOREIGN KEY (`thread_id`) REFERENCES `Thread` (`id`),
ADD CONSTRAINT `FK_5BC96BF0F675F31B` FOREIGN KEY (`author_id`) REFERENCES `fos_user` (`id`);

--
-- Constraints for table `Favorite`
--
ALTER TABLE `Favorite`
ADD CONSTRAINT `FK_91B3EC8FA76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`),
ADD CONSTRAINT `FK_91B3EC8FC54C8C93` FOREIGN KEY (`type_id`) REFERENCES `FavoriteType` (`id`);

--
-- Constraints for table `FieldValue`
--
ALTER TABLE `FieldValue`
ADD CONSTRAINT `FK_DD026D414DFD750C` FOREIGN KEY (`record_id`) REFERENCES `Record` (`id`),
ADD CONSTRAINT `FK_DD026D416D5405D6` FOREIGN KEY (`recordTypeField_id`) REFERENCES `RecordTypeField` (`id`);

--
-- Constraints for table `FieldValueBoolean`
--
ALTER TABLE `FieldValueBoolean`
ADD CONSTRAINT `FK_BDD0E199BF396750` FOREIGN KEY (`id`) REFERENCES `FieldValue` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `FieldValueChoice`
--
ALTER TABLE `FieldValueChoice`
ADD CONSTRAINT `FK_4CF95B79BF396750` FOREIGN KEY (`id`) REFERENCES `FieldValue` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `fieldvaluechoice_choice`
--
ALTER TABLE `fieldvaluechoice_choice`
ADD CONSTRAINT `FK_5F1AFA29998666D1` FOREIGN KEY (`choice_id`) REFERENCES `Choice` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `FK_5F1AFA29C95ED4EB` FOREIGN KEY (`fieldvaluechoice_id`) REFERENCES `FieldValueChoice` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `FieldValueDate`
--
ALTER TABLE `FieldValueDate`
ADD CONSTRAINT `FK_A35F3384BF396750` FOREIGN KEY (`id`) REFERENCES `FieldValue` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `FieldValueDateTime`
--
ALTER TABLE `FieldValueDateTime`
ADD CONSTRAINT `FK_F94CFE17BF396750` FOREIGN KEY (`id`) REFERENCES `FieldValue` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `FieldValueFile`
--
ALTER TABLE `FieldValueFile`
ADD CONSTRAINT `FK_855E32EE9DD92E3C` FOREIGN KEY (`storedFile_id`) REFERENCES `StoredFile` (`id`),
ADD CONSTRAINT `FK_855E32EEBF396750` FOREIGN KEY (`id`) REFERENCES `FieldValue` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `FieldValueFileGallery`
--
ALTER TABLE `FieldValueFileGallery`
ADD CONSTRAINT `FK_CF1F0DE9BF396750` FOREIGN KEY (`id`) REFERENCES `FieldValue` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `fieldvaluefilegallery_storedfile`
--
ALTER TABLE `fieldvaluefilegallery_storedfile`
ADD CONSTRAINT `FK_F1678E13526417A0` FOREIGN KEY (`storedfile_id`) REFERENCES `StoredFile` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `FK_F1678E13A2BFAE0E` FOREIGN KEY (`fieldvaluefilegallery_id`) REFERENCES `FieldValueFileGallery` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `FieldValueFloat`
--
ALTER TABLE `FieldValueFloat`
ADD CONSTRAINT `FK_93A9408ABF396750` FOREIGN KEY (`id`) REFERENCES `FieldValue` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `FieldValueInteger`
--
ALTER TABLE `FieldValueInteger`
ADD CONSTRAINT `FK_6D5AC2FABF396750` FOREIGN KEY (`id`) REFERENCES `FieldValue` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `FieldValueRecords`
--
ALTER TABLE `FieldValueRecords`
ADD CONSTRAINT `FK_ABC831B7BF396750` FOREIGN KEY (`id`) REFERENCES `FieldValue` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `fieldvaluerecords_record`
--
ALTER TABLE `fieldvaluerecords_record`
ADD CONSTRAINT `FK_1ECA109C28D60D7B` FOREIGN KEY (`fieldvaluerecords_id`) REFERENCES `FieldValueRecords` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `FK_1ECA109C4DFD750C` FOREIGN KEY (`record_id`) REFERENCES `Record` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `FieldValueString`
--
ALTER TABLE `FieldValueString`
ADD CONSTRAINT `FK_13ECB342BF396750` FOREIGN KEY (`id`) REFERENCES `FieldValue` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `FieldValueTable`
--
ALTER TABLE `FieldValueTable`
ADD CONSTRAINT `FK_AC259159BF396750` FOREIGN KEY (`id`) REFERENCES `FieldValue` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `FieldValueText`
--
ALTER TABLE `FieldValueText`
ADD CONSTRAINT `FK_324AA339BF396750` FOREIGN KEY (`id`) REFERENCES `FieldValue` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `FieldValueTime`
--
ALTER TABLE `FieldValueTime`
ADD CONSTRAINT `FK_66559CBBBF396750` FOREIGN KEY (`id`) REFERENCES `FieldValue` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `fos_user_user_group`
--
ALTER TABLE `fos_user_user_group`
ADD CONSTRAINT `FK_B3C77447A76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`),
ADD CONSTRAINT `FK_B3C77447FE54D947` FOREIGN KEY (`group_id`) REFERENCES `fos_group` (`id`);

--
-- Constraints for table `Record`
--
ALTER TABLE `Record`
ADD CONSTRAINT `FK_9C989AA7919E14F2` FOREIGN KEY (`recordType_id`) REFERENCES `RecordType` (`id`);

--
-- Constraints for table `RecordTag`
--
ALTER TABLE `RecordTag`
ADD CONSTRAINT `FK_9289C3FC4DFD750C` FOREIGN KEY (`record_id`) REFERENCES `Record` (`id`),
ADD CONSTRAINT `FK_9289C3FCBAD26311` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`id`);

--
-- Constraints for table `RecordTypeField`
--
ALTER TABLE `RecordTypeField`
ADD CONSTRAINT `FK_1AE9F493919E14F2` FOREIGN KEY (`recordType_id`) REFERENCES `RecordType` (`id`),
ADD CONSTRAINT `FK_1AE9F493D986DE8E` FOREIGN KEY (`fieldType_id`) REFERENCES `FieldType` (`id`),
ADD CONSTRAINT `FK_1AE9F493E5F61522` FOREIGN KEY (`referencedRecordType_id`) REFERENCES `RecordType` (`id`);

--
-- Constraints for table `TableCell`
--
ALTER TABLE `TableCell`
ADD CONSTRAINT `FK_ADCA39E183A269F2` FOREIGN KEY (`row_id`) REFERENCES `TableRow` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `FK_ADCA39E1BE8E8ED5` FOREIGN KEY (`column_id`) REFERENCES `TableColumn` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `TableColumn`
--
ALTER TABLE `TableColumn`
ADD CONSTRAINT `FK_7AA72C7B6D5405D6` FOREIGN KEY (`recordTypeField_id`) REFERENCES `RecordTypeField` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `TableRow`
--
ALTER TABLE `TableRow`
ADD CONSTRAINT `FK_CFB53F019A3A1D29` FOREIGN KEY (`fieldValue_id`) REFERENCES `FieldValueTable` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `Vote`
--
ALTER TABLE `Vote`
ADD CONSTRAINT `FK_FA222A5AEBB4B8AD` FOREIGN KEY (`voter_id`) REFERENCES `fos_user` (`id`),
ADD CONSTRAINT `FK_FA222A5AF8697D13` FOREIGN KEY (`comment_id`) REFERENCES `Comment` (`id`);
SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
