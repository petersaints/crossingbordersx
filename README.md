CrossingBordersX
================================================================================
An ambitious document management system for conservation and restoration
researchers.
The project is licensed under the GNU GPLv3 license. Check the LICENSE file for
more information.

Install 
================================================================================
Requirements
--------------------------------------------------------------------------------
PHP >= 5.3.3 (5.4 or higher recommended)
* JSON support is required
* ctype support is required
* php.ini needs to have the date.timezone setting

PHP Extensions
--------------------------------------------------------------------------------
* imagick
* gd
* fileinfo
* intl
* json
* pdo
* pdo_mysql

PHP Options
--------------------------------------------------------------------------------
* post_max_size = 0
* upload_max_filesize = 0

Libraries
========
PHP
--------------------------------------------------------------------------------
* Symfony 2.6.x

JavaScript
--------------------------------------------------------------------------------
* jQuery 
* jQuery Dragtable
* jQuery UI
* jQuery UI Timepicker
* KineticJS
* Modernizr
* HTML2Canvas

Others
--------------------------------------------------------------------------------
* Bootstrap (CSS + JavaScript)
* wkhtmltopdf (external binary dependency)