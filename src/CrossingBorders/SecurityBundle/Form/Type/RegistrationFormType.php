<?php

namespace CrossingBorders\SecurityBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseType;
use EWZ\Bundle\RecaptchaBundle\Validator\Constraints\True;

class RegistrationFormType extends BaseType {
    public function buildForm(FormBuilderInterface $builder, array $options) {
        parent::buildForm($builder, $options);
        //Add Recaptcha
        $builder->add('recaptcha',
                      'ewz_recaptcha',
                       array(
                                'attr' => array(
                                        '           options' => array(
                                                                        'theme' => 'white'
                                                                     )
                                               ),
                                'mapped' => false,
                                'constraints' => array(new True())
                            )
                     );
        $builder->add('submit',
                      'submit');
    }
    public function getName() {
        return 'crossingbordersx_security_registration';
    }
}