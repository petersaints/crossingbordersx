<?php
namespace CrossingBorders\SecurityBundle\EventListener;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Doctrine\Bundle\DoctrineBundle\Registry;
/**
 * Listener responsible to change the redirection at the end of the password resetting
 */
class UserEventListener implements EventSubscriberInterface
{
    private $em;
    public function __construct(Registry $reg) {
        $this->em = $reg->getEntityManager();
    }
    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents() {
        return array(
            FOSUserEvents::REGISTRATION_SUCCESS => 'onRegistrationSuccess',
        );
    }
    public function onRegistrationSuccess(FormEvent $event) {
        $this->user = $event->getForm()->getData();
        $entity = $this->em->getRepository('CrossingBordersSecurityBundle:Group')->findOneByName('User');
        $this->user->addGroup($entity);
        $this->em->flush();
    }
}