<?php
namespace CrossingBorders\XBundle\Utilities;

use CrossingBorders\XBundle\Entity\Record;
use CrossingBorders\XBundle\Form\SearchAdvancedType;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * Search utilities that are shared accross different controllers/features
 * @author Pedro
 */
class SearchUtilities {
    public static function simpleSearchBuildQuery($form, $em, $defaultOrder = true) {
        $query = $em->createQueryBuilder()
                     ->select('DISTINCT r')
                     ->from('CrossingBordersXBundle:Record', 'r');
        if($defaultOrder) {
            $query = $query->addOrderBy('r.name');
        }
        if($form->get('name')->getData() !== null) {
            $query = $query->andWhere('r.name LIKE :search_name')                            
                           ->setParameter('search_name','%'.$form->get('name')->getData().'%');
        }
        if($form->get('tags')->getData() !== null) {
            $individualTags = array_unique(
                                            array_map(
                                                        'trim',
                                                        explode(
                                                                ",",
                                                                $form->get('tags')->getData()
                                                               )
                                                     )
                                          );
            foreach ($individualTags as $key => $tag) {
                $subQuery = $em->createQueryBuilder()
                               ->select('DISTINCT r'.$key)
                               ->from('CrossingBordersXBundle:Record', 'r'.$key)
                               ->join('r'.$key.'.recordTags', 'rtg'.$key)
                               ->join('rtg'.$key.'.tag', 'tg'.$key)
                               ->andWhere('tg'.$key.'.name LIKE :search_tag'.$key);
                $query = $query->andWhere($query->expr()->in('r.id', $subQuery->getDQL()))
                               ->setParameter('search_tag'.$key,'%'.$tag.'%');
            }
        }
        if($form->has('fields') && $form->get('fields')->getData() !== null) {                            
            $fieldValues = [];
            $fieldValues[] = $em->createQuery('SELECT fvs.id from CrossingBordersXBundle:FieldValueString fvs '
                                             .'WHERE fvs.value LIKE :search_fields')
                                             ->setParameter('search_fields','%'.$form->get('fields')->getData().'%')
                                             ->getResult();              
            $fieldValues[] = $em->createQuery('SELECT fvt.id from CrossingBordersXBundle:FieldValueText fvt '
                                             .'WHERE fvt.value LIKE :search_fields')
                                             ->setParameter('search_fields','%'.$form->get('fields')->getData().'%')
                                             ->getResult();
            $fieldValues[] = $em->createQuery('SELECT fvi.id from CrossingBordersXBundle:FieldValueInteger fvi '
                                             .'WHERE fvi.value LIKE :search_fields')
                                             ->setParameter('search_fields','%'.$form->get('fields')->getData().'%')
                                             ->getResult();
            $fieldValues[] = $em->createQuery('SELECT fvf.id from CrossingBordersXBundle:FieldValueFloat fvf '
                                             .'WHERE fvf.value LIKE :search_fields')
                                             ->setParameter('search_fields','%'.$form->get('fields')->getData().'%')
                                             ->getResult();
            $fieldValues[] = $em->createQuery('SELECT fvb.id from CrossingBordersXBundle:FieldValueBoolean fvb '
                                             .'WHERE fvb.value LIKE :search_fields')
                                             ->setParameter('search_fields','%'.$form->get('fields')->getData().'%')
                                             ->getResult();
            $fieldValues[] = $em->createQuery('SELECT fvd.id from CrossingBordersXBundle:FieldValueDate fvd '
                                             .'WHERE fvd.value LIKE :search_fields')
                                             ->setParameter('search_fields','%'.$form->get('fields')->getData().'%')
                                             ->getResult();
            $fieldValues[] = $em->createQuery('SELECT fvdt.id from CrossingBordersXBundle:FieldValueDateTime fvdt '
                                             .'WHERE fvdt.value LIKE :search_fields')
                                             ->setParameter('search_fields','%'.$form->get('fields')->getData().'%')
                                             ->getResult();
            $fieldValues[] = $em->createQuery('SELECT fvti.id from CrossingBordersXBundle:FieldValueTime fvti '
                                             .'WHERE fvti.value LIKE :search_fields')
                                             ->setParameter('search_fields','%'.$form->get('fields')->getData().'%')
                                             ->getResult();
            $fieldValues[] = $em->createQuery('SELECT fvfi.id from CrossingBordersXBundle:FieldValueFile fvfi '
                                             .'JOIN fvfi.storedFile sf '
                                             .'WHERE sf.originalName LIKE :search_fields')
                                             ->setParameter('search_fields','%'.$form->get('fields')->getData().'%')
                                             ->getResult();
            $fieldValues[] = $em->createQuery('SELECT fvtb.id from CrossingBordersXBundle:FieldValueTable fvtb '
                                             .'JOIN fvtb.rows fvtbr '
                                             .'JOIN fvtbr.cells fvtbrc '
                                             .'WHERE fvtbrc.value LIKE :search_fields')
                                             ->setParameter('search_fields','%'.$form->get('fields')->getData().'%')
                                             ->getResult();
            $fieldValues[] = $em->createQuery('SELECT fvtb.id from CrossingBordersXBundle:FieldValueTable fvtb '
                                             .'JOIN fvtb.rows fvtbr '
                                             .'JOIN fvtbr.cells fvtbrc '
                                             .'JOIN fvtbrc.record fvtbrcr '
                                             .'WHERE fvtbrcr.name LIKE :search_fields')
                                             ->setParameter('search_fields','%'.$form->get('fields')->getData().'%')
                                             ->getResult();
            $fieldValues[] = $em->createQuery('SELECT fvc.id from CrossingBordersXBundle:FieldValueChoice fvc '
                                             .'JOIN fvc.value fvcs '
                                             .'WHERE fvcs.name LIKE :search_fields')
                                             ->setParameter('search_fields','%'.$form->get('fields')->getData().'%')
                                             ->getResult();
            $fieldValues[] = $em->createQuery('SELECT fvr.id from CrossingBordersXBundle:FieldValueRecords fvr '
                                             .'JOIN fvr.value fvrr '
                                             .'WHERE fvrr.name LIKE :search_fields')
                                             ->setParameter('search_fields','%'.$form->get('fields')->getData().'%')
                                             ->getResult();
            if(!empty($fieldValues)) {
                $mergedFieldValues = call_user_func_array('array_merge', $fieldValues);
                $query = $query->join('r.fieldValues', 'fv')
                               ->andWhere('fv.id IN (:field_values)')
                               ->setParameter('field_values',$mergedFieldValues);
            }
        }
        if($form->has('recordType') && $form->get('recordType')->getData() !== null) {
            $query = $query->join('r.recordType', 'rt')
                           ->andWhere('rt.name LIKE :search_record_type')                            
                           ->setParameter('search_record_type','%'.$form->get('recordType')->getData().'%');
        }
        return $query;
    }
    /** 
     * @param type $form
     * @param type $recordType
     * @return type
     */
    public static function advancedSearchBuildQuery($form, $recordType, $em, $recursion = 0) {
        $query = SearchUtilities::simpleSearchBuildQuery($form, $em, true)->join('r.recordType', 'rt')
                                                                          ->andWhere('rt.id = :record_type_id')
                                                                          ->setParameter('record_type_id',$recordType->getId())
                                                                          ->join('rt.recordTypeFields', 'rtf')
                                                                          ->join('rtf.fieldType', 'ft');
        $fieldValues = [];
        foreach ($recordType->getRecordTypeFields() as $index => $recordTypeField) {
            $name = str_replace(' ', '_',  $recordTypeField->getName());
            if($form->has($name)) {
                $recordTypeFieldId = $recordTypeField->getId();
                $value = $form->get($name)->getData();
                $filled = $value !== null && !empty($value);
                if($filled) {
                    switch ($recordTypeField->getFieldType()->getName()) {
                        case "String":
                            $fieldValues[] = $em->createQuery('SELECT fvs.id from CrossingBordersXBundle:FieldValueString fvs '
                                                             .'JOIN fvs.recordTypeField rtf '
                                                             .'WHERE fvs.value LIKE :field_value '
                                                             .'AND rtf.id = :field_name')
                                                             ->setParameter('field_value','%'.$value.'%')
                                                             ->setParameter('field_name',$recordTypeFieldId)
                                                             ->getResult();
                            break;
                        case "Text":
                            $fieldValues[] = $em->createQuery('SELECT fvt.id from CrossingBordersXBundle:FieldValueText fvt '
                                                             .'JOIN fvt.recordTypeField rtf '
                                                             .'WHERE fvt.value LIKE :field_value '
                                                             .'AND rtf.id = :field_name')
                                                             ->setParameter('field_value','%'.$value.'%')
                                                             ->setParameter('field_name',$recordTypeFieldId)
                                                             ->getResult();
                            break;
                        case "Integer":
                            $equals = $value['equals'];
                            $minimum = $value['minimum'];
                            $maximum = $value['maximum'];
                            $fieldQuery = $em->createQueryBuilder()
                                             ->select('fvi.id')
                                             ->from('CrossingBordersXBundle:FieldValueInteger', 'fvi')
                                             ->join('fvi.recordTypeField', 'rtf')
                                             ->where('rtf.id = :field_name')
                                             ->orderBy('fvi.value')
                                             ->setParameter('field_name',$recordTypeFieldId);
                            if($equals !== null) {
                                $fieldQuery = $fieldQuery->andWhere('fvi.value = :field_value')
                                                         ->setParameter('field_value',$equals);
                            }
                            if($minimum !== null) {
                                $fieldQuery = $fieldQuery->andWhere('fvi.value >= :field_minimum')
                                                         ->setParameter('field_minimum',$minimum);
                            }
                            if($maximum !== null) {
                                $fieldQuery = $fieldQuery->andWhere('fvi.value <= :field_maximum')
                                                         ->setParameter('field_maximum',$maximum);
                            }
                            if($equals !== null
                            ||$minimum !== null
                            ||$maximum !== null) {
                                $fieldValues[] = $fieldQuery->getQuery()
                                                            ->getResult();
                            }
                            break;
                         case "Float":
                            $equals = $value['equals'];
                            $minimum = $value['minimum'];
                            $maximum = $value['maximum'];
                            $fieldQuery = $em->createQueryBuilder()
                                             ->select('fvf.id')
                                             ->from('CrossingBordersXBundle:FieldValueFloat', 'fvf')
                                             ->join('fvf.recordTypeField', 'rtf')
                                             ->where('rtf.id = :field_name')
                                             ->orderBy('fvf.value')
                                             ->setParameter('field_name',$recordTypeFieldId);
                            if($equals !== null) {
                                $fieldQuery = $fieldQuery->andWhere('fvf.value = :field_value')
                                                         ->setParameter('field_value',$equals);
                            }
                            if($minimum !== null) {
                                $fieldQuery = $fieldQuery->andWhere('fvf.value >= :field_minimum')
                                                         ->setParameter('field_minimum',$minimum);
                            }
                            if($maximum !== null) {
                                $fieldQuery = $fieldQuery->andWhere('fvf.value <= :field_maximum')
                                                         ->setParameter('field_maximum',$maximum);
                            }
                            if($equals !== null
                            ||$minimum !== null
                            ||$maximum !== null) {
                                $fieldValues[] = $fieldQuery->getQuery()
                                                            ->getResult();
                            }
                            break;
                        case "Boolean":
                            $fieldValues[] = $em->createQuery('SELECT fvb.id from CrossingBordersXBundle:FieldValueBoolean fvb '
                                                             .'JOIN fvb.recordTypeField rtf '
                                                             .'WHERE fvb.value = :field_value '
                                                             .'AND rtf.id = :field_name')
                                                             ->setParameter('field_value',$value === 'true')
                                                             ->setParameter('field_name',$recordTypeFieldId)
                                                             ->getResult();
                            break;
                        case "Date Time":
                        case "Date":
                        case "Time":
                            $entityName = "FieldValue";
                            if($recordTypeField->getFieldType()->getName() === "Date") {
                                $entityName .= 'Date';
                            } else if($recordTypeField->getFieldType()->getName() === "Time") {
                                $entityName .= 'Time';
                            } else if($recordTypeField->getFieldType()->getName() === "Date Time") {
                                $entityName .= 'DateTime';
                            }
                            $equals  = $value['equals'];
                            $minimum = $value['minimum'];
                            $maximum = $value['maximum'];
                            $fieldQuery = $em->createQueryBuilder()
                                             ->select('fvdt.id')
                                             ->from('CrossingBordersXBundle:'.$entityName, 'fvdt')
                                             ->join('fvdt.recordTypeField', 'rtf')
                                             ->where('rtf.id = :field_name')
                                             ->setParameter('field_name',$recordTypeFieldId);
                            if($equals !== null) {
                                $fieldQuery = $fieldQuery->andWhere('fvdt.value = :field_value')
                                                         ->setParameter('field_value',$equals);
                            }
                            if($minimum !== null) {
                                $fieldQuery = $fieldQuery->andWhere('fvdt.value >= :field_minimum')
                                                         ->setParameter('field_minimum',$minimum);
                            }
                            if($maximum !== null) {
                                $fieldQuery = $fieldQuery->andWhere('fvdt.value <= :field_maximum')
                                                         ->setParameter('field_maximum',$maximum);
                            }
                            if($equals !== null
                            ||$minimum !== null
                            ||$maximum !== null) {
                                $fieldValues[] = $fieldQuery->getQuery()
                                                            ->getResult();
                            }
                            break;
                        case "Image":
                        case "Audio":
                        case "Video":
                        case "File":
                            $fieldValues[] = $em->createQuery('SELECT fvf.id from CrossingBordersXBundle:FieldValueFile fvf '
                                                             .'JOIN fvf.recordTypeField rtf '
                                                             .'JOIN fvf.storedFile sf '
                                                             .'WHERE sf.originalName LIKE :field_value '
                                                             .'AND rtf.id = :field_name')
                                                             ->setParameter('field_value','%'.$value.'%')
                                                             ->setParameter('field_name',$recordTypeFieldId)
                                                             ->getResult();
                            break;
                        case "Choice":
                            $fieldQuery = $em->createQueryBuilder()
                                             ->select('fvc.id')
                                             ->from('CrossingBordersXBundle:FieldValueChoice', 'fvc')
                                             ->join('fvc.recordTypeField', 'rtf')
                                             ->where('rtf.id = :field_name')
                                             ->setParameter('field_name',$recordTypeFieldId);                            
                            if($value instanceof ArrayCollection && !$value->empty()) {
                                foreach ($value as $index => $choice) {
                                    $fieldQuery = $fieldQuery->join('fvc.value','fvcs'.$index)
                                                             ->andWhere('fvcs'.$index.' = :field_value'.$index)
                                                             ->setParameter('field_value'.$index, $choice);
                                }
                                $fieldValues[] = $fieldQuery->getQuery()
                                                            ->getResult();
                            } else if(!$value instanceof ArrayCollection) {
                                $fieldValues[] = $fieldQuery->join('fvc.value','fvcs')
                                                            ->andWhere('fvcs = :field_value')
                                                            ->setParameter('field_value',$value)
                                                            ->getQuery()
                                                            ->getResult();
                            }              
                            break;
                        case "Table":
                            $fieldQuery = $em->createQueryBuilder()
                                             ->select('fvtb.id')
                                             ->from('CrossingBordersXBundle:FieldValueTable', 'fvtb')
                                             ->join('fvtb.recordTypeField', 'rtf')
                                             ->where('rtf.id = :field_name')
                                             ->setParameter('field_name',$recordTypeFieldId);
                            $empty = true;
                            $index = 0;
                            foreach ($value as $column => $cell) {
                                if($cell !== null) {
                                    $fieldQuery = $fieldQuery->join('fvtb.rows', 'fvtbr'.$index)
                                                             ->join('fvtbr'.$index.'.cells', 'fvtbrc'.$index)
                                                             ->leftJoin('fvtbrc'.$index.'.column', 'fvtbrcc'.$index)
                                                             ->leftJoin('fvtbrc'.$index.'.record', 'fvtbrcr'.$index)
                                                             ->andWhere('fvtbrc'.$index.'.value LIKE :field_value'.$index.' OR fvtbrc'.$index.'.record = :field_record'.$index)
                                                             ->andWhere('fvtbrcc'.$index.'.name = :col_name'.$index)
                                                             ->setParameter('field_value'.$index,'%'.$cell.'%')
                                                             ->setParameter('field_record'.$index,$cell)
                                                             ->setParameter('col_name'.$index, str_replace('_',' ',$column));
                                    $empty = false;
                                }
                                $index++;
                            }
                            if(!$empty) {
                                $fieldValues[] = $fieldQuery->getQuery()
                                                            ->getResult();
                            }
                            break;
                        case "Record":
                            if($recursion < SearchAdvancedType::$maxRecursion) {
                                $subForm = $form->get($name);
                                if($subForm->isValid()) {
                                    $subQuery = SearchUtilities::advancedSearchBuildQuery($subForm, $recordTypeField->getReferencedRecordType(), $em, $recursion+1);
                                    $subRecords = $subQuery->getQuery()
                                                           ->getResult();        
                                    $fieldValues[] = $em->createQuery('SELECT fvr.id from CrossingBordersXBundle:FieldValueRecords fvr '
                                                                     .'JOIN fvr.recordTypeField rtf '
                                                                     .'JOIN fvr.value rr '
                                                                     .'WHERE rr IN (:field_value) '
                                                                     .'AND rtf.id = :field_name')
                                                                     ->setParameter('field_value',$subRecords)
                                                                     ->setParameter('field_name',$recordTypeFieldId)
                                                                     ->getResult();   
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
        }
        /*foreach ($fieldValues as $index => $value) {
            $query = $query->leftJoin('r.fieldValues', "fv$index")
                           ->andWhere("fv$index.id IN (:field_values$index)")
                           ->setParameter("field_values$index",$value);
        }*/
        if(!empty($fieldValues)) {
            $mergedFieldValues = call_user_func_array('array_merge', $fieldValues);
            $query = $query->join('r.fieldValues', 'fva')
                           ->andWhere('fva.id IN (:field_values_advanced)')
                           ->setParameter('field_values_advanced', $mergedFieldValues);
        }
        foreach ($form as $name => $field) {
            $index = 0;
            if(Record::checkIfNameIsRecordReference($name)) {
                $tokens = Record::getRecordReferenceTokens($name);
                if($tokens !== null) {
                    $subRecords = $field->getData()->toArray();
                    if(!empty($subRecords)) {
                        $referencingRecordsIds = $em->createQuery('SELECT rr.id FROM CrossingBordersXBundle:FieldValueRecords fvr '
                                                                 .'INNER JOIN fvr.recordTypeField rtf '
                                                                 .'INNER JOIN fvr.value rr '
                                                                 .'INNER JOIN fvr.record r '
                                                                 .'WHERE r IN (:field_value) '
                                                                 .'AND rtf.name=:field_name '
                                                                 .'AND rtf.referencedRecordType = :referenced_record_type '
                                                                 .'AND rtf.recordType = :record_type '
                                                                 .'GROUP BY rr '
                                                                 .'HAVING count(rr) = :number_of_selected_records')
                                                                 ->setParameter('field_value',$subRecords)
                                                                 ->setParameter('field_name',$tokens['recordTypeFieldName'])
                                                                 ->setParameter('referenced_record_type',$recordType)
                                                                 ->setParameter('record_type', $tokens['recordTypeId'])
                                                                 ->setParameter('number_of_selected_records', count($subRecords))
                                                                 ->getResult();
                        $query = $query->andWhere("r.id IN (:referencing_records$index)")
                                       ->setParameter(":referencing_records$index", $referencingRecordsIds);
                    }
                }
                $index++;
            }
        }
        return $query;
    }
    private static function isFormEmpty($form) {
        foreach ($form as $field) {
            $data = $field->getData();
            if((!$data instanceof ArrayCollection && !empty($data)) || ($data instanceof ArrayCollection && !$data->isEmpty())) {
                return false; 
            } else if($field->count() > 0){
                return SearchUtilities::isFormEmpty($field); 
            }
        }
        return true;
    }
}