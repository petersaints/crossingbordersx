<?php

namespace CrossingBorders\XBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use CrossingBorders\XBundle\DependencyInjection\Compiler\CustomCommentSecurityCompilerPass;

class CrossingBordersXBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);
        $container->addCompilerPass(new CustomCommentSecurityCompilerPass());
    }
}
