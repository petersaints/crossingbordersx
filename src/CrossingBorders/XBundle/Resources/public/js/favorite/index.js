var Favorites = {
  deleteConfirmation: 'Are you sure you want to delete this favorite?',
  deleteError: 'The selected favorite could not be deleted.',
  init: function() {
      $(document).on('click','.edit-favorite', function () {
            var row = $(this).closest('.row');
            var route = row.data('edit-route');
            var csrfToken = row.data('csrf-token');
            console.log('Edit Button Clicked with Route: '+ route +' CSRF Token: '+csrfToken);

            var cell = $('.favorite-cell', row);          
            var shown = $('.show', cell);
            var hidden = $('.hidden', cell);

            shown.removeClass('show').addClass('hidden');
            hidden.removeClass('hidden').addClass('show');             
            $('.favorite-edit-form .submit', cell).click(function(e) {
                e.preventDefault();
                var newName = $('.favorite-edit-name', cell).val();
                console.log('Changing Favorite Name to: ' + newName);   
                $.ajax({
                    url: route,
                    type: 'PUT',
                    data: { name: newName, csrfToken: csrfToken }
                  }).done(function() {
                    console.log('Edit Successful');
                    shown.removeClass('hidden').addClass('show');
                    hidden.removeClass('show').addClass('hidden'); 
                    $('.favorite-name', cell).text(newName);
                  }).fail(function() {
                    alert(Favorites.editError);
                  });
                shown.removeClass('hidden').addClass('show');
                hidden.removeClass('show').addClass('hidden');                
            });
      });
      $(document).on('click','.delete-favorite', function () {
          var confirmation = confirm(Favorites.deleteConfirmation);
          if (confirmation === true) {
            var row = $(this).closest('.row');
            var route = row.data('delete-route');
            var csrfToken = row.data('csrf-token');
            console.log('Delete Button Clicked with Route: '+ route + ' CSRF Token: '+csrfToken);
            $.ajax({
              url: route,
              type: 'DELETE',
              data: { csrfToken: csrfToken }
            }).done(function() {
              console.log('Delete Successful');
                row.remove();
            }).fail(function() {
                alert(Favorites.deleteError);
            });
        } 
      });
      Favorites.initPagination();
    },
    //Pagination
    loadPage: function(button, url){
        button.addClass('active');
        console.log('Load URL: ' +url);
        $.get(url, function(data) {
            if(data.length === 0) {
                alert(Favorites.noMoreFavorites);
                button.attr('disabled', 'disabled');
            } else {
                $('.favorites').html($('.favorites').html()+data);
            }
            button.removeClass('active');
        });
    },
    initPagination: function() {
        var rootPath = window.location.pathname.match(/.+(?=\/favorite)/g);
        var favoriteTypeMatch = window.location.pathname.match(/(?:\/favorite\/)(\d+)/); 
        var currPage = 1;
        var loadButton = $('.favorites-load-more');
        var clickFunction = function(){
            var url = window.location.protocol+'//'+window.location.host+rootPath+'/favorite/page/'+currPage;
            if(favoriteTypeMatch !== null) {
                url += '/'+favoriteTypeMatch[1];
            }
            Favorites.loadPage(loadButton, url);
            currPage++;
        };
        clickFunction();
        loadButton.click(clickFunction);
    }
};
$(document).ready(function() {
    console.log('Search Add Loaded...');
    Favorites.init();
});