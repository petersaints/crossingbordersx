var Favorites = {
  csrfToken: '[TOKEN]',
  saveRoute: '/',
  savePrompt: 'Please enter a unique favorite name',
  init: function() {
      var url = Utilities.currentRoute+window.location.search+window.location.hash;
      var page = url.match(/([^\/]+)\/([^\/]+)/);
      var type = 'Generic';
      switch(page[2]) {
        case "simple":   type='Simple';   break;
        case "advanced": type='Advanced'; break;
        default: break;
      }
      switch(page[1]) {
        case "search":  type+=' Search';  break;
        case "gallery": type+=' Gallery'; break;
        default: break;
      }
      $('.save-search').click(function () {
        var name = prompt(Favorites.savePrompt);
        if(name !== null) {
            console.log('Save Search Name: '+name+' Type: '+type+' URL: '+url);
            var saveURL = Favorites.saveRoute;
            $.post(saveURL, {name: name, url: url, type: type, csrfToken: Favorites.csrfToken}, function(data) {
                console.log('Save Status: '+ data.status +' Result: '+data);
            }, 'json');
        }
    });
  }  
};

$(document).ready(function() {
    console.log('Search Save Loaded...');
    Favorites.init();
});