var Record = {
    strings: {},
    init: function () {
        console.log("Record Form Code Init");
        //Init Textarea Editors
        $('form[name="crossingborders_xbundle_record"] textarea').ckeditor({
            //TODO: Enable only the relevant buttons
            /*toolbar: [
		{ name: 'document', items: [ 'Source', '-', 'NewPage', 'Preview', '-', 'Templates' ] },	// Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
		[ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],			// Defines toolbar group without name.
		'/',																					// Line break - next group will be placed in new line.
		{ name: 'basicstyles', items: [ 'Bold', 'Italic' ] }
            ],*/
            toolbarGroups: [
                { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
                { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
                { name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ] },
                { name: 'forms' },
                '/',
                { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
                { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
                { name: 'links' },
                { name: 'insert' },
                '/',
                { name: 'styles' },
                { name: 'colors' },
                { name: 'tools' },
                { name: 'others' },
                { name: 'about' }
            ]
        });
        //Init Date Picker
        if (!Modernizr.inputtypes.date) {
            console.log('No native support for input type time');
            $('form[name="crossingborders_xbundle_record"] input[type="date"]').datepicker({ dateFormat: 'yy-mm-dd' });
        }
        if (!Modernizr.inputtypes.time) {
            console.log('No native support for input type date');
            $('form[name="crossingborders_xbundle_record"] input[type="time"]').timepicker();
        }
        //Init File View
        $(".form-file").each(function() {
            var form = $(this);
            $(".form-file-delete", form).click(function() {
                console.log("Delete...");
                $(this).addClass("hidden");
                $(".form-file-undo", form).removeClass("hidden");
                $(".form-file-link", form).addClass("hidden");
                $("input[type=file]",form).replaceWith(form.data('form-hidden'));
            });
            $(".form-file-undo", form).click(function() {
                console.log("Undo...");
                $(this).addClass("hidden");
                $(".form-file-delete", form).removeClass("hidden");
                $(".form-file-link", form).removeClass("hidden");
                $("input[type=hidden]",form).replaceWith(form.data('form-file'));
            });
        });
        Record.initTables();
    },
    initTables: function() {
        console.log('Initializing Tables');
        var tables = $('.table-form');
        tables.each(function() {
            var rows = $('tbody tr',$(this)).size();
            $(this).data('rows',rows);
            
            var columns = $('thead tr th',$(this)).size();
            $(this).data('columns',columns);
        });
        var addRow = $('<a href="#" class="add_row_link btn btn-success">'+Record.strings.addRow+'</a>');
        var addRowCell = $('<td></td>').append(addRow);
        $('tfoot',tables).append(addRowCell);
        $(document).on('click','.add_row_link', function(e) {
            e.preventDefault();
            var tableForm = $(this).closest('.table-form');
            var rows = tableForm.data('rows');
            var rowPrototype = tableForm.data('prototype-row');
            var row = rowPrototype.replace(/__table__name__/g, rows);
            //var newRow = jQuery.parseHTML(row);
            $('tbody', tableForm).append(row);
            tableForm.data('rows',rows+1);
        });
        $(document).on("click",'.remove_row_link',function(e) {
            $(this).closest('tr').remove();
        });
    }
};
$(document).ready(function() {
    Record.init();
});