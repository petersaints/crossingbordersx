var Utilities = {
    currentRoute: '/',
    exportToPDFRoute: '/utility/export/pdf',
    svgCheck: function() {
        if(!Modernizr.svg) {
            console.log("SVG not supported... falling back to PNG");
            $('img[src*="svg"]').attr('src', function() {
                return $(this).attr('src').replace('.svg', '.png');
            });
        }
    },
    initDragtable: function() {
        $('.dragtable').dragtable();
    },
    initSortable: function() {
        $('.sortable').sortable();
        $('.sortable').disableSelection();
    },
    initPrintButton: function() {
        $('.print-page').click(function() {
            window.print();
        });
    },
    initExportToPDFButton: function() {
        $('.export-page-to-pdf').click(function() {
            var url = Utilities.currentRoute+window.location.search+window.location.hash;
            console.log('Export to PDF: ' + url);
            window.location.href = Utilities.exportToPDFRoute+'?url='+url;
        });
    },
    /*NOTE: This could be expanded to beautify more stuff automagically without the need to edit templated */
    beautifyCommentButtons: function() {
        $('#fos_comment_thread button').addClass('btn btn-default');
        $('#fos_comment_thread').on('DOMSubtreeModified propertychange', function() {
            $('#fos_comment_thread button,\n\
               #fos_comment_thread input[type="button"],\n\
               #fos_comment_thread input[type="submit"],\n\
               #fos_comment_thread input[type="reset"]').each(function() {
                if(!$(this).hasClass('btn') && !$(this).hasClass('btn-default')) {
                    $(this).addClass('btn btn-default');
                }
            });
        });
    },
    init: function() {
        Utilities.svgCheck();
        Utilities.initDragtable();
        Utilities.initSortable();
        Utilities.initPrintButton();
        Utilities.initExportToPDFButton();
        Utilities.beautifyCommentButtons();
    },
    stringContains: function(haystack,needle) { 
        return haystack.indexOf(needle) !== -1; 
    }
};
$(document).ready(function() {
    Utilities.init();
});