$(document).ready(function() {
    //Init Textarea Editors
    $('form[name="search_advanced"] textarea').ckeditor({
        //TODO: Enable only the relevant buttons
        /*toolbar: [
            { name: 'document', items: [ 'Source', '-', 'NewPage', 'Preview', '-', 'Templates' ] },	// Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
            [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],			// Defines toolbar group without name.
            '/',																					// Line break - next group will be placed in new line.
            { name: 'basicstyles', items: [ 'Bold', 'Italic' ] }
        ],*/
        toolbarGroups: [
            { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
            { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
            { name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ] },
            { name: 'forms' },
            '/',
            { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
            { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
            { name: 'links' },
            { name: 'insert' },
            '/',
            { name: 'styles' },
            { name: 'colors' },
            { name: 'tools' },
            { name: 'others' },
            { name: 'about' }
        ]
    });
    //Init Date Picker
    if (!Modernizr.inputtypes.date) {
        console.log('No native support for input type time');
        $('form[name="search_advanced"] input[type="date"]').datepicker({ dateFormat: 'yy-mm-dd' });
    }
    if (!Modernizr.inputtypes.time) {
        console.log('No native support for input type date');
        $('form[name="search_advanced"] input[type="time"]').timepicker();
    }
});