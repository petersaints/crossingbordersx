var RecordTypes = {
    strings: {},
    init: function () {
        // Get the ul that holds the collection of tags
        var collectionHolder = $('ul.field-types');
        // add a delete link to all of the existing tag form li elements
        collectionHolder.find('li').each(function() {
            RecordTypes.addDeleteLink($(this));
        });
        // setup an "add a tag" link
        var addLink = $('<a href="#" class="add_field_link btn btn-info">'+RecordTypes.strings.addField+'</a>');
        var addLinkLi = $('<li></li>').append(addLink);
        // add the "add a tag" anchor and li to the tags ul
        collectionHolder.append(addLinkLi);
        // count the current form inputs we have (e.g. 2), use that as the new
        // index when inserting a new item (e.g. 2)
        collectionHolder.data('index', collectionHolder.find('.field-type').length);
        addLink.on('click', function(e) {
            // prevent the link from creating a "#" on the URL
            e.preventDefault();
            // add a new tag form (see next code block)
            RecordTypes.addFieldTypeForm(collectionHolder, addLinkLi);
        });
        collectionHolder.on('change', '.field-type select', RecordTypes.bindFieldTypeChange);
        collectionHolder.on('click', '.add_choice_link', function(e) {
            e.preventDefault();
            RecordTypes.addChoiceForm(collectionHolder, $(this));
        });
        collectionHolder.on('click', '.add_tablecolumn_link', function(e) {
            e.preventDefault();
            RecordTypes.addTableColumnForm(collectionHolder, $(this));
        });
        RecordTypes.sortable();
    },
    addFieldTypeForm: function(collectionHolder, addLinkLi) {
        // Get the data-prototype explained earlier
        var prototype = collectionHolder.data('prototype');
        // get the new index
        var index = collectionHolder.data('index');
        // Replace '__name__' in the prototype's HTML to
        // instead be a number based on how many items we have
        var newForm = prototype.replace(/__name__/g, index);
        newForm = $(newForm).data('index',index);
        // increase the index with one for the next item
        collectionHolder.data('index', index+1);
        // Display the form in the page in an li, before the "Add a tag" link li
        var newFormElement = $('<li></li>').append(newForm);
        newFormElement.attr("class","field-type");
        addLinkLi.before(newFormElement);
        RecordTypes.addDeleteLink(newFormElement);
        RecordTypes.updateDisplayOrder();
    },
    addDeleteLink: function(formElement) {
        var removeForm = $('<a href="#" class="btn btn-warning delete-link">'+RecordTypes.strings.delete+'</a>');
        formElement.append(removeForm);
        removeForm.on('click', function(e) {
            // prevent the link from creating a "#" on the URL
            e.preventDefault();
            // remove the li for the tag form
            formElement.remove();
        });
    },
    bindFieldTypeChange: function(e) {
        if($(e.target).closest('.referenced-record-type').length === 0
        && $(e.target).closest('.tablecolumns').length === 0) {
            var collectionHolder = $('ul.field-types');
            var containerElement = $(e.target).parent();
            var option = $("select option:selected", containerElement).text();
            console.log("Selected Field Type: "+option);

            RecordTypes.removeMulipleSelection(containerElement);
            if(option === "Record") RecordTypes.addFieldTypeReferencedRecordTypeForm(containerElement, collectionHolder);
            else                    RecordTypes.removeFieldTypeReferencedRecordTypeForm(containerElement);
            if(option === "Choice") RecordTypes.addChoices(containerElement, collectionHolder);
            else                    RecordTypes.removeChoices(containerElement);
            if(option === "Table")  RecordTypes.addTableColumns(containerElement);
            else                    RecordTypes.removeTableColumns(containerElement);
        } else {
            console.log("Some other <select> that has nothing to do with the field type was changed");
        }
     },
    addFieldTypeReferencedRecordTypeForm: function(containerElement, collectionHolder) {
        var prototype = collectionHolder.data('prototype-record-type');
        var index = containerElement.closest('.field-type-form-elements').data('index');
        var newForm = prototype.replace(/__name__/g, index);
        var newFormDiv = $('<div class="referenced-record-type"></div>').append(newForm);
        var formElements = containerElement.closest('.field-type-form-elements');
        var prototypeMultipleSelection = collectionHolder.data('prototype-multiple-selection');
        var multipleSelection = prototypeMultipleSelection.replace(/__name__/g, index);
        formElements.append(multipleSelection);
        formElements.append(newFormDiv);
    },
    removeFieldTypeReferencedRecordTypeForm: function(containerElement) {
        $('.referenced-record-type', containerElement.closest('.field-type')).remove();   
    },
    addChoices: function(containerElement, collectionHolder) {
        var addLinkElement = $('<a href="#" class="add_choice_link btn btn-info">'+RecordTypes.strings.addChoice+'</a>');
        var containerDiv = $('<div class="choices"></div>');
        containerDiv.data('index', 0);
        var formList = $('<ul class="sortable"></ul>');
        formList = formList.append(addLinkElement);
        var formElements = containerElement.closest('.field-type-form-elements');
        var prototypeMultipleSelection = collectionHolder.data('prototype-multiple-selection');
        var index = formElements.data('index');
        var multipleSelection = prototypeMultipleSelection.replace(/__name__/g, index);
        formElements.append(multipleSelection);
        containerDiv.append(formList);
        formElements.append(containerDiv);
        RecordTypes.sortable();
    },
    removeChoices: function(containerElement) {
        $('.choices', containerElement.closest('.field-type')).remove(); 
    },
    addChoiceForm: function(collectionHolder, addLinkElement) {
        // Get the data-prototype explained earlier
        var prototype = collectionHolder.data('prototype-choice');
        // get the new index
        var index = addLinkElement.closest('.field-type-form-elements').data('index');
        // Replace '__name__' in the prototype's HTML to
        // instead be a number based on how many items we have
        var newForm = prototype.replace(/__name___choices/g, index+"_choices");
        newForm = newForm.replace(/__name__\]\[choices/g, index+"][choices");
        var choiceIndex = addLinkElement.closest('.choices').data('index');
        addLinkElement.closest('.choices').data('index',choiceIndex+1);
        newForm = newForm.replace(/__name__/g, choiceIndex);
        // Display the form in the page in an li, before the "Add a tag" link li
        var newFormElement = $('<li>'+newForm+'</li>');
        newFormElement.data("index",index);
        addLinkElement.before(newFormElement);
        RecordTypes.addDeleteLink(newFormElement);
        RecordTypes.updateDisplayOrder();
    },
    addTableColumns: function(containerElement) {
        var addLinkElement = $('<a href="#" class="add_tablecolumn_link btn btn-info">'+RecordTypes.strings.addTableColumn+'</a>');
        var containerDiv = $('<div class="tablecolumns"></div>');
        containerDiv.data('index', 0);
        var formList = $('<ul class="sortable"></ul>');
        formList = formList.append(addLinkElement);
        var formElements = containerElement.closest('.field-type-form-elements');
        containerDiv.append(formList);
        formElements.append(containerDiv);
        RecordTypes.sortable();
    },
    removeTableColumns: function(containerElement) {
        $('.tablecolumns', containerElement.closest('.field-type')).remove(); 
    },
    addTableColumnForm: function(collectionHolder, addLinkElement) {
        // Get the data-prototype explained earlier
        var prototype = collectionHolder.data('prototype-tablecolumn');
        // get the new index
        var index = addLinkElement.closest('.field-type-form-elements').data('index');
        // Replace '__name__' in the prototype's HTML to
        // instead be a number based on how many items we have
        var newForm = prototype.replace(/__name___tableColumns/g, index+"_tableColumns");
        newForm = newForm.replace(/__name__\]\[tableColumns/g, index+"][tableColumns");
        var tableColumnIndex = addLinkElement.closest('.tablecolumns').data('index');
        addLinkElement.closest('.tablecolumns').data('index',tableColumnIndex+1);
        newForm = newForm.replace(/__name__/g, tableColumnIndex);
        // Display the form in the page in an li, before the "Add a tag" link li
        var newFormElement = $('<li>'+newForm+'</li>');
        newFormElement.data("index",index);
        addLinkElement.before(newFormElement);
        RecordTypes.addDeleteLink(newFormElement);
        RecordTypes.updateDisplayOrder();
    },
    updateDisplayOrder: function() {
        $(".displayOrder input[type='hidden']").each(function(index){ $(this).val(index); });
        $(".displayOrderChoice input[type='hidden']").each(function(index){ $(this).val(index); });
        $(".displayOrderTableColumn input[type='hidden']").each(function(index){ $(this).val(index); });
    },
    sortable: function() {
        RecordTypes.updateDisplayOrder();
        $(".form-sortable").sortable({
            stop: function() { RecordTypes.updateDisplayOrder(); },
            handle: '.handle'
        });    
        $(".form-sortable").on('mousedown', '.handle', function() {
            $(this).css('cursor','grabbing');
        });
        $(".form-sortable").on('mouseup', '.handle', function() {
            $(this).css('cursor','grab');
        });
    },
    removeMulipleSelection: function(containerElement){
        $('.multiple-selection', containerElement.closest('.field-type')).remove(); 
    }
};
$(document).ready(function() {
    RecordTypes.init();
});