//Keep track of special pressed keys
var PressedKeys = {
    alt: false,
    ctrl: false,
    meta: false,
    shift: false,
    observers: new Array(),
    registerObserver: function(func) {
        PressedKeys.observers.push(func);
    },
    keydownFunction: function(e) {
        console.log('Keydown');
        if(e.altKey) {
            PressedKeys.alt = true;
        }
        if(e.ctrlKey) {
            PressedKeys.ctrl = true;
        }
        if(e.metaKey) {
            PressedKeys.meta = true;
        }
        if(e.shiftKey) {
            PressedKeys.shift = true;
        }
        $.each(PressedKeys.observers, function(key, value) { value.pressedKeysCallback(); });
    },
    keyupFunction: function(e) {
        console.log('Keyup');
        if(!e.altKey) {
            PressedKeys.alt = false;
        }
        if(!e.ctrlKey) {
            PressedKeys.ctrl = false;
        }
        if(!e.metaKey) {
            PressedKeys.meta = false;
        }
        if(!e.shiftKey) {
            PressedKeys.shift = false;
        }
        $.each(PressedKeys.observers, function(key, value) { value.pressedKeysCallback(); });
    }
};
//Classes
function CanvasImage(jsImage, thumbnail){
    var self = this;
    this.jsImage =  jsImage;
    this.thumbnail = thumbnail;
    this.crossX = false;
    this.crossY = false;
    this.buttons = new Array();
    this.pointed = false;
    this.zoom = new CanvasImageZoom();
    this.pan = new CanvasImagePan();
    this.group = new Kinetic.Group({
                                    x: Gallery.numberOfSelectedImages * CanvasImage.OVERLAP_OFFSET,
                                    y: Gallery.TOP_OFFSET + Gallery.numberOfSelectedImages * CanvasImage.OVERLAP_OFFSET,
                                    draggable: true,
                                    name: 'group'
                                  });
    this.layer = new Kinetic.Layer({ name: Gallery.imageIdCounter });
    this.layer.add(this.group);
    Gallery.stage.add(this.layer);
    var imageElement = $('img', this.thumbnail);
    var imageWidth = imageElement.width();
    var imageHeight = imageElement.height();
    this.image = new Kinetic.Image({
        x: imageWidth/2,
        y: imageHeight/2,
        image: self.jsImage,
        width: imageWidth,
        height: imageHeight,
        offset: { x: imageWidth/2, y: imageHeight/2 },
        name: 'image'
    });
    this.group.add(this.image); 
    this.addExtraElements();
    this.group.on('mousedown', function(e) {
        console.log('Mouse Down');
        self.layer.moveToTop();
    });
    this.group.on('dragstart', function() {
        console.log('Drag Started');
        self.layer.moveToTop();
    });
    this.group.on('dragend',function(){
        console.log('Drag Ended');
        Gallery.resetDropZones();
        var pointerPosition = Gallery.stage.getPointerPosition();
        var dropZone = Gallery.dropZoneLayer.getIntersection(pointerPosition);
        if(dropZone && dropZone.fillAlpha() !== Gallery.DROP_ZONE_SELECTED_ALPHA) {
            var topLeftMostAnchor = self.getTheCurrentlyTopLeftMostAnchor();
            var bottomRightMostAnchor = self.getTheCurrentlyBottomRightMostAnchor();
            topLeftMostAnchor.x(0); topLeftMostAnchor.y(0);
            var ratio = self.image.width()/self.image.height();
            var extraElementsOffset = CanvasImage.BUTTON_HEIGHT - CanvasImage.ANCHOR_OFFSET;
            if(dropZone === Gallery.leftDropZone) {
                console.log('Drop Detected in the Left Region');
                if(self.image.rotation() === 0 || self.image.rotation() === 180) {
                    var height = Math.min(Gallery.stage.width()/2/ratio, Gallery.stage.height()-extraElementsOffset);
                    var width = height*ratio;
                    bottomRightMostAnchor.x(width);
                    bottomRightMostAnchor.y(height);
                } else {
                    var width = Math.min(Gallery.stage.width()/2*ratio,Gallery.stage.height()-extraElementsOffset);
                    var height = width/ratio;
                    bottomRightMostAnchor.x(height);
                    bottomRightMostAnchor.y(width);
                }
                self.update(bottomRightMostAnchor);
                self.update(topLeftMostAnchor);
                var virtualHeight = bottomRightMostAnchor.y() - topLeftMostAnchor.y();
                var verticalPosition = (Gallery.stage.height() - virtualHeight)/2;  
                this.x(0); this.y(verticalPosition);
                self.layer.draw();
            } else if(dropZone === Gallery.rightDropZone) {
                console.log('Drop Detected in the Right Region');
                if(self.image.rotation() === 0 || self.image.rotation() === 180) {
                    var height = Math.min(Gallery.stage.width()/2/ratio, Gallery.stage.height()-extraElementsOffset);
                    var width = height*ratio;
                    bottomRightMostAnchor.x(width);
                    bottomRightMostAnchor.y(height);
                    this.x(Gallery.stage.width()-width);
                } else {
                    var width = Math.min(Gallery.stage.width()/2*ratio,Gallery.stage.height() - extraElementsOffset);
                    var height = width/ratio;
                    bottomRightMostAnchor.x(height);
                    bottomRightMostAnchor.y(width);
                    this.x(Gallery.stage.width()-height);
                }
                self.update(bottomRightMostAnchor);
                self.update(topLeftMostAnchor);
                var virtualHeight = bottomRightMostAnchor.y() - topLeftMostAnchor.y();
                var verticalPosition = (Gallery.stage.height() - virtualHeight)/2; 
                this.y(verticalPosition);
                self.layer.draw();
            } else if(dropZone === Gallery.topDropZone) {
                console.log('Drop Detected in the Top Region');
                if(self.image.rotation() === 0 || self.image.rotation() === 180) {
                    var height = Math.min(Gallery.stage.width()/ratio,Gallery.stage.height()/2-extraElementsOffset);
                    var width = height*ratio;
                    bottomRightMostAnchor.x(width);
                    bottomRightMostAnchor.y(height);
                } else {
                    var width = Math.min(Gallery.stage.width()*ratio,Gallery.stage.height()/2-extraElementsOffset);
                    var height = width/ratio;
                    bottomRightMostAnchor.x(height);
                    bottomRightMostAnchor.y(width);
                }
                self.update(bottomRightMostAnchor);
                self.update(topLeftMostAnchor);
                var virtualWidth = bottomRightMostAnchor.x() - topLeftMostAnchor.x();
                var horizontalPosition = (Gallery.stage.width() - virtualWidth)/2;  
                this.x(horizontalPosition); this.y(0);
                self.layer.draw();
            } else if(dropZone === Gallery.bottomDropZone) {
                console.log('Drop Detected in the Bottom Region');
                if(self.image.rotation() === 0 || self.image.rotation() === 180) {
                    var height = Math.min(Gallery.stage.width()/ratio, Gallery.stage.height()/2 - extraElementsOffset);
                    var width = height*ratio;
                    bottomRightMostAnchor.x(width);
                    bottomRightMostAnchor.y(height);
                } else {
                    var width = Math.min(Gallery.stage.width()*ratio, Gallery.stage.height()/2 - extraElementsOffset);
                    var height = width/ratio;
                    bottomRightMostAnchor.x(height);
                    bottomRightMostAnchor.y(width);
                }
                self.update(bottomRightMostAnchor);
                self.update(topLeftMostAnchor);
                var virtualWidth = bottomRightMostAnchor.x() - topLeftMostAnchor.x();
                var horizontalPosition = (Gallery.stage.width() - virtualWidth)/2;
                var virtualHeight = bottomRightMostAnchor.y() - topLeftMostAnchor.y();
                var verticalPosition = Gallery.stage.height() - virtualHeight - extraElementsOffset;
                this.x(horizontalPosition); 
                this.y(verticalPosition);
                self.layer.draw();
            } else if(dropZone === Gallery.topLeftDropZone) {
                console.log('Drop Detected in the Top Left Region');
                if(self.image.rotation() === 0 || self.image.rotation() === 180) {
                    var height = Math.min(Gallery.stage.width()/2/ratio, Gallery.stage.height()/2-extraElementsOffset);
                    var width = height*ratio;
                    bottomRightMostAnchor.x(width);
                    bottomRightMostAnchor.y(height);
                } else {
                    var width = Math.min(Gallery.stage.width()/2*ratio, Gallery.stage.height()/2-extraElementsOffset);                    
                    var height = width/ratio;
                    bottomRightMostAnchor.x(height);
                    bottomRightMostAnchor.y(width);
                }
                self.update(bottomRightMostAnchor);
                self.update(topLeftMostAnchor);
                this.x(0); this.y(0);
                self.layer.draw();
            } else if(dropZone === Gallery.topRightDropZone) {
                console.log('Drop Detected in the Top Right Region');
                if(self.image.rotation() === 0 || self.image.rotation() === 180) {
                    var height = Math.min(Gallery.stage.width()/2/ratio,Gallery.stage.height()/2-extraElementsOffset);
                    var width = height*ratio;
                    bottomRightMostAnchor.x(width);
                    bottomRightMostAnchor.y(height);
                    this.x(Gallery.stage.width()-width);
                } else {
                    var width = Math.min(Gallery.stage.width()/2*ratio,Gallery.stage.height()/2-extraElementsOffset);
                    var height = width/ratio;
                    bottomRightMostAnchor.x(height);
                    bottomRightMostAnchor.y(width);
                    this.x(Gallery.stage.width()-height);
                }
                self.update(bottomRightMostAnchor);
                self.update(topLeftMostAnchor);
                this.y(0);
                self.layer.draw();
            } else if(dropZone === Gallery.bottomLeftDropZone) {
                console.log('Drop Detected in the Bottom Left Region');
                if(self.image.rotation() === 0 || self.image.rotation() === 180) {
                    var height = Math.min(Gallery.stage.width()/2/ratio,Gallery.stage.height()/2-extraElementsOffset);
                    var width = height*ratio;
                    bottomRightMostAnchor.x(width);
                    bottomRightMostAnchor.y(height);
                } else {
                    var width = Math.min(Gallery.stage.width()/2*ratio,Gallery.stage.height()/2-extraElementsOffset);
                    var height = width/ratio;
                    bottomRightMostAnchor.x(height);
                    bottomRightMostAnchor.y(width);
                }
                self.update(bottomRightMostAnchor);
                self.update(topLeftMostAnchor);
                var virtualHeight = bottomRightMostAnchor.y() - topLeftMostAnchor.y();
                var verticalPosition = Gallery.stage.height() - virtualHeight - extraElementsOffset;  
                this.x(0); this.y(verticalPosition);
                self.layer.draw();
            } else if(dropZone === Gallery.bottomRightDropZone) {
                console.log('Drop Detected in the Bottom Right Region');
                if(self.image.rotation() === 0 || self.image.rotation() === 180) {
                    var height = Math.min(Gallery.stage.width()/2/ratio,Gallery.stage.height()/2-extraElementsOffset);
                    var width = height*ratio;
                    bottomRightMostAnchor.x(width);
                    bottomRightMostAnchor.y(height);
                    this.x(Gallery.stage.width()-width);
                } else {
                    var width = Math.min(Gallery.stage.width()/2*ratio,Gallery.stage.height()/2-extraElementsOffset);
                    var height = width/ratio;
                    bottomRightMostAnchor.x(height);
                    bottomRightMostAnchor.y(width);
                    this.x(Gallery.stage.width()-height);
                }
                self.update(bottomRightMostAnchor);
                self.update(topLeftMostAnchor);
                var virtualHeight = bottomRightMostAnchor.y() - topLeftMostAnchor.y();
                var verticalPosition = Gallery.stage.height() - virtualHeight - extraElementsOffset;  
                this.y(verticalPosition);                
                self.layer.draw();
            } else {
                console.log('Drop Detected in an Unknown Region Pos X: '+pointerPosition.x+' Pos Y: '+pointerPosition.y);
            }
            dropZone.fillAlpha(Gallery.DROP_ZONE_SELECTED_ALPHA);
        }
        Gallery.dropZoneLayer.draw();
    });
    PressedKeys.registerObserver(self);
    this.group.on('mousemove', function(e) {
        if(PressedKeys.ctrl) {
            console.log('Panning Over Image');
            if(self.pan.x && self.pan.y) {
                var deltaX = e.evt.clientX - self.pan.x;
                var deltaY = e.evt.clientY - self.pan.y;
                if(self.image.rotation() === 90
                || self.image.rotation() === 270) {
                    var deltaTemp = deltaX;
                    deltaX = deltaY;
                    deltaY = deltaTemp;
                }
                var crop = self.image.crop();
                console.log('Move Delta X: '+deltaX+' Y: '+deltaY+' Crop Width: '+crop.width +' Height: '+crop.height+' X: '+crop.x+' Y: '+crop.y);
                var widthRatio = self.image.getImage().width / self.image.width();
                var heightRatio = self.image.getImage().height / self.image.height();
                if(self.image.rotation() === 90
                || self.image.rotation() === 270) {
                    var ratioTemp = widthRatio;
                    widthRatio = heightRatio;
                    heightRatio = ratioTemp;
                }
                crop.x-=deltaX*widthRatio;
                crop.y-=deltaY*heightRatio;
                if(crop.x + crop.width > self.image.getImage().width) {
                    console.log('Pan Width Out of Bounds (too big)');
                    crop.x = Math.abs(self.image.getImage().width - crop.width);
                }
                if(crop.y + crop.height > self.image.getImage().height) {
                    console.log('Pan Height Out of Bounds (too big)');
                    crop.y = Math.abs(self.image.getImage().height - crop.height);
                }
                if(crop.x < 0) {
                    console.log('Pan Width Out of Bounds (too small)');
                    crop.x = 0;
                }
                if(crop.y < 0) {
                    console.log('Pan Height Out of Bounds (too small)');
                    crop.y = 0;
                }
                self.zoom.x = crop.x;
                self.zoom.y = crop.y;
                self.image.crop(crop);
                self.layer.draw();
            } else { console.log('First Move'); }
            self.pan.x = e.evt.clientX;
            self.pan.y = e.evt.clientY;
        }
    });
    this.image.on('mouseenter', function() {
        self.pointed = true;
        Gallery.pointedImage = self;
        self.pressedKeysCallback();
    });
    this.image.on('mouseleave', function() {
        self.pointed = false;
        Gallery.pointedImage = null;
        document.body.style.cursor = 'default';
        self.pan.reset();
    });
    this.image.on('mousemove', function() {
        self.pressedKeysCallback();
    });
    //Store image object and draw
    Gallery.images[Gallery.imageIdCounter] = this;
    Gallery.imageIdCounter++;
    this.layer.draw();
};
CanvasImage.ANCHOR_OFFSET = 6;
CanvasImage.BUTTON_HEIGHT = 18;
CanvasImage.OVERLAP_OFFSET = 12;
CanvasImage.prototype.pressedKeysCallback = function() {
    if(PressedKeys.ctrl) {
        document.body.style.cursor = 'move';
        this.group.draggable(false);
    } else {
        document.body.style.cursor = 'default';
        this.pan.reset();
        this.group.draggable(true);
    }
};
CanvasImage.prototype.addExtraElements = function() {
    //Add Anchors
    this.addAnchor('topLeft');
    this.addAnchor('topRight');
    this.addAnchor('bottomRight');
    this.addAnchor('bottomLeft');
    //Add Buttons
    var removeButtonOffset = this.addButton(CanvasButton.PADDING * 1, 'remove', '\uf00d', 'red');
    var rotateLeftButtonOffset = this.addButton(CanvasButton.PADDING * 2 + removeButtonOffset, 'rotate_left', '\uf0e2', 'blue');
    this.addButton(CanvasButton.PADDING * 3 + removeButtonOffset + rotateLeftButtonOffset, 'rotate_right', '\uf01e', 'blue');
};
CanvasImage.prototype.getTheCurrentlyBottomRightMostAnchor = function() {
    var topLeftAnchor = this.group.find('.topLeft')[0];
    var topRightAnchor = this.group.find('.topRight')[0];
    var bottomRightAnchor = this.group.find('.bottomRight')[0];
    var bottomLeftAnchor = this.group.find('.bottomLeft')[0];
    
    var currentlyBottomRightMostAnchor = bottomRightAnchor;
    if(topLeftAnchor.x() >= currentlyBottomRightMostAnchor.x() && topLeftAnchor.y() >= currentlyBottomRightMostAnchor.y()) {
        currentlyBottomRightMostAnchor = topLeftAnchor;
    } else if(topRightAnchor.x() >= currentlyBottomRightMostAnchor.x() && topRightAnchor.y() >= currentlyBottomRightMostAnchor.y()) {
        currentlyBottomRightMostAnchor = topRightAnchor;
    } else if(bottomLeftAnchor.x() >= currentlyBottomRightMostAnchor.x() && bottomLeftAnchor.y() >= currentlyBottomRightMostAnchor.y()) {
        currentlyBottomRightMostAnchor = bottomLeftAnchor;
    }
    return currentlyBottomRightMostAnchor;
};
CanvasImage.prototype.getTheCurrentlyTopLeftMostAnchor = function() {
    var topLeftAnchor = this.group.find('.topLeft')[0];
    var topRightAnchor = this.group.find('.topRight')[0];
    var bottomRightAnchor = this.group.find('.bottomRight')[0];
    var bottomLeftAnchor = this.group.find('.bottomLeft')[0];
    
    var currentlyTopLeftMostAnchor = topLeftAnchor;
    if(topRightAnchor.x() <= currentlyTopLeftMostAnchor.x() && topRightAnchor.y() <= currentlyTopLeftMostAnchor.y()) {
        currentlyTopLeftMostAnchor = topRightAnchor;
    } else if(bottomRightAnchor.x() <= currentlyTopLeftMostAnchor.x() && bottomRightAnchor.y() <= currentlyTopLeftMostAnchor.y()) {
        currentlyTopLeftMostAnchor = bottomRightAnchor;
    } else if(bottomLeftAnchor.x() <= currentlyTopLeftMostAnchor.x() && bottomLeftAnchor.y() <= currentlyTopLeftMostAnchor.y()) {
        currentlyTopLeftMostAnchor = bottomLeftAnchor;
    }
    return currentlyTopLeftMostAnchor;
};
CanvasImage.prototype.addAnchor = function(name) {
    var self = this;
    var group = this.group;
    switch(name) {
        case 'topLeft':
          var x = 0 - CanvasImage.ANCHOR_OFFSET;
          var y = 0 - CanvasImage.ANCHOR_OFFSET;
          break;
        case 'topRight':
          var x = this.image.width() + CanvasImage.ANCHOR_OFFSET;
          var y = 0 - CanvasImage.ANCHOR_OFFSET;
          break;
        case 'bottomRight':
          var x = this.image.width() + CanvasImage.ANCHOR_OFFSET;
          var y = this.image.height() + CanvasImage.ANCHOR_OFFSET;
          break;
        case 'bottomLeft':
          var x = 0 - CanvasImage.ANCHOR_OFFSET;
          var y = this.image.height() + CanvasImage.ANCHOR_OFFSET;
          break;
        default:
          var x = 0;
          var y = 0;
          break;
    }
    var anchor = new Kinetic.Circle({
      x: x,
      y: y,
      stroke: '#666',
      fill: '#ddd',
      strokeWidth: 2,
      radius: CanvasImage.ANCHOR_OFFSET,
      name: name,
      draggable: true,
      dragOnTop: false
    });
    anchor.on('dragstart', function() {
      group.setDraggable(false);
      this.moveToTop();
    });
    anchor.on('dragmove', function() {
      var layer = this.getLayer();
      if(layer !== null) {
        self.update(this);
        layer.draw();
      }
    });
    anchor.on('dragend', function() {
      var layer = this.getLayer();
      if(layer !== null) {
        group.setDraggable(true);
        layer.draw();
      }
    });
    // add hover styling
    anchor.on('mouseover', function() {
      var layer = this.getLayer();
      if(layer !== null) {
        switch(name) {
            case 'topLeft':
              if(!self.crossX && !self.crossY) {
                document.body.style.cursor = 'nw-resize';
              } else if(self.crossX && !self.crossY) {
                document.body.style.cursor = 'ne-resize';
              } else if(!self.crossX && self.crossY) {
                document.body.style.cursor = 'sw-resize';
              } else if(self.crossX && self.crossY) {
                document.body.style.cursor = 'se-resize';
              } break;
            case 'topRight':
              if(!self.crossX && !self.crossY) {
                document.body.style.cursor = 'ne-resize';
              } else if(self.crossX && !self.crossY) {
                document.body.style.cursor = 'nw-resize';
              } else if(!self.crossX && self.crossY) {
                document.body.style.cursor = 'se-resize';
              } else if(self.crossX && self.crossY) {
                document.body.style.cursor = 'sw-resize';
              } break;
            case 'bottomRight':
              if(!self.crossX && !self.crossY) {
                document.body.style.cursor = 'se-resize';
              } else if(self.crossX && !self.crossY) {
                document.body.style.cursor = 'sw-resize';
              } else if(!self.crossX && self.crossY) {
                document.body.style.cursor = 'ne-resize';
              } else if(self.crossX && self.crossY) {
                document.body.style.cursor = 'nw-resize';
              } break;
            case 'bottomLeft':
              if(!self.crossX && !self.crossY) {
                document.body.style.cursor = 'sw-resize';
              } else if(self.crossX && !self.crossY) {
                document.body.style.cursor = 'se-resize';
              } else if(!self.crossX && self.crossY) {
                document.body.style.cursor = 'nw-resize';
              } else if(self.crossX && self.crossY) {
                document.body.style.cursor = 'ne-resize';
              } break;
        }
        this.setStrokeWidth(4);
        layer.draw();
      }
    });
    anchor.on('mouseout', function() {
      var layer = this.getLayer();
      if(layer !== null) {
        document.body.style.cursor = 'default';
        this.strokeWidth(2);
        layer.draw();
      }
    });
    group.add(anchor);
    return anchor;
};
CanvasImage.prototype.addButton = function(x, name, label, color) {
    var self = this;
    var group = this.layer.find('.group');
    var button = new Kinetic.Group({
        name: name,
        x: x,
        y: this.image.height()
    });
    var label = new Kinetic.Text({
        name: 'label',
        x: CanvasButton.PADDING,
        y: CanvasButton.PADDING,
        text: label,
        fontSize: 12,
        fontFamily: 'FontAwesome',
        fill: color
    });
    var rectangle = new Kinetic.Rect({
        name: 'rectangle',
        x: 0,
        y: 0,
        width: label.width()+2*CanvasButton.PADDING,
        height: CanvasImage.BUTTON_HEIGHT,
        fill: 'white',
        stroke: 'black',
        strokeWidth: 2
    });
    button.on('mouseover', function() {
        var layer = this.getLayer();
        if(layer !== null) {
            document.body.style.cursor = 'pointer';
            this.get('.rectangle').strokeWidth(4);
            layer.draw();
        }
    });
    button.on('mouseout', function() {
        var layer = this.getLayer();
        if(layer !== null) {
            document.body.style.cursor = 'default';
            this.get('.rectangle').strokeWidth(2);
            layer.draw();
        }
    });
    button.on('mousedown', function() {
        var layer = this.getLayer();
        if(layer !== null) {
            document.body.style.cursor = 'default';
            this.get('.label').fontStyle('bold');
            layer.draw();
        }
    });
    button.on('mouseup', function() {
        var layer = this.getLayer();
        if(layer !== null) {
            document.body.style.cursor = 'default';
            this.get('.label').fontStyle('normal');
            layer.draw();
        }
    });
    button.on('click', function(evt) {
        var layer = this.getLayer();
        switch(this.name()) {
            case 'remove':
                console.log('Remove Button');
                evt.cancelBubble = true;
                var canvasImage = Gallery.images[layer.name()];
                canvasImage.layer.destroy();
                canvasImage.thumbnail.trigger('click');
                break;
            case 'rotate_left':
                self.rotate(1);
                console.log('Rotate Left Button: '+self.image.rotation());
                break;
            case 'rotate_right':
                self.rotate(-1);
                console.log('Rotate Right Button: '+self.image.rotation());
                break;
            default: console.log('Unknown Button'); break;
        }
    });
    button.add(rectangle);
    button.add(label);
    group.add(button);
    this.buttons.push(new CanvasButton(button, x));
    return rectangle.width();
};
CanvasImage.prototype.rotate = function(direction) {
    this.image.offset({x: this.image.width()/2, y: this.image.height()/2});
    switch(this.image.rotation()) {
        case 0:
            if(direction > 0) {
                this.image.rotation(90);
            } else {
                this.image.rotation(270);
            }
            break;
        case 90:
        case 180:
            if(direction > 0) {
                this.image.rotation(this.image.rotation()+90);
            } else {
                this.image.rotation(this.image.rotation()-90);
            }
            break;
        case 270:
            if(direction > 0) {
                this.image.rotation(0);
            } else {
                this.image.rotation(180);
            }
            break;
    };
    var topLeft = this.group.find('.topLeft')[0];
    var topRight = this.group.find('.topRight')[0];
    var bottomLeft = this.group.find('.bottomLeft')[0];
    var bottomRight = this.group.find('.bottomRight')[0];
        
    var rotationFactor = 1;
    var horizontalSize = 0;
    var verticalSize = 0;    
    switch(this.image.rotation()) {
        case 90:
        case 270:
            rotationFactor = 1;
            if(this.image.width() / this.image.height() < 1) {
                rotationFactor = -1;
            }
            horizontalSize = this.image.height();            
            verticalSize = this.image.width();            
            break;
        case 0:
        case 180:
        default:
            var rotationFactor = -1;
            if(this.image.width() / this.image.height() < 1) {
                rotationFactor = 1;
            }
            horizontalSize = this.image.width();
            verticalSize = this.image.height();
            break;
    };
    var crossXFactor = 1;
    this.crossX ? crossXFactor = -1 : crossXFactor = 1;
    
    var diffHorizontal = Math.abs(topLeft.x() - topRight.x());
    var offsetHorizontal = Math.abs(diffHorizontal - horizontalSize)/2 - rotationFactor*CanvasImage.ANCHOR_OFFSET; 
    
    topLeft.x(topLeft.x()+crossXFactor*rotationFactor*offsetHorizontal);
    topRight.x(topRight.x()-crossXFactor*rotationFactor*offsetHorizontal);
    bottomLeft.x(bottomLeft.x()+crossXFactor*rotationFactor*offsetHorizontal);
    bottomRight.x(bottomRight.x()-crossXFactor*rotationFactor*offsetHorizontal);
    
    var crossYFactor = 1;
    this.crossY ? crossYFactor = -1 : crossYFactor = 1;
    
    var diffVertical = Math.abs(topLeft.y() - bottomLeft.y());
    var offsetVertical = Math.abs(diffVertical - verticalSize)/2 + rotationFactor*CanvasImage.ANCHOR_OFFSET; 
    
    topLeft.y(topLeft.y()-crossYFactor*rotationFactor*offsetVertical);
    bottomLeft.y(bottomLeft.y()+crossYFactor*rotationFactor*offsetVertical);
    topRight.y(topRight.y()-crossYFactor*rotationFactor*offsetVertical);
    bottomRight.y(bottomRight.y()+crossYFactor*rotationFactor*offsetVertical);
    
    this.updateButtons(horizontalSize, verticalSize);
    this.image.getLayer().draw();
};
CanvasImage.prototype.update = function(activeAnchor) {    
    var self = this;
    var group = activeAnchor.getParent();

    var topLeft = group.find('.topLeft')[0];
    var topRight = group.find('.topRight')[0];
    var bottomRight = group.find('.bottomRight')[0];
    var bottomLeft = group.find('.bottomLeft')[0];
    var image = this.image;

    var anchorX = activeAnchor.x();
    var anchorY = activeAnchor.y();
    // update anchor positions depending on the active anchor
    switch (activeAnchor.name()) {
      case 'topLeft':
        topRight.y(anchorY);
        bottomLeft.x(anchorX);
        break;
      case 'topRight':
        topLeft.y(anchorY);
        bottomRight.x(anchorX);
        break;
      case 'bottomRight':
        bottomLeft.y(anchorY);
        topRight.x(anchorX); 
        break;
      case 'bottomLeft':
        bottomRight.y(anchorY);
        topLeft.x(anchorX); 
        break;
    }
    var imageAspectRatio = 1;
    if(image.rotation() === 90 || image.rotation() === 270) { 
        imageAspectRatio = image.height()/image.width();
    } else {
        imageAspectRatio = image.width()/image.height();
    }
    var width  = Math.abs(topRight.x()   - topLeft.x() - 2 * CanvasImage.ANCHOR_OFFSET);
    var height = Math.abs(bottomLeft.y() - topLeft.y() - 2 * CanvasImage.ANCHOR_OFFSET);
    
    var x = topLeft.x()+CanvasImage.ANCHOR_OFFSET;
    var y = topLeft.y()+CanvasImage.ANCHOR_OFFSET;

    if(topRight.x() < topLeft.x()
    || bottomRight.x() < bottomLeft.x()) {
        console.log('--- Cross X ---');
        self.crossX = true;
        image.scaleX(-1);
        width = Math.abs(width - 4*CanvasImage.ANCHOR_OFFSET);
        x -= 2*CanvasImage.ANCHOR_OFFSET;
    } else {
        console.log('--- Not Cross X ---');
        self.crossX = false;
        image.scaleX(1);
    }
    if(bottomLeft.y() < topLeft.y()
    || bottomRight.y() < topRight.y()) {
        console.log('--- Cross Y ---');
        self.crossY = true;
        image.scaleY(-1);
        height = Math.abs(height - 4*CanvasImage.ANCHOR_OFFSET);
        y -= 2*CanvasImage.ANCHOR_OFFSET;
    } else {
        console.log('--- Not Cross Y ---');
        self.crossY = false;
        image.scaleY(1);
    }
    /* Force active anchor to follow the current aspect ratio when shift is NOT pressed
     * NOTE: This aspect ratio lock still has a few behavioral issues.
     *       Nothing major but can be improved (but it is not trivial)
     */
    if(!PressedKeys.shift) {
        height = Math.abs(width / imageAspectRatio);
        if(Utilities.stringContains(activeAnchor.name(),'bottom')) {
            bottomLeft.y(y+height+CanvasImage.ANCHOR_OFFSET);
            bottomRight.y(y+height+CanvasImage.ANCHOR_OFFSET);
            if(self.crossY) {    
            bottomLeft.y(y-height-CanvasImage.ANCHOR_OFFSET);
            bottomRight.y(y-height-CanvasImage.ANCHOR_OFFSET);
            }
        } else {
            var bottomOffset = (bottomLeft.y() - (topLeft.y() + height) - CanvasImage.ANCHOR_OFFSET*2);
            topLeft.y(topLeft.y()+bottomOffset);
            topRight.y(topRight.y()+bottomOffset);
            y+=bottomOffset;
            if(self.crossY) {
                topLeft.y(y-height-CanvasImage.ANCHOR_OFFSET);
                topRight.y(y-height-CanvasImage.ANCHOR_OFFSET);
            }
        }
    }
    if(image.rotation() === 90 || image.rotation() === 270) {
        image.setSize({width: height, height: width});
    } else {
        image.setSize({width: width, height: height});
    }
    image.offset({ x: image.width()/2, y: image.height()/2 });
    if(image.rotation() === 90 || image.rotation() === 270) {
        if(!self.crossX) {
            image.x(x + this.image.offsetY());
        } else {
            image.x(x - this.image.offsetY());
        }
        if(!self.crossY) {
            image.y(y + this.image.offsetX());
        } else {
            image.y(y - this.image.offsetX());
        } 
    } else {
        if(!self.crossX) {
            image.x(x + this.image.offsetX());
        } else {
            image.x(x - this.image.offsetX());
        }
        if(!self.crossY) {
            image.y(y + this.image.offsetY());
        } else {
            image.y(y - this.image.offsetY());
        } 
    }
    this.updateButtons(width,height);
    console.log('Active Anchor Name: '+activeAnchor.name()+' X: '+activeAnchor.x()+' Y: '+activeAnchor.y());
    console.log('Image X: '+image.x()+' Y: '+image.y()+' Width: '+image.width()+' Height: '+image.height()+' Aspect Ratio: '+imageAspectRatio);
};
CanvasImage.prototype.updateButtons = function(width, height) {
    var self = this;
    $.each(this.buttons, function(key, value) {
        if(self.image.rotation() === 90 || self.image.rotation() === 270) {
            value.group.x(self.image.x()+value.offsetX-width/2);
            value.group.y(self.image.y()+self.image.width()-height/2);
        } else {
            value.group.x(self.image.x()+value.offsetX-width/2);
            value.group.y(self.image.y()+self.image.height()-height/2);
        }
        console.log('Button: '+value.group.name()+' X: '+value.group.x()+' Y: '+value.group.y());
    });
};
function CanvasImageZoom(){
    this.reset();
};
CanvasImageZoom.prototype.reset = function() {
    this.x = null;
    this.y = null;
    this.factor = 1.0;
};
function CanvasImagePan() {
    this.reset();
};
CanvasImagePan.prototype.reset = function() {
    this.x = null;
    this.y = null;
};
function CanvasButton(group, offsetX){
    this.group = group;
    this.offsetX = offsetX;
};
CanvasButton.PADDING = 4;
var Gallery = {
        stage: null,
        canvasMode: false,
        images: {},
        pointedImage: null,
        imageIdCounter: 0,
        numberOfSelectedImages: 0,
        TOP_OFFSET: 40,
        DROP_ZONE_THICKNESS: 40,
        DROP_ZONE_SELECTED_ALPHA: 0.5,
        DROP_ZONE_UNSELECTED_ALPHA: 0,
	init:  function() {
            $('.mode-toggle-link').click(function() {
              Gallery.canvasMode = !Gallery.canvasMode;
              if(Gallery.canvasMode) {
                console.log('Canvas Mode');
                $('#canvas-container').show();
                $('#records-table').hide();
              } else {
                console.log('Gallery Mode');
                $('#canvas-container').hide();
                $('#records-table').show();
              }
            });
            $('.save-gallery-screenshot').click(function() {
                console.log('Saving Screenshot');
                html2canvas($('#canvas'), {
                    onrendered: function(canvas) {
                        // Construct the <a> element
                        var link = document.createElement("a");
                        link.download = "Gallery Overview Screenshot";
                        // Construct the uri                        
                        link.href = canvas.toDataURL();
                        link.target = '_blank';
                        document.body.appendChild(link);
                        link.click();
                        // Cleanup the DOM
                        document.body.removeChild(link);
                        delete link;
                    }
                });
            });
            $('.gallery-table').on('click', '.gallery-thumbnail-link', function() {
                if($(this).data('selected') !== true) {
                    Gallery.numberOfSelectedImages++;
                    $(this).data('selected',true);
                    $(this).data('imageId',Gallery.imageIdCounter);
                    $('.gallery-thumbnail',$(this)).addClass('gallery-thumbnail-selected');
                    var imageSrc = $('img',$(this)).data('image-url');                  
                    var thumbnail = $(this);
                    var image = new Image();
                    image.onload = function() {
                        console.log('Image Loaded');
                        new CanvasImage(image, thumbnail);
                    };
                    image.src = imageSrc;
                } else {
                    Gallery.numberOfSelectedImages--;
                    $(this).data('selected',false);
                    $('.gallery-thumbnail',$(this)).removeClass('gallery-thumbnail-selected');
                    var imageId = $(this).data('imageId');
                    Gallery.images[imageId].layer.destroy();
                    delete Gallery.images[imageId];
                    Gallery.stage.draw();
                }
                $('#number-of-selected-images').text(Gallery.numberOfSelectedImages);
            });
            Gallery.setupStage();
            Gallery.resizeCanvas();
            $(window).resize(function () {
		Gallery.resizeCanvas();
            });
            Gallery.initPagination();
	},
	setupStage: function() {
            Gallery.stage = new Kinetic.Stage({
                container: 'canvas'
            });
            Gallery.setupKeys();
            Gallery.setupZoom();
            Gallery.setupDropZones();
	},
        setupKeys: function() {
            //Capture events
            $('body').keydown(PressedKeys.keydownFunction);
            $('body').mouseenter(PressedKeys.keydownFunction);
            $('body').keyup(PressedKeys.keyupFunction);
            $('body').mouseleave(PressedKeys.keyupFunction);
        },
        setupZoom: function() {
            $('#canvas-container').bind('mousewheel DOMMouseScroll', function(e) {
                if(Gallery.pointedImage) {
                    e.preventDefault();
                    var event;
                    if(e.type === 'DOMMouseScroll') {
                        event = e.originalEvent;
                    } else {
                        event = e;
                    }
                    var canvasImage = Gallery.pointedImage;                
                    var image = canvasImage.image;
                    var posX = 0;
                    var posY = 0;
                    if(event.pageX || event.pageY) {
                        posX = event.pageX;
                        posY = event.pageY;
                    } else if (event.clientX || event.clientY) 	{
                        posX = event.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
                        posY = event.clientY + document.body.scrollTop + document.documentElement.scrollTop;
                    }
                    var canvasOffset = $(Gallery.stage.container()).offset();
                    var relativeX = posX-image.getAbsolutePosition().x-canvasOffset.left+image.width()/2;
                    var relativeY = posY-image.getAbsolutePosition().y-canvasOffset.top+image.height()/2;
                    var widthRatio = image.getImage().width / image.width();
                    var heightRatio = image.getImage().height / image.height();
                    var originalX = Math.floor(relativeX * widthRatio);
                    var originalY = Math.floor(relativeY * heightRatio);
                    var zoomMode = 1;
                    var eventDelta;
                    if(e.type === 'DOMMouseScroll') {
                        eventDelta = -event.detail;
                    } else {
                        eventDelta = event.originalEvent.wheelDelta;
                    }
                    if(eventDelta < 0) {
                        zoomMode = -1;
                    }
                    var zoomFactor = 0.025 * canvasImage.zoom.factor;
                    canvasImage.zoom.factor += zoomMode * zoomFactor;
                    if(canvasImage.zoom.factor < 1.0) {
                        canvasImage.zoom.reset();
                    }
                    var zoomedX = originalX;
                    var zoomedY = originalY;
                    var zoomedWidth = Math.abs(image.getImage().width / canvasImage.zoom.factor);
                    var zoomedHeight = Math.abs(image.getImage().height / canvasImage.zoom.factor);
                    if(zoomedX + zoomedWidth > image.getImage().width) {
                        console.log('Width Out of Bounds');
                        zoomedX = Math.abs(image.getImage().width - zoomedWidth);
                    }
                    if(zoomedY + zoomedHeight > image.getImage().height) {
                        console.log('Height Out of Bounds');
                        zoomedY = Math.abs(image.getImage().height - zoomedHeight);
                    }
                    if(!canvasImage.zoom.x && !canvasImage.zoom.y) {
                        console.log('First Zoom');
                        canvasImage.zoom.x = zoomedX;
                        canvasImage.zoom.y = zoomedY;
                    } else {
                        console.log('Another Zoom');
                        canvasImage.zoom.x = 0.5*canvasImage.zoom.x + 0.5*zoomedX;
                        canvasImage.zoom.y = 0.5*canvasImage.zoom.y + 0.5*zoomedY;
                    }
                    console.log('Mouse Over Image X: '+originalX+' Y: '+originalY+' Relative X: '+relativeX+' Y: '+relativeY+' Zoom Mode: '+zoomMode+' Zoom: '+canvasImage.zoom.factor+' Zoomed X: '+zoomedX+' Y: '+zoomedY+' Width: '+zoomedWidth+' Height: '+zoomedHeight);
                    image.crop({
                                    x: canvasImage.zoom.x,
                                    y: canvasImage.zoom.y,
                                    width: zoomedWidth,
                                    height: zoomedHeight
                                });
                    image.getLayer().draw();
                } else {
                    console.log('No image pointed');
                }
            });
        },
        setupDropZones: function() {
            Gallery.dropZoneLayer =  new Kinetic.Layer();
            Gallery.stage.add(Gallery.dropZoneLayer);
            Gallery.leftDropZone = new Kinetic.Rect({
                                        x: 0,
                                        y: Gallery.stage.height()/2,
                                        width: Gallery.DROP_ZONE_THICKNESS,
                                        height: Gallery.stage.height()/2,
                                        fillBlue: 127,
                                        fillGreen: 127,
                                        fillRed: 127,
                                        fillAlpha: Gallery.DROP_ZONE_UNSELECTED_ALPHA
                                      });                          
            Gallery.rightDropZone = new Kinetic.Rect({
                                        x: Gallery.stage.width()-Gallery.DROP_ZONE_THICKNESS,
                                        y: 0,
                                        width: Gallery.DROP_ZONE_THICKNESS,
                                        height: Gallery.stage.height()/2,
                                        fillBlue: 127,
                                        fillGreen: 127,
                                        fillRed: 127,
                                        fillAlpha: Gallery.DROP_ZONE_UNSELECTED_ALPHA
                                      });
            Gallery.topDropZone = new Kinetic.Rect({
                                        x: 0,
                                        y: Gallery.stage.height()/2,
                                        width: Gallery.stage.width()-(Gallery.DROP_ZONE_THICKNESS*2),
                                        height: Gallery.DROP_ZONE_THICKNESS,
                                        fillBlue: 127,
                                        fillGreen: 127,
                                        fillRed: 127,
                                        fillAlpha: Gallery.DROP_ZONE_UNSELECTED_ALPHA
                                      });  
            Gallery.bottomDropZone = new Kinetic.Rect({
                                        x: 0,
                                        y: Gallery.stage.height()-Gallery.DROP_ZONE_THICKNESS,
                                        width: Gallery.stage.width()-(Gallery.DROP_ZONE_THICKNESS*2),
                                        height: Gallery.DROP_ZONE_THICKNESS,
                                        fillBlue: 127,
                                        fillGreen: 127,
                                        fillRed: 127,
                                        fillAlpha: Gallery.DROP_ZONE_UNSELECTED_ALPHA
                                      });          
            Gallery.topLeftDropZone = new Kinetic.Rect({
                                        x: 0,
                                        y: 0,
                                        width: Gallery.DROP_ZONE_THICKNESS,
                                        height: Gallery.stage.height()/4,
                                        fillBlue: 127,
                                        fillGreen: 127,
                                        fillRed: 127,
                                        fillAlpha: Gallery.DROP_ZONE_UNSELECTED_ALPHA
                                      });                          
            Gallery.topRightDropZone = new Kinetic.Rect({
                                        x: Gallery.stage.width()-Gallery.DROP_ZONE_THICKNESS,
                                        y: 0,
                                        width: Gallery.DROP_ZONE_THICKNESS,
                                        height: Gallery.stage.height()/4,
                                        fillBlue: 127,
                                        fillGreen: 127,
                                        fillRed: 127,
                                        fillAlpha: Gallery.DROP_ZONE_UNSELECTED_ALPHA
                                      });
            Gallery.bottomLeftDropZone = new Kinetic.Rect({
                                        x: 0,
                                        y: (Gallery.stage.height()/4)*3,
                                        width: Gallery.DROP_ZONE_THICKNESS,
                                        height: Gallery.stage.height()/4,
                                        fillBlue: 127,
                                        fillGreen: 127,
                                        fillRed: 127,
                                        fillAlpha: Gallery.DROP_ZONE_UNSELECTED_ALPHA
                                      });                          
            Gallery.bottomRightDropZone = new Kinetic.Rect({
                                        x: Gallery.stage.width()-Gallery.DROP_ZONE_THICKNESS,
                                        y: (Gallery.stage.height()/4)*3,
                                        width: Gallery.DROP_ZONE_THICKNESS,
                                        height: Gallery.stage.height()/4,
                                        fillBlue: 127,
                                        fillGreen: 127,
                                        fillRed: 127,
                                        fillAlpha: Gallery.DROP_ZONE_UNSELECTED_ALPHA
                                      });                    
            Gallery.dropZoneLayer.add(Gallery.leftDropZone);
            Gallery.dropZoneLayer.add(Gallery.rightDropZone);
            Gallery.dropZoneLayer.add(Gallery.topDropZone);
            Gallery.dropZoneLayer.add(Gallery.bottomDropZone);
            Gallery.dropZoneLayer.add(Gallery.topLeftDropZone);
            Gallery.dropZoneLayer.add(Gallery.topRightDropZone);
            Gallery.dropZoneLayer.add(Gallery.bottomLeftDropZone);
            Gallery.dropZoneLayer.add(Gallery.bottomRightDropZone);
        },
        resetDropZones: function() {
            Gallery.leftDropZone.fillAlpha(Gallery.DROP_ZONE_UNSELECTED_ALPHA);
            Gallery.rightDropZone.fillAlpha(Gallery.DROP_ZONE_UNSELECTED_ALPHA);
            Gallery.topDropZone.fillAlpha(Gallery.DROP_ZONE_UNSELECTED_ALPHA);
            Gallery.bottomDropZone.fillAlpha(Gallery.DROP_ZONE_UNSELECTED_ALPHA);
            Gallery.topLeftDropZone.fillAlpha(Gallery.DROP_ZONE_UNSELECTED_ALPHA);
            Gallery.topRightDropZone.fillAlpha(Gallery.DROP_ZONE_UNSELECTED_ALPHA);
            Gallery.bottomLeftDropZone.fillAlpha(Gallery.DROP_ZONE_UNSELECTED_ALPHA);
            Gallery.bottomRightDropZone.fillAlpha(Gallery.DROP_ZONE_UNSELECTED_ALPHA);
        },
	resizeCanvas: function() {
            $('#canvas-container').width($(window).width());
            $('#canvas-container').height($(window).height());
            $('#canvas').width($(window).width());
            $('#canvas').height($(window).height()-$('#canvas').offset().top);
            Gallery.stage.width($(window).width());
            Gallery.stage.height($(window).height()-$('#canvas').offset().top);
            //Resize Dropzones X
            Gallery.leftDropZone.x(0);
            Gallery.rightDropZone.x(Gallery.stage.width()-Gallery.DROP_ZONE_THICKNESS);
            Gallery.topDropZone.x(Gallery.DROP_ZONE_THICKNESS);
            Gallery.bottomDropZone.x(Gallery.DROP_ZONE_THICKNESS);
            Gallery.topLeftDropZone.x(0);
            Gallery.topRightDropZone.x(Gallery.stage.width()-Gallery.DROP_ZONE_THICKNESS);
            Gallery.bottomLeftDropZone.x(0);
            Gallery.bottomRightDropZone.x(Gallery.stage.width()-Gallery.DROP_ZONE_THICKNESS);
            //Resize Dropzones Y
            Gallery.leftDropZone.y(Gallery.stage.height()/4);
            Gallery.rightDropZone.y(Gallery.stage.height()/4);
            Gallery.topDropZone.y(0);
            Gallery.bottomDropZone.y(Gallery.stage.height()-Gallery.DROP_ZONE_THICKNESS);
            Gallery.topLeftDropZone.y(0);
            Gallery.topRightDropZone.y(0);
            Gallery.bottomLeftDropZone.y((Gallery.stage.height()/4)*3);
            Gallery.bottomRightDropZone.y((Gallery.stage.height()/4)*3);
            //Resize Dropzones Height
            Gallery.leftDropZone.height(Gallery.stage.height()/2);
            Gallery.rightDropZone.height(Gallery.stage.height()/2);
            Gallery.topLeftDropZone.height(Gallery.stage.height()/4);
            Gallery.topRightDropZone.height(Gallery.stage.height()/4);
            Gallery.bottomLeftDropZone.height(Gallery.stage.height()/4);
            Gallery.bottomRightDropZone.height(Gallery.stage.height()/4);
            //Resize Dropzones Width
            Gallery.topDropZone.width(Gallery.stage.width()-(Gallery.DROP_ZONE_THICKNESS*2));
            Gallery.bottomDropZone.width(Gallery.stage.width()-(Gallery.DROP_ZONE_THICKNESS*2));
            //Redraw
            Gallery.dropZoneLayer.draw();
	}
};
//Pagination
Gallery.loadPage = function(button, baseURL, page, query){
    button.addClass('active');
    var reqURL = baseURL+page+query;
    console.log('Load URL:' +reqURL);
    $.get(reqURL, function(data) {
        if(data.length === 0) {
            alert(Gallery.noMoreResults);
            button.attr('disabled', 'disabled');
        } else {
            $('.gallery-results-table-body').html($('.gallery-results-table-body').html()+data);
        }
        button.removeClass('active');
    });
};
Gallery.initPagination = function() {
    //Currently only the Advanced Gallery is Dynamically Paginated
    if(window.location.pathname.match(/gallery\/advanced/g)) {
        var rootPath = window.location.pathname.match(/.+(?=\/gallery)/g);
        var recordType = window.location.pathname.match(/\d+(?=\/results)/g); 
        var baseURL = window.location.protocol+'//'+window.location.host+rootPath+'/gallery/advanced/results/recordtype/'+recordType+'/page/';
        var query = window.location.search;
        var currPage = 1;
        var loadButton = $('.gallery-load-more');
        Gallery.loadPage(loadButton, baseURL, currPage, query);

        currPage++;
        loadButton.click(function(){
            Gallery.loadPage(loadButton, baseURL, currPage, query);
            currPage++;
        });
    }
};
//Init Gallery
$(document).ready(function() {
    Gallery.init(); 
});