<?php
namespace CrossingBorders\XBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\ArrayInput;

use CrossingBorders\XBundle\Entity\RecordType;
use CrossingBorders\XBundle\Entity\Record;
use CrossingBorders\XBundle\Entity\RecordTypeField;

use CrossingBorders\XBundle\Entity\Choice;
use CrossingBorders\XBundle\Entity\TableRow;
use CrossingBorders\XBundle\Entity\TableColumn;
use CrossingBorders\XBundle\Entity\TableCell;
use CrossingBorders\XBundle\Entity\StoredFile;
use Symfony\Component\HttpFoundation\File\File;


class TestDataCommand extends ContainerAwareCommand {
    protected function configure() {
        $this
            ->setName('testdata:create')
            ->setDescription('Create some test data')
            ->addOption(
                'recordTypes',
                null,
                InputOption::VALUE_OPTIONAL,
                'How many record types do you want to create?',
                50
            )->addOption(
                'recordsByType',
                null,
                InputOption::VALUE_OPTIONAL,
                'How many record do you want to create for each type?',
                100
            )
        ;
    }
    protected function execute(InputInterface $input, OutputInterface $output) {
        $command = $this->getApplication()->find('doctrine:fixtures:load');
        $command->run(new ArrayInput(array('')), $output);
        
        $objectManager = $this->getContainer()->get('doctrine.orm.entity_manager');
        
        $fieldTypeRepository = $objectManager->getRepository('CrossingBordersXBundle:FieldType');
        $fieldTypeBoolean = $fieldTypeRepository->findOneByName("Boolean");
        $fieldTypeChoice = $fieldTypeRepository->findOneByName("Choice");
        $fieldTypeDate = $fieldTypeRepository->findOneByName("Date");
        $fieldTypeDateTime = $fieldTypeRepository->findOneByName("Date Time");
        $fieldTypeFloat = $fieldTypeRepository->findOneByName("Float");
        $fieldTypeInteger = $fieldTypeRepository->findOneByName("Integer");
        $fieldTypeRecord = $fieldTypeRepository->findOneByName("Record");
        $fieldTypeString = $fieldTypeRepository->findOneByName("String");
        $fieldTypeText = $fieldTypeRepository->findOneByName("Text");
        $fieldTypeTime = $fieldTypeRepository->findOneByName("Time");
        $fieldTypeTable = $fieldTypeRepository->findOneByName("Table");
        $fieldTypeFile = $fieldTypeRepository->findOneByName("File");
        $fieldTypeImage = $fieldTypeRepository->findOneByName("Image");
        $fieldTypeAudio = $fieldTypeRepository->findOneByName("Audio");
        $fieldTypeVideo = $fieldTypeRepository->findOneByName("Video");
        
        $recordTypeCountry = $objectManager->getRepository('CrossingBordersXBundle:RecordType')->findOneByName('Country');
        $countryRecords = $objectManager->getRepository('CrossingBordersXBundle:Record')->findBy(array('recordType' => $recordTypeCountry->getId()));
        
        $numRecordTypes = $input->getOption('recordTypes');
        $numRecordsByType = $input->getOption('recordsByType');
        
        $userManager = $this->getContainer()->get('fos_user.user_manager');
        $groupManager = $this->getContainer()->get('fos_user.group_manager');
        
        /* Init Users */
        $userDummy = $userManager->createUser();
        $userDummy->setUsername('user');
        $userDummy->setEmail('user@fct.unl.pt');
        $userDummy->setPlainPassword('user');
        $userDummy->setEnabled('true');
        $userDummy->addGroup($groupManager->findGroupByName('User'));
        $userManager->updateUser($userDummy);
                
        $userDummy2 = $userManager->createUser();
        $userDummy2->setUsername('user2');
        $userDummy2->setEmail('user2@fct.unl.pt');
        $userDummy2->setPlainPassword('user2');
        $userDummy2->setEnabled('true');
        $userDummy2->addGroup($groupManager->findGroupByName('User'));
        $userManager->updateUser($userDummy2);
        
        $userEditor = $userManager->createUser();
        $userEditor->setUsername('editor');
        $userEditor->setEmail('editor@fct.unl.pt');
        $userEditor->setPlainPassword('editor');
        $userEditor->setEnabled('true');
        $userEditor->addGroup($groupManager->findGroupByName('Editor'));
        $userManager->updateUser($userEditor);
        
        $userEditor2 = $userManager->createUser();
        $userEditor2->setUsername('editor2');
        $userEditor2->setEmail('editor2@fct.unl.pt');
        $userEditor2->setPlainPassword('editor2');
        $userEditor2->setEnabled('true');
        $userEditor2->addGroup($groupManager->findGroupByName('Editor'));
        $userManager->updateUser($userEditor2);
        
        $userAdmin = $userManager->createUser();
        $userAdmin->setUsername('admin');
        $userAdmin->setEmail('admin@fct.unl.pt');
        $userAdmin->setPlainPassword('admin');
        $userAdmin->setEnabled('true');
        $userAdmin->addGroup($groupManager->findGroupByName('Administrator'));
        $userManager->updateUser($userAdmin);
        
        $userAdmin2 = $userManager->createUser();
        $userAdmin2->setUsername('admin2');
        $userAdmin2->setEmail('admin2@fct.unl.pt');
        $userAdmin2->setPlainPassword('admin2');
        $userAdmin2->setEnabled('true');
        $userAdmin2->addGroup($groupManager->findGroupByName('Administrator'));
        $userManager->updateUser($userAdmin2);
        
        $recordTypeSample = new RecordType();
        $recordTypeSample->setName('Sample');
        $recordTypeSampleImageField = new RecordTypeField();
        $recordTypeSampleImageField->setName('Image');
        $recordTypeSampleImageField->setFieldType($fieldTypeImage);
        $recordTypeSampleImageField->setRequired(true);
        $recordTypeSample->addRecordTypeField($recordTypeSampleImageField);
        $objectManager->persist($recordTypeSample);
        for($i = 0; $i < $numRecordTypes; $i++) {
            $sampleImageId = $i % 10;
            
            $recordSample = new Record();
            $recordSample->setName("Sample $i");
            $recordSample->setRecordType($recordTypeSample);
            $objectManager->persist($recordSample);
            
            $imageFieldValue = new \CrossingBorders\XBundle\Entity\FieldValueFile();
            $imageFieldValue->setPath($sampleImageId.'.tif');
            $imageFieldValue->setOriginalName($sampleImageId.'.tif');
            $imageFieldValue->setRecordTypeField($recordTypeSampleImageField);
            $imageFieldValue->setRecord($recordSample);
            $objectManager->persist($imageFieldValue);
        }
        for($i = 0; $i < $numRecordTypes; $i++) {
            $recordType = new RecordType();
            $recordType->setName("RecordType $i");
            $objectManager->persist($recordType);
            for($j = 0; $j < $numRecordsByType; $j++) {
                $record = new Record();
                $record->setName("Record $i-$j");
                $record->setRecordType($recordType);
                $objectManager->persist($record);
            }
        }
        
        /* Sample Record Type withh All Field Types */
        $recordTypeAllFields = new RecordType();
        $recordTypeAllFields->setName('All Fields');
        /* Boolean Field Type */
        $recordTypeAllFieldsBooleanField = new RecordTypeField();
        $recordTypeAllFieldsBooleanField->setName('Boolean');
        $recordTypeAllFieldsBooleanField->setFieldType($fieldTypeBoolean);
        $recordTypeAllFieldsBooleanField->setRequired(false);
        $recordTypeAllFieldsBooleanField->setDisplayOrder(0);
        
        /* Choice Field Type */
        $recordTypeAllFieldsChoiceField = new RecordTypeField();
        $recordTypeAllFieldsChoiceField->setName('Choice');
        $recordTypeAllFieldsChoiceField->setFieldType($fieldTypeChoice);
        $recordTypeAllFieldsChoiceField->setRequired(false);
        $recordTypeAllFieldsChoiceField->setDisplayOrder(1);
        
        $choiceYes = new Choice();
        $choiceYes->setDisplayOrder(0);
        $choiceYes->setName('Yes');

        $choiceNo = new Choice();
        $choiceNo->setDisplayOrder(1);
        $choiceNo->setName('Maybe');
        
        $choiceMaybe = new Choice();
        $choiceMaybe->setDisplayOrder(2);
        $choiceMaybe->setName('No');
        
        $recordTypeAllFieldsChoiceField->addChoice($choiceYes);
        $recordTypeAllFieldsChoiceField->addChoice($choiceNo);
        $recordTypeAllFieldsChoiceField->addChoice($choiceMaybe);
        
        /* Date Field Type */
        $recordTypeAllFieldsDateField = new RecordTypeField();
        $recordTypeAllFieldsDateField->setName('Date');
        $recordTypeAllFieldsDateField->setFieldType($fieldTypeDate);
        $recordTypeAllFieldsDateField->setRequired(false);
        $recordTypeAllFieldsDateField->setDisplayOrder(2);
        
        /* Date Time Field Type */
        $recordTypeAllFieldsDateTimeField = new RecordTypeField();
        $recordTypeAllFieldsDateTimeField->setName('Date Time');
        $recordTypeAllFieldsDateTimeField->setFieldType($fieldTypeDateTime);
        $recordTypeAllFieldsDateTimeField->setRequired(false);
        $recordTypeAllFieldsDateTimeField->setDisplayOrder(3);
        
        /* Float Field Type */
        $recordTypeAllFieldsFloatField = new RecordTypeField();
        $recordTypeAllFieldsFloatField->setName('Float');
        $recordTypeAllFieldsFloatField->setFieldType($fieldTypeFloat);
        $recordTypeAllFieldsFloatField->setRequired(false);
        $recordTypeAllFieldsFloatField->setDisplayOrder(4);
        
        /* Integer Field Type */
        $recordTypeAllFieldsIntegerField = new RecordTypeField();
        $recordTypeAllFieldsIntegerField->setName('Integer');
        $recordTypeAllFieldsIntegerField->setFieldType($fieldTypeInteger);
        $recordTypeAllFieldsIntegerField->setRequired(false);
        $recordTypeAllFieldsIntegerField->setDisplayOrder(5);
        
        /* Record Field Type */
        $recordTypeAllFieldsRecordField = new RecordTypeField();
        $recordTypeAllFieldsRecordField->setName('Record');
        $recordTypeAllFieldsRecordField->setFieldType($fieldTypeRecord);
        $recordTypeAllFieldsRecordField->setRequired(false);
        $recordTypeAllFieldsRecordField->setDisplayOrder(6);
        $recordTypeAllFieldsRecordField->setReferencedRecordType($recordTypeCountry);
        $recordTypeAllFieldsRecordField->setMultipleSelection(false);
        
        /* String Field Type */
        $recordTypeAllFieldsStringField = new RecordTypeField();
        $recordTypeAllFieldsStringField->setName('String');
        $recordTypeAllFieldsStringField->setFieldType($fieldTypeString);
        $recordTypeAllFieldsStringField->setRequired(false);
        $recordTypeAllFieldsStringField->setDisplayOrder(7);
        
        /* Text Field Type */
        $recordTypeAllFieldsTextField = new RecordTypeField();
        $recordTypeAllFieldsTextField->setName('Text');
        $recordTypeAllFieldsTextField->setFieldType($fieldTypeText);
        $recordTypeAllFieldsTextField->setRequired(false);
        $recordTypeAllFieldsTextField->setDisplayOrder(8);
        
        /* Time Field Type */
        $recordTypeAllFieldsTimeField = new RecordTypeField();
        $recordTypeAllFieldsTimeField->setName('Time');
        $recordTypeAllFieldsTimeField->setFieldType($fieldTypeTime);
        $recordTypeAllFieldsTimeField->setRequired(false);
        $recordTypeAllFieldsTimeField->setDisplayOrder(9);
        
        /* Column Field Type */
        $recordTypeAllFieldsTableField = new RecordTypeField();
        $recordTypeAllFieldsTableField->setName('Table');
        $recordTypeAllFieldsTableField->setFieldType($fieldTypeTable);
        $recordTypeAllFieldsTableField->setRequired(false);
        $recordTypeAllFieldsTableField->setDisplayOrder(10);

        $column1 = new TableColumn();
        $column1->setDisplayOrder(0);
        $column1->setName('Column 1');

        $column2 = new TableColumn();
        $column2->setDisplayOrder(1);
        $column2->setName('Column 2');
        
        $column3 = new TableColumn();
        $column3->setDisplayOrder(2);
        $column3->setName('Column 3');
        
        $recordTypeAllFieldsTableField->addTableColumn($column1);
        $recordTypeAllFieldsTableField->addTableColumn($column2);
        $recordTypeAllFieldsTableField->addTableColumn($column3);
        
        /* File Field Type */
        $recordTypeAllFieldsFileField = new RecordTypeField();
        $recordTypeAllFieldsFileField->setName('File');
        $recordTypeAllFieldsFileField->setFieldType($fieldTypeFile);
        $recordTypeAllFieldsFileField->setRequired(false);
        $recordTypeAllFieldsFileField->setDisplayOrder(11);
        
        /* Image Field Type */
        $recordTypeAllFieldsImageField = new RecordTypeField();
        $recordTypeAllFieldsImageField->setName('Image');
        $recordTypeAllFieldsImageField->setFieldType($fieldTypeImage);
        $recordTypeAllFieldsImageField->setRequired(false);
        $recordTypeAllFieldsImageField->setDisplayOrder(12);
        
        /* Audio Field Type */
        $recordTypeAllFieldsAudioField = new RecordTypeField();
        $recordTypeAllFieldsAudioField->setName('Audio');
        $recordTypeAllFieldsAudioField->setFieldType($fieldTypeAudio);
        $recordTypeAllFieldsAudioField->setRequired(false);
        $recordTypeAllFieldsAudioField->setDisplayOrder(13);
        
        /* Video Field Type */
        $recordTypeAllFieldsVideoField = new RecordTypeField();
        $recordTypeAllFieldsVideoField->setName('Video');
        $recordTypeAllFieldsVideoField->setFieldType($fieldTypeVideo);
        $recordTypeAllFieldsVideoField->setRequired(false);
        $recordTypeAllFieldsVideoField->setDisplayOrder(14);
        
        /* Add Fields to the Record Type */
        $recordTypeAllFields->addRecordTypeField($recordTypeAllFieldsBooleanField);
        $recordTypeAllFields->addRecordTypeField($recordTypeAllFieldsChoiceField);
        $recordTypeAllFields->addRecordTypeField($recordTypeAllFieldsDateField);
        $recordTypeAllFields->addRecordTypeField($recordTypeAllFieldsDateTimeField);
        $recordTypeAllFields->addRecordTypeField($recordTypeAllFieldsFloatField);
        $recordTypeAllFields->addRecordTypeField($recordTypeAllFieldsIntegerField);
        $recordTypeAllFields->addRecordTypeField($recordTypeAllFieldsRecordField);
        $recordTypeAllFields->addRecordTypeField($recordTypeAllFieldsStringField);
        $recordTypeAllFields->addRecordTypeField($recordTypeAllFieldsTextField);
        $recordTypeAllFields->addRecordTypeField($recordTypeAllFieldsTimeField);
        $recordTypeAllFields->addRecordTypeField($recordTypeAllFieldsTableField);
        $recordTypeAllFields->addRecordTypeField($recordTypeAllFieldsFileField);
        $recordTypeAllFields->addRecordTypeField($recordTypeAllFieldsImageField);
        $recordTypeAllFields->addRecordTypeField($recordTypeAllFieldsAudioField);
        $recordTypeAllFields->addRecordTypeField($recordTypeAllFieldsVideoField);
        
        for($i = 0; $i < $numRecordsByType; $i++) {
            $record = new Record();
            $record->setName("All Fields Record $i");
            $record->setRecordType($recordTypeAllFields);
            switch ($i % 2) {
                case 0:
                    $record->__set('Boolean', true);
                    break;
                case 1:
                    $record->__set('Boolean', false);
                    break;
                default:
                    $record->__set('Boolean', true);
                    break;
            }
            switch ($i % 3) {
                case 0:
                    $record->__set('Choice', $choiceYes);
                    break;
                case 1:
                    $record->__set('Choice', $choiceNo);
                    break;
                case 2:
                    $record->__set('Choice', $choiceMaybe);
                    break;
                default:
                    $record->__set('Choice', $choiceYes);
                    break;
            }
            $record->__set('Date', new \DateTime((1900+$i).'-01-01'));
            $record->__set('Date Time', new \DateTime((1900+($numRecordsByType-$i)).'-12-31 23:59:59'));
            $record->__set('Float', $i+0.1*$i);
            $record->__set('Integer', 2*$i);
            $record->__set('Record', $countryRecords[$i % count($countryRecords)]);
            $record->__set('String','Hello World');
            $record->__set('Text', 'Hello Longer Text');
            $record->__set('Time', new \DateTime('24:00:00'));

            $tableRows = array();
            for($j = 0; $j <= $i * 0.5; $j++) {
                $tableRow = new TableRow();
                $tableCell1 = new TableCell();
                $tableCell1->setColumn($column1);
                $tableCell1->setValue("Hello Cell $j,1");
                $tableCell2 = new TableCell();
                $tableCell2->setColumn($column2);
                $tableCell2->setValue("Hello Cell $j,2");
                $tableCell3 = new TableCell();
                $tableCell3->setColumn($column3);
                $tableCell3->setValue("Hello Cell $j,3");
                $tableRow->addCell($tableCell1);
                $tableRow->addCell($tableCell2);
                $tableRow->addCell($tableCell3);
                $tableRows[] = $tableRow;
            }
            $record->__set('Table', $tableRows); 
            
            $record->__set('File',  new File(StoredFile::getUploadRootDir().'/document.pdf'));
            $record->__set('Image', new File(StoredFile::getUploadRootDir().'/image.jpg'));
            $record->__set('Audio', new File(StoredFile::getUploadRootDir().'/audio.mp3'));
            $record->__set('Video', new File(StoredFile::getUploadRootDir().'/video.mp4'));
            
            $objectManager->persist($record);
        }
        /* Persist the record type */
        $objectManager->persist($recordTypeAllFields);
        /* Save everything to the database */
        $objectManager->flush();
    }
}
