<?php
namespace CrossingBorders\XBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class CustomCommentSecurityCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container) {
        $container->getDefinition('fos_comment.acl.comment.security')
                  ->setClass('CrossingBorders\XBundle\Acl\CustomSecurityCommentAcl');
        $container->getDefinition('fos_comment.acl.thread.security')
                  ->setClass('CrossingBorders\XBundle\Acl\CustomSecurityThreadAcl');
        $container->getDefinition('fos_comment.acl.vote.security')
                  ->setClass('CrossingBorders\XBundle\Acl\CustomSecurityVoteAcl');
    }
}