<?php
namespace CrossingBorders\XBundle\Twig;

class DeleteFormExtension extends \Twig_Extension {
    private $environment;
    public function initRuntime(\Twig_Environment $environment) {
        $this->environment = $environment;
    }
    public function getFunctions() {
        return array(
            'form_delete' => new \Twig_Function_Node('Symfony\Bridge\Twig\Node\SearchAndRenderBlockNode', array('is_safe' => array('html'))),
        );
    }
    public function formDelete() {
        return $this->environment->renderBlock("form_delete");
    }
    public function getName()
    {
        return 'delete_form_extension';
    }
}
