<?php
namespace CrossingBorders\XBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 */
class FieldValueBoolean extends FieldValue {
    /**
     * @ORM\Column(type="boolean")
     */
    protected $value;
    
    /**
     * Set value
     *
     * @param boolean $value
     * @return FieldValueBoolean
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return boolean 
     */
    public function getValue()
    {
        return $this->value;
    }
    public function getType() {
        return 'Boolean';
    }
}
