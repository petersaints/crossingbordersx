<?php
namespace CrossingBorders\XBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 */
class RecordType extends Base {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\Column(type="string", unique=true, nullable=false)
     */
    protected $name;
    /**
     * @ORM\OneToMany(targetEntity="CrossingBorders\XBundle\Entity\RecordTypeField", mappedBy="recordType", cascade={"persist","remove"})
     * @ORM\OrderBy({"displayOrder" = "ASC"})
     */
    protected $recordTypeFields;
    /**
     * @ORM\OneToMany(targetEntity="CrossingBorders\XBundle\Entity\Record", mappedBy="recordType", cascade={"remove"})
     */
    protected $records;
    /**
     * @ORM\OneToMany(targetEntity="CrossingBorders\XBundle\Entity\TableColumn", mappedBy="recordType", cascade={"persist"})
     */
    private $tableColumns;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->recordTypeFields = new \Doctrine\Common\Collections\ArrayCollection();
        $this->records = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tableColumns = new \Doctrine\Common\Collections\ArrayCollection();
    }
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    /**
     * Set name
     *
     * @param string $name
     * @return RecordType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }
    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Add fieldTypes
     *
     * @param \CrossingBorders\XBundle\Entity\RecordTypeField $fieldTypes
     * @return RecordType
     */
    public function addRecordTypeField(\CrossingBorders\XBundle\Entity\RecordTypeField $recordTypeFields)
    {
        $recordTypeFields->setRecordType($this);
        $this->recordTypeFields[] = $recordTypeFields;
        return $this;
    }

    /**
     * Remove fieldTypes
     *
     * @param \CrossingBorders\XBundle\Entity\RecordTypeField $fieldTypes
     */
    public function removeRecordTypeField(\CrossingBorders\XBundle\Entity\RecordTypeField $recordTypeFields)
    {
        $this->recordTypeFields->removeElement($recordTypeFields);
    }
    /**
     * Get fieldTypes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRecordTypeFields()
    {
        return $this->recordTypeFields;
    }
    /**
     * Clear fieldTypes
     */
    public function clearRecordTypeFields()
    {
        $this->recordTypeFields = new \Doctrine\Common\Collections\ArrayCollection();
    }
    /**
     * Add records
     *
     * @param \CrossingBorders\XBundle\Entity\Record $records
     * @return RecordType
     */
    public function addRecord(\CrossingBorders\XBundle\Entity\Record $records)
    {
        $this->records[] = $records;

        return $this;
    }
    /**
     * Remove records
     *
     * @param \CrossingBorders\XBundle\Entity\Record $records
     */
    public function removeRecord(\CrossingBorders\XBundle\Entity\Record $records)
    {
        $this->records->removeElement($records);
    }
    /**
     * Get records
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRecords()
    {
        return $this->records;
    }
    
    /**
     * Add tableColumns
     *
     * @param \CrossingBorders\XBundle\Entity\TableColumn $tableColumns
     * @return RecordType
     */
    public function addTableColumn(\CrossingBorders\XBundle\Entity\TableColumn $tableColumns)
    {
        $tableColumns->setRecordTypeField($this);
        $this->tableColumns[] = $tableColumns;
        return $this;
    }

    /**
     * Remove tableColumns
     *
     * @param \CrossingBorders\XBundle\Entity\TableColumn $tableColumns
     */
    public function removeTableColumn(\CrossingBorders\XBundle\Entity\TableColumn $tableColumns)
    {
        $this->tableColumns->removeElement($tableColumns);
    }

    /**
     * Get tableColumns
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTableColumns()
    {
        return $this->tableColumns;
    }
    
    
    public function __toString() {
        return $this->name;
    }
}