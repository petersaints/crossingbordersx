<?php
namespace CrossingBorders\XBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 */
class FieldValueText extends FieldValue {
    /**
     * @ORM\Column(type="text")
     */
    protected $value;

    /**
     * Set value
     *
     * @param string $value
     * @return FieldValueText
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var \CrossingBorders\XBundle\Entity\RecordTypeField
     */
    protected $recordTypeField;

    /**
     * @var \CrossingBorders\XBundle\Entity\Record
     */
    protected $record;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set recordTypeField
     *
     * @param \CrossingBorders\XBundle\Entity\RecordTypeField $recordTypeField
     * @return FieldValueText
     */
    public function setRecordTypeField(\CrossingBorders\XBundle\Entity\RecordTypeField $recordTypeField = null)
    {
        $this->recordTypeField = $recordTypeField;

        return $this;
    }

    /**
     * Get recordTypeField
     *
     * @return \CrossingBorders\XBundle\Entity\RecordTypeField 
     */
    public function getRecordTypeField()
    {
        return $this->recordTypeField;
    }

    /**
     * Set record
     *
     * @param \CrossingBorders\XBundle\Entity\Record $record
     * @return FieldValueText
     */
    public function setRecord(\CrossingBorders\XBundle\Entity\Record $record = null)
    {
        $this->record = $record;

        return $this;
    }

    /**
     * Get record
     *
     * @return \CrossingBorders\XBundle\Entity\Record 
     */
    public function getRecord()
    {
        return $this->record;
    }

    public function getType() {
        return 'Text';
    }
}
