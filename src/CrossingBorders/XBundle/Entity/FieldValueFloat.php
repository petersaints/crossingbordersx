<?php
namespace CrossingBorders\XBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 */
class FieldValueFloat extends FieldValue {
    /**
     * @ORM\Column(type="float")
     */
    protected $value;
    
    /**
     * Set value
     *
     * @param float $value
     * @return FieldValueFloat
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return float 
     */
    public function getValue()
    {
        return $this->value;
    }
    public function getType() {
        return 'Float';
    }
}
