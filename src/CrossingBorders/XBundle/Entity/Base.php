<?php
namespace CrossingBorders\XBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use DateTime;

/**
 * @ORM\MappedSuperclass
 * @ORM\HasLifecycleCallbacks
 */
class Base {
    /**
     * @ORM\Column(type="datetime")
     */
    private $created;
    /**
     * @ORM\Column(type="datetime")
     */
    private $updated;
    /**
     * @ORM\Column(type="integer")
     */
    private $versionNumber;
    /**
     * @ORM\PrePersist
     */
    public function preCreate() {
        $this->versionNumber = 0;
        $this->created = $this->updated = new DateTime("now");
    }
    /**
     * @ORM\PreUpdate
     */
    public function preUpdate() {
        $this->versionNumber++;
        $this->updated = new DateTime("now");
    }
    public function getCreated() {
        return $this->created;
    }
    public function getUpdated() {
        return $this->updated;
    }
    public function setCreated($created) {
        $this->created = $created;
        return $this;
    }
    public function setUpdated($updated) {
        $this->updated = $updated;
        return $this;
    }
    public function getVersionNumber() {
        return $this->versionNumber;
    }
    public function setVersionNumber($versionNumber) {
        $this->versionNumber = $versionNumber;
        return $this;
    }
    public function copy($em) {
        $clone = clone $this;
        $em->persist($clone);
        $em->flush();
        return $clone;
    }
}
