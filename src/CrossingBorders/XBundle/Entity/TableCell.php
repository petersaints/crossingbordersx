<?php
namespace CrossingBorders\XBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * TableCell
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class TableCell
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\ManyToOne(targetEntity="CrossingBorders\XBundle\Entity\TableRow", inversedBy="cells", cascade={"persist"})
     * @ORM\JoinColumn(name="row_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    protected $row;
    /**
     * @ORM\ManyToOne(targetEntity="CrossingBorders\XBundle\Entity\TableColumn", inversedBy="cells", cascade={"persist"})
     * @ORM\JoinColumn(name="column_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    protected $column;
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $value;
    /**
     * @ORM\ManyToOne(targetEntity="CrossingBorders\XBundle\Entity\Record", inversedBy="tableCells", cascade={"persist"})
     * @ORM\JoinColumn(name="record_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    protected $record;
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set row
     *
     * @param \CrossingBorders\XBundle\Entity\TableRow $row
     * @return TableCell
     */
    public function setRow(\CrossingBorders\XBundle\Entity\TableRow $row = null)
    {
        $row->addCell($this);
        $this->row = $row;
        return $this;
    }

    /**
     * Get row
     *
     * @return \CrossingBorders\XBundle\Entity\TableRow 
     */
    public function getRow()
    {
        return $this->row;
    }

    /**
     * Set column
     *
     * @param \CrossingBorders\XBundle\Entity\TableColumn $column
     * @return TableCell
     */
    public function setColumn(\CrossingBorders\XBundle\Entity\TableColumn $column = null)
    {
        $column->addCell($this);
        $this->column = $column;
        return $this;
    }

    /**
     * Get column
     *
     * @return \CrossingBorders\XBundle\Entity\TableColumn 
     */
    public function getColumn()
    {
        return $this->column;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return TableCell
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }
      
    /**
     * Set record
     *
     * @param \CrossingBorders\XBundle\Entity\Record $record
     * @return TableCell
     */
    function setRecord($record) {
        $this->record = $record;
        return $this;
    }
    
    /**
     * Get record
     *
     * @return \CrossingBorders\XBundle\Entity\Record 
     */
    function getRecord() {
        return $this->record;
    }


    
    public function __toString() {
        if(is_null($this->value) && is_null($this->record)) {
            return '';
        } else {
            return (string) $this->value;
        }
    }
}
