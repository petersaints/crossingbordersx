<?php
namespace CrossingBorders\XBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class StoredFile {
    //Source: http://en.wikipedia.org/wiki/Internet_media_type#Type_image
    public static $imageMimeTypes = array('image/gif',
                                          'image/jpeg',
                                          'image/pjpeg',
                                          'image/png',
                                          'image/tiff',
                                          'image/example');
    public static $videoMimeTypes = array('application/ogg',
                                          'video/avi',
                                          'video/example',
                                          'video/mpeg',
                                          'video/mp4',
                                          'video/ogg',
                                          'video/quicktime',
                                          'video/webm',
                                          'video/x-matroska',
                                          'video/x-ms-wmv',
                                          'video/x-flv');
    public static $audioMimeTypes = array('audio/basic',
                                          'audio/L24',
                                          'audio/mp4',
                                          'audio/mpeg',
                                          'audio/ogg',
                                          'audio/opus',
                                          'audio/vorbis',
                                          'audio/vorbis',
                                          'audio/vnd.rn-realaudio',
                                          'audio/vnd.wave',
                                          'audio/webm',
                                          'audio/example');
    
    protected static $thumbnailSuffix = '.thumbnail.jpeg';
    protected static $thumbnailWidth = 200;
    protected static $thumbnailHeight = 150;
    protected static $thumbnailJpegCompression = 60;
    
    protected static $defaultImageSuffix = '.default.jpeg';
    protected static $defaultImageWidth = 1600;
    protected static $defaultImageHeight = 1200;
    protected static $defaultImageJpegCompression = 80;
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $path;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $originalName;
    private $file;
    private $temp;
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }
    /**
     * Set value
     *
     * @param File $value
     * @return FieldValueString
     */
    public function setValue($value) {
        $this->setFile($value);
        return $this;
    }
    /**
     * Get value
     *
     * @return File 
     */
    public function getValue() {
        return $this->getFile();
    }
    /**
     * Set path
     *
     * @param string $path
     * @return FieldValueString
     */
    public function setPath($path) {
        $this->path = $path;
        return $this;
    }
    /**
     * Get path
     *
     * @return string 
     */
    public function getPath() {
        //If the path is not set try to return an old path so that a still not deleted file may be used instead!
        if(is_null($this->path)) {
            return $this->temp;
        } else {
            return $this->path;
        }
    }
    /**
     * Set originalName
     *
     * @param string $orignalName
     * @return FieldValueString
     */
    public function setOriginalName($orignalName) {
        $this->originalName = $orignalName;
        return $this;
    }
    /**
     * Get originalName
     *
     * @return string 
     */
    public function getOriginalName() {
        return $this->originalName;
    }
    /**
     * Sets file.
     *
     * @param File $file
     */
    public function setFile($file = null) {
        if($file instanceof File) {
            $this->file = $file;
            // check if we have an old image path
            if (isset($this->path)) {
                // store the old name to delete after the update
                $this->temp = $this->path;
                $this->path = null;
            } else {
                $this->path = 'initial';
            }
        } else if(is_string($file) && $file == 'delete') {
            $this->removeUpload();
        }
        return $this;
    }
    /**
     * Get file.
     *
     * @return File
     */
    public function getFile() {
        if($this->file === null) {
            return new File($this->getAbsolutePath(), true);
        } else {
            return $this->file;
        }
    }
    //Get Multiple Types of Path Methods
    public function getAbsolutePath() {
        return is_null($this->getPath())
            ? null
            : self::getUploadRootDir().'/'.$this->getPath();
    }
    public function getWebPath() {
        return is_null($this->getPath())
            ? null
            : $this->getUploadDir().'/'.$this->getPath().StoredFile::$defaultImageSuffix;
    }
    public function getWebPathOriginal() {
        return is_null($this->getPath())
            ? null
            : $this->getUploadDir().'/'.$this->getPath();
    }
    public function getWebPathThumbnail() {
        return is_null($this->getPath())
            ? null
            : $this->getUploadDir().'/'.$this->getPath().StoredFile::$thumbnailSuffix;
    }
    public static function getUploadRootDir() {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../../web/'.self::getUploadDir();
    }
    public static function getUploadDir() {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads';
    }
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload() {
        $file = $this->getFile();
        if (null !== $file && $file instanceof UploadedFile) {
            // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), true));
            $this->path = $filename.'.'.$this->getFile()->guessExtension();
            $this->originalName = $this->getFile()->getClientOriginalName();
        } else {
            $this->path = $this->getFile()->getFilename();
            $this->originalName = $this->getFile()->getFilename();
        }
    }
    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload() {
        if (null === $this->getFile()) {
            return;
        }
        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->getFile()->move(self::getUploadRootDir(), $this->path);
        // check if we have an old image
        if (isset($this->temp)) {
            // delete the old image
            if(file_exists(self::getUploadRootDir().'/'.$this->temp)){
                //unlink($this->getUploadRootDir().'/'.$this->temp);
                array_map('unlink', glob(self::getUploadRootDir().'/'.$this->temp.'*'));
            }
            // clear the temp image path
            $this->temp = null;
        }
        $this->file = null;
        $this->generateThumbnail();
        $this->generateDefaultImage();
    }
    /**
     * @ORM\PreRemove()
     */
    public function saveFilenameForRemoval() {
        $this->temp = $this->getAbsolutePath();
    }
    /**
     * @ORM\PostRemove()
     */
    public function removeFile() {
        $file = $this->temp;
        if ($file) {
            array_map('unlink', glob($file.'*'));
        }
    }
    public function getExists() {
        try {
            $filePath = $this->getAbsolutePath();
            new File($filePath);
            return true;
        } catch(FileNotFoundException $e) {
            return false;
        }
    }
    public function getIsImage() {
        try {
            $file = new File($this->getAbsolutePath());
            return in_array($file->getMimeType(), self::$imageMimeTypes);
        } catch(FileNotFoundException $e) {
            return false;
        }
    }
    public function getIsAudio() {
        try {
            $file = new File($this->getAbsolutePath());
            return in_array($file->getMimeType(), self::$audioMimeTypes);
        } catch(FileNotFoundException $e) {
            return false;
        }
    }
    public function getIsVideo() {
        try {
            $file = new File($this->getAbsolutePath());
            return in_array($file->getMimeType(), self::$videoMimeTypes);
        } catch(FileNotFoundException $e) {
            return false;
        }
    }
    private function generateThumbnail() {
        if($this->getIsImage()) {
            $thumbImage = new \Imagick($this->getAbsolutePath());
            //Set compression
            $thumbImage->setImageCompression(\Imagick::COMPRESSION_JPEG);
            $thumbImage->setImageCompressionQuality(StoredFile::$thumbnailJpegCompression);
            //Strip out unneeded meta data
            $thumbImage->stripImage();
            if($thumbImage->getImageWidth() >= StoredFile::$defaultImageWidth
            || $thumbImage->getImageHeight() >= StoredFile::$defaultImageHeight) {
                $thumbImage->thumbnailImage(StoredFile::$thumbnailWidth,
                            StoredFile::$thumbnailHeight,
                            true);
            }
            $thumbImage->writeImage($this->getAbsolutePath().StoredFile::$thumbnailSuffix);
            $thumbImage->destroy();
        }
    }
    private function generateDefaultImage() {
        if($this->getIsImage()) {
            $defaultImage = new \Imagick($this->getAbsolutePath()); 
            //Set compression
            $defaultImage->setImageCompression(\Imagick::COMPRESSION_JPEG);
            $defaultImage->setImageCompressionQuality(StoredFile::$defaultImageJpegCompression);
            //Strip out unneeded meta data
            $defaultImage->stripImage();
            if($defaultImage->getImageWidth() >= StoredFile::$defaultImageWidth
            || $defaultImage->getImageHeight() >= StoredFile::$defaultImageHeight) {
                $defaultImage->resizeImage(StoredFile::$defaultImageWidth,
                                           StoredFile::$defaultImageHeight,
                                           \Imagick::FILTER_LANCZOS, 1, true);
            }
            $defaultImage->writeImage($this->getAbsolutePath().StoredFile::$defaultImageSuffix);
            $defaultImage->destroy();
        }
    }
    public function getType() {
        return 'File';
    }
}