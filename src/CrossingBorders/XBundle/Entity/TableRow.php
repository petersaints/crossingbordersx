<?php

namespace CrossingBorders\XBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TableRow
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class TableRow implements \IteratorAggregate, \ArrayAccess
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\ManyToOne(targetEntity="CrossingBorders\XBundle\Entity\FieldValueTable", inversedBy="rows")
     * @ORM\JoinColumn(name="fieldValue_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    protected $fieldValue;
    /**
     * @ORM\OneToMany(targetEntity="CrossingBorders\XBundle\Entity\TableCell", mappedBy="row", cascade={"persist"}, orphanRemoval=true)
     */
    protected $cells;
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->cells = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set fieldValue
     *
     * @param \CrossingBorders\XBundle\Entity\FieldValueTable $fieldValue
     * @return TableRow
     */
    public function setFieldValue(\CrossingBorders\XBundle\Entity\FieldValueTable $fieldValue = null)
    {
        $this->fieldValue = $fieldValue;

        return $this;
    }

    /**
     * Get fieldValue
     *
     * @return \CrossingBorders\XBundle\Entity\FieldValueTable 
     */
    public function getFieldValue()
    {
        return $this->fieldValue;
    }

    /**
     * Add cells
     *
     * @param \CrossingBorders\XBundle\Entity\TableCell $cells
     * @return TableRow
     */
    public function addCell(\CrossingBorders\XBundle\Entity\TableCell $cells)
    {
        if (!$this->cells->contains($cells)) {
             $this->cells->add($cells);
        }
    }

    /**
     * Remove cells
     *
     * @param \CrossingBorders\XBundle\Entity\TableCell $cells
     */
    public function removeCell(\CrossingBorders\XBundle\Entity\TableCell $cells)
    {
        $this->cells->removeElement($cells);
    }
    /**
     * Get cells
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCells() {
        return $this->cells;
    }
    /**
     * TO DO: Maybe use a data transformer instead of this half-baked implementation
     * Fix the fact that the cells are stored in reverse on update
     */
    //ArrayAccess Implementation
    public function offsetExists($offset) {
        return isset($this->cells[$offset]);
    }
    public function offsetGet($offset) {
        return isset($this->cells[$offset]) ? $this->cells[$offset] : null;
    }
    public function offsetSet($offset, $value) {
        if (is_null($offset)) {
            $this->addCell($value);
        } else {
            $this->cells->set($offset,$value);
        }
    }
    public function offsetUnset($offset) {
        unset($this->cells[$offset]);
    }
    //IteratorAggregate Implementation
    public function getIterator() {
        $iterator = $this->cells->getIterator();
        return $iterator;
    }
    /**
     * @ORM\PostLoad()
     */
    public function onLoad() {
        if($this->cells->count() > 0
        && $this->cells[0]->getColumn() !== null) {
           $columns = $this->cells[0]->getColumn()->getRecordTypeField()->getTableColumns();
        }
        foreach($columns as $column) {
            $columnExists = false;
            foreach ($this->cells as $cell) {
                if($column === $cell->getColumn()) {
                    $columnExists = true;
                    break;
                }
            }
            if(!$columnExists) {
                $tableCell = new TableCell();
                $tableCell->setValue('');
                $tableCell->setRow($this);
                $tableCell->setColumn($column);
            }
        }
        $iterator = $this->cells->getIterator();
        $iterator->uasort(function ($a, $b) {
            if($a->getColumn() === null
            || $b->getColumn() === null) {
                return 0;
            } else {
                return ($a->getColumn()->getDisplayOrder() < $b->getColumn()->getDisplayOrder()) ? -1 : 1;
            }
        });
        $this->cells = new \Doctrine\Common\Collections\ArrayCollection(iterator_to_array($iterator));
    }
}
