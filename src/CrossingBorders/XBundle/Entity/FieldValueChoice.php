<?php
namespace CrossingBorders\XBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class FieldValueChoice extends FieldValue {
    /**
     * @ORM\ManyToMany(targetEntity="CrossingBorders\XBundle\Entity\Choice")
     */
    protected $value;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->value = new \Doctrine\Common\Collections\ArrayCollection();
    }
    /**
     * Add value
     *
     * @param \CrossingBorders\XBundle\Entity\Choice $value
     * @return FieldValueChoice
     */
    public function addValue(\CrossingBorders\XBundle\Entity\Choice $value)
    {
        if(!$this->getRecordTypeField()->getMultipleSelection()) {
            $this->value->clear();
        }
        if (!$this->value->contains($value)) {
             $this->value->add($value);
        }
        return $this;
    }
    /**
     * Remove value
     *
     * @param \CrossingBorders\XBundle\Entity\Choice $value
     */
    public function removeValue(\CrossingBorders\XBundle\Entity\Choice $value)
    {
        $this->value->removeElement($value);
    }
    /**
     * Get value
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getValue()
    {
        return $this->value;
    }
    /**
     * Get value
     *
     * @return \CrossingBorders\XBundle\Entity\Record
     */
    public function getSingleValue()
    {
        if($this->value->count() > 0
        &&!$this->getRecordTypeField()->getMultipleSelection()) {
            return $this->value[0];
        } else {
            return null;
        }
    }
    public function getNotSelectedChoices() {
        return array_diff($this->getRecordTypeField()->getChoices()->toArray(),
                          $this->value->toArray());
    }
    public function getIsMultipleChoice() {
        return $this->getRecordTypeField()->getMultipleSelection();
    }
    public function getType() {
        return 'Choice';
    }
}