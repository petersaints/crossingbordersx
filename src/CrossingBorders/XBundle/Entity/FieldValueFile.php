<?php
namespace CrossingBorders\XBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class FieldValueFile extends FieldValue {
    /** 
     * @ORM\OneToOne(targetEntity="CrossingBorders\XBundle\Entity\StoredFile", cascade={"persist","remove"}, orphanRemoval=true)
     */
    protected $storedFile;
    public function __construct() {
        $this->storedFile = new StoredFile();
    }
    /**
     * Set storedFile
     *
     * @param \CrossingBorders\XBundle\Entity\StoredFile $storedFile
     * @return FieldValueFile
     */
    public function setStoredFile(\CrossingBorders\XBundle\Entity\StoredFile $storedFile) {
        $this->storedFile = $storedFile;
        return $this;
    }
    /**
     * Get storedFile
     *
     * @return \CrossingBorders\XBundle\Entity\StoredFile 
     */
    public function getStoredFile() {
        return $this->storedFile;
    }
    /**
     * Set value
     *
     * @param File $value
     * @return FieldValueString
     */
    public function setValue($value) {
        $this->setFile($value);
        return $this;
    }
    /**
     * Get value
     *
     * @return File 
     */
    public function getValue() {
        return $this->getFile();
    }
    /**
     * Set path
     *
     * @param string $path
     * @return FieldValueString
     */
    public function setPath($path) {
        $this->storedFile->setPath($path);
        return $this;
    }
    /**
     * Get path
     *
     * @return string 
     */
    public function getPath() {
        if(!$this->getIsStoredFileNull()) {
            return $this->storedFile->getPath();
        } else {
            return null;
        }
    }
    /**
     * Set originalName
     *
     * @param string $orignalName
     * @return FieldValueString
     */
    public function setOriginalName($orignalName)
    {
        $this->storedFile->setOriginalName($orignalName);
        return $this;
    }
    /**
     * Get originalName
     *
     * @return string 
     */
    public function getOriginalName()
    {
        if(!$this->getIsStoredFileNull()) {
            return $this->storedFile->getOriginalName();
        } else {
             return null;
        }
    }
    /**
     * Sets file.
     *
     * @param File $file
     */
    public function setFile($file = null)
    {
        if($this->getIsStoredFileNull()) {
            $this->storedFile = new StoredFile();
        }
        $this->storedFile->setFile($file);
        return $this;
    }
    /**
     * Get file.
     *
     * @return File
     */
    public function getFile() {
        if(!$this->getIsStoredFileNull()) {
            return $this->storedFile->getFile();
        } else {
             return null;
        }
    }
    public function removeFile() {
        $this->storedFile = null;
    }
    public function getAbsolutePath() {
         if(!$this->getIsStoredFileNull()) {
             return $this->storedFile->getAbsolutePath();
         } else {
             return null;
         }
    }
    public function getWebPath() {
         if(!$this->getIsStoredFileNull()) {
             return $this->storedFile->getWebPath();
         } else {
             return null;
         }
    }
    public function getWebPathOriginal() {
         if(!$this->getIsStoredFileNull()) {
             return $this->storedFile->getWebPathOriginal();
         } else {
             return null;
         }
    }
    public function getWebPathThumbnail() {
         if(!$this->getIsStoredFileNull()) {
             return $this->storedFile->getWebPathThumbnail();
         } else {
             return null;
         }
    }
    protected function getUploadRootDir() {
        return StoredFile::getUploadRootDir();
    }
    protected function getUploadDir() {
         if(!$this->getIsStoredFileNull()) {
             return $this->storedFile->getUploadDir();
         } else {
             return null;
         }
    }
    public function getIsStoredFileNull() {
        return is_null($this->storedFile) || $this->storedFile->getPath() === 'initial';
    }
    public function getExists() {
         if(!$this->getIsStoredFileNull()) {
             return $this->storedFile->getExists();
         } else {
             return false;
         }
    }
    public function getIsImage() {
        if (!$this->getIsStoredFileNull()) {
            return $this->storedFile->getIsImage();
        } else {
            return false;
        }
    }
    public function getIsVideo() {
        if (!$this->getIsStoredFileNull()) {
            return $this->storedFile->getIsVideo();
        } else {
            return false;
        }
    }
    public function getIsAudio() {
        if (!$this->getIsStoredFileNull()) {
            return $this->storedFile->getIsAudio();
        } else {
            return false;
        }
    }
    public function getType() {
        return 'File';
    }
}
