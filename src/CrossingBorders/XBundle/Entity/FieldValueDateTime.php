<?php
namespace CrossingBorders\XBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 */
class FieldValueDateTime extends FieldValue {
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $value;
   
    /**
     * Set value
     *
     * @param \DateTime $value
     * @return FieldValueDateTime
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return \DateTime 
     */
    public function getValue()
    {
        return $this->value;
    }
    public function getType() {
        return 'DateTime';
    }
}
