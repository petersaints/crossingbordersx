<?php
namespace CrossingBorders\XBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 */
class FieldValueTable extends FieldValue {
    /**
     * @ORM\OneToMany(targetEntity="CrossingBorders\XBundle\Entity\TableRow", mappedBy="fieldValue", cascade={"persist"}, orphanRemoval=true)
     */
    protected $rows;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->rows = new \Doctrine\Common\Collections\ArrayCollection();
    }
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add rows
     *
     * @param \CrossingBorders\XBundle\Entity\TableRow $rows
     * @return FieldValueTable
     */
    public function addRow(\CrossingBorders\XBundle\Entity\TableRow $rows)
    {
        $rows->setFieldValue($this);
        if (!$this->rows->contains($rows)) {
             $this->rows->add($rows);
        }
        return $this;
    }

    /**
     * Remove rows
     *
     * @param \CrossingBorders\XBundle\Entity\TableRow $rows
     */
    public function removeRow(\CrossingBorders\XBundle\Entity\TableRow $rows)
    {
        $this->rows->removeElement($rows);
    }

    /**
     * Get rows
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRows()
    {
        return $this->rows;
    }
    
    public function getColumns() {
        return $this->getRecordTypeField()->getTableColumns();
    }
    
    public function getType() {
        return 'Table';
    }
}
