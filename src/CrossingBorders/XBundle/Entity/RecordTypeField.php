<?php
namespace CrossingBorders\XBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(name="record_type_field_unique_record_type_field_type_name_idx",
 *                               columns={"recordType_id", "fieldType_id", "name"}) } )
 */
class RecordTypeField extends Base {    
   /**
    * @ORM\Id
    * @ORM\Column(type="integer")
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    protected $id;
    /**
     * @ORM\ManyToOne(targetEntity="CrossingBorders\XBundle\Entity\RecordType", inversedBy="recordTypeFields")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $recordType;
    /**
     * @ORM\ManyToOne(targetEntity="CrossingBorders\XBundle\Entity\FieldType", inversedBy="recordTypeFields")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $fieldType;
    /**
     * @ORM\Column(type="string")
     * @Assert\Regex(
     *     pattern="/^[^_]+$/",
     *     match=true,
     *     message="Your name cannot contain an underscore"
     * )
     */
    protected $name; 
    /**
     * @ORM\Column(type="integer")
     */
    protected $displayOrder;
    /**
     * @ORM\OneToMany(targetEntity="CrossingBorders\XBundle\Entity\Choice", mappedBy="recordTypeField", cascade={"persist"})
     * @ORM\OrderBy({"displayOrder" = "ASC"})
     */
    private $choices;
    /**
     * @ORM\OneToMany(targetEntity="CrossingBorders\XBundle\Entity\TableColumn", mappedBy="recordTypeField", cascade={"persist"})
     * @ORM\OrderBy({"displayOrder" = "ASC"})
     */
    private $tableColumns;
    /**
     * @ORM\ManyToOne(targetEntity="CrossingBorders\XBundle\Entity\RecordType")
     */
    protected $referencedRecordType;
    /**
     * @ORM\Column(type="boolean")
     */
    protected $multipleSelection;
    /**
     * @ORM\Column(type="boolean")
     */
    protected $required;
    /**
     * @ORM\OneToMany(targetEntity="CrossingBorders\XBundle\Entity\FieldValue", mappedBy="recordTypeField", cascade={"persist"})
     */
    private $fieldValues;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->displayOrder = 0;
        $this->choices = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tableColumns = new \Doctrine\Common\Collections\ArrayCollection();
        $this->multipleSelection = false;
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    
    /**
     * Set name
     *
     * @param string $name
     * @return RecordTypeField
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set recordType
     *
     * @param \CrossingBorders\XBundle\Entity\RecordType $recordType
     * @return RecordTypeField
     */
    public function setRecordType(\CrossingBorders\XBundle\Entity\RecordType $recordType)
    {
        $this->recordType = $recordType;
        return $this;
    }

    /**
     * Get recordType
     *
     * @return \CrossingBorders\XBundle\Entity\RecordType 
     */
    public function getRecordType()
    {
        return $this->recordType;
    }

    /**
     * Set fieldType
     *
     * @param \CrossingBorders\XBundle\Entity\FieldType $fieldType
     * @return RecordTypeField
     */
    public function setFieldType(\CrossingBorders\XBundle\Entity\FieldType $fieldType)
    {
        $this->fieldType = $fieldType;

        return $this;
    }

    /**
     * Get fieldType
     *
     * @return \CrossingBorders\XBundle\Entity\FieldType 
     */
    public function getFieldType()
    {
        return $this->fieldType;
    }
    
    /**
     * Add choices
     *
     * @param \CrossingBorders\XBundle\Entity\Choice $choices
     * @return RecordTypeField
     */
    public function addChoice(\CrossingBorders\XBundle\Entity\Choice $choices)
    {
        $choices->setRecordTypeField($this);
        $this->choices[] = $choices;
        return $this;
    }

    /**
     * Remove choices
     *
     * @param \CrossingBorders\XBundle\Entity\Choice $choices
     */
    public function removeChoice(\CrossingBorders\XBundle\Entity\Choice $choices)
    {
        $this->choices->removeElement($choices);
    }

    /**
     * Get choices
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getChoices()
    {
        return $this->choices;
    }

    /**
     * Set displayOrder
     *
     * @param integer $displayOrder
     * @return RecordTypeField
     */
    public function setDisplayOrder($displayOrder)
    {
        $this->displayOrder = $displayOrder;

        return $this;
    }

    /**
     * Get displayOrder
     *
     * @return integer 
     */
    public function getDisplayOrder()
    {
        return $this->displayOrder;
    }

    /**
     * Set referencedRecordType
     *
     * @param \CrossingBorders\XBundle\Entity\RecordType $referencedRecordType
     * @return RecordTypeField
     */
    public function setReferencedRecordType(\CrossingBorders\XBundle\Entity\RecordType $referencedRecordType = null)
    {
        $this->referencedRecordType = $referencedRecordType;

        return $this;
    }

    /**
     * Get referencedRecordType
     *
     * @return \CrossingBorders\XBundle\Entity\RecordType 
     */
    public function getReferencedRecordType()
    {
        return $this->referencedRecordType;
    }

    /**
     * Set multipleSelection
     *
     * @param boolean $multipleSelection
     * @return RecordTypeField
     */
    public function setMultipleSelection($multipleSelection)
    {
        $this->multipleSelection = $multipleSelection;

        return $this;
    }

    /**
     * Get multipleSelection
     *
     * @return boolean 
     */
    public function getMultipleSelection()
    {
        return $this->multipleSelection;
    }

    /**
     * Add tableColumns
     *
     * @param \CrossingBorders\XBundle\Entity\TableColumn $tableColumns
     * @return RecordTypeField
     */
    public function addTableColumn(\CrossingBorders\XBundle\Entity\TableColumn $tableColumns)
    {
        $tableColumns->setRecordTypeField($this);
        $this->tableColumns[] = $tableColumns;
        return $this;
    }

    /**
     * Remove tableColumns
     *
     * @param \CrossingBorders\XBundle\Entity\TableColumn $tableColumns
     */
    public function removeTableColumn(\CrossingBorders\XBundle\Entity\TableColumn $tableColumns)
    {
        $this->tableColumns->removeElement($tableColumns);
    }

    /**
     * Get tableColumns
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTableColumns()
    {
        return $this->tableColumns;
    }

    /**
     * Add fieldValues
     *
     * @param \CrossingBorders\XBundle\Entity\FieldValue $fieldValues
     * @return RecordTypeField
     */
    public function addFieldValue(\CrossingBorders\XBundle\Entity\FieldValue $fieldValues)
    {
        $this->fieldValues[] = $fieldValues;

        return $this;
    }

    /**
     * Remove fieldValues
     *
     * @param \CrossingBorders\XBundle\Entity\FieldValue $fieldValues
     */
    public function removeFieldValue(\CrossingBorders\XBundle\Entity\FieldValue $fieldValues)
    {
        $this->fieldValues->removeElement($fieldValues);
    }

    /**
     * Get fieldValues
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFieldValues()
    {
        return $this->fieldValues;
    }

    /**
     * Set required
     *
     * @param boolean $required
     * @return RecordTypeField
     */
    public function setRequired($required)
    {
        $this->required = $required;

        return $this;
    }

    /**
     * Get required
     *
     * @return boolean 
     */
    public function getRequired()
    {
        return $this->required;
    }
}
