<?php
namespace CrossingBorders\XBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Record extends Base {
    public static $recordReferencePrefix = '___REFERENCING_RECORDS___';
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $name;
    /**
     * @ORM\OneToMany(targetEntity="CrossingBorders\XBundle\Entity\RecordTag", mappedBy="record", cascade={"persist"}, orphanRemoval=true)
     * @ORM\OrderBy({"displayOrder" = "ASC"})
     */
    protected $recordTags;
    /**
     * @ORM\ManyToOne(targetEntity="CrossingBorders\XBundle\Entity\RecordType", inversedBy="records")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $recordType;
    /**
     * @ORM\OneToMany(targetEntity="CrossingBorders\XBundle\Entity\FieldValue", mappedBy="record", cascade={"persist","remove"})
     */
    protected $fieldValues;
    /**
     * @ORM\ManyToMany(targetEntity="CrossingBorders\XBundle\Entity\FieldValueRecords", mappedBy="value", cascade={"persist","remove"})
     */
    protected $fieldValueRecords;
    /**
     * @ORM\OneToMany(targetEntity="CrossingBorders\XBundle\Entity\TableCell", mappedBy="record", cascade={"persist","remove"})
     */
    protected $tableCells;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->recordTags = new \Doctrine\Common\Collections\ArrayCollection();
        $this->fieldValues = new \Doctrine\Common\Collections\ArrayCollection();
        $this->fieldValueRecords = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tableCells = new \Doctrine\Common\Collections\ArrayCollection();
    }
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Record
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set recordType
     *
     * @param \CrossingBorders\XBundle\Entity\RecordType $recordType
     * @return Record
     */
    public function setRecordType(\CrossingBorders\XBundle\Entity\RecordType $recordType = null)
    {
        $this->recordType = $recordType;

        return $this;
    }

    /**
     * Get recordType
     *
     * @return \CrossingBorders\XBundle\Entity\RecordType 
     */
    public function getRecordType()
    {
        return $this->recordType;
    }

    /**
     * Add fieldValues
     *
     * @param \CrossingBorders\XBundle\Entity\FieldValue $fieldValues
     * @return Record
     */
    public function addFieldValue(\CrossingBorders\XBundle\Entity\FieldValue $fieldValues)
    {
        $this->fieldValues[] = $fieldValues;

        return $this;
    }

    /**
     * Remove fieldValues
     *
     * @param \CrossingBorders\XBundle\Entity\FieldValue $fieldValues
     */
    public function removeFieldValue(\CrossingBorders\XBundle\Entity\FieldValue $fieldValues)
    {
        $this->fieldValues->removeElement($fieldValues);
    }

    /**
     * Get fieldValues
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFieldValues()
    {
        return $this->fieldValues;
    }
    
    public static function checkIfNameIsRecordReference($name) {
        $prefixLength = strlen(Record::$recordReferencePrefix);
        $prefixCheck = substr($name, 0, $prefixLength);
        return ($prefixCheck === self::$recordReferencePrefix);
    }
    public static function getRecordReferenceTokens($name) {
         $nameLength = strlen($name);
         $suffix = substr($name, strlen(self::$recordReferencePrefix)-$nameLength);
         $tokens = explode('___', $suffix);
         if(count($tokens) == 2) {
             return array(
                            'recordTypeId' => intval($tokens[0]),
                            'recordTypeFieldName' => str_replace('_', ' ',$tokens[1])
                         );
         }
         return null;
    }
    private function clearReferencingRecords($record, $name) {
        foreach ($this->fieldValueRecords as $fieldValueRecord) {
            if($fieldValueRecord->getRecordTypeField()->getReferencedRecordType() === $this->getRecordType()
            && $fieldValueRecord->getRecordTypeField()->getName() == $name
            && $fieldValueRecord->getRecordTypeField()->getRecordType() === $record->getRecordType()) {
                $this->removeFieldValueRecord($fieldValueRecord);
            }
        }
    }
    public function __set($name, $value) {
        if(self::checkIfNameIsRecordReference($name)) {
            $tokens = self::getRecordReferenceTokens($name);
            if($tokens !== null) {
                $referencingRecords = $this->$name;
                foreach ($referencingRecords as $referencingRecord) {
                    $this->clearReferencingRecords($referencingRecord, $tokens['recordTypeFieldName']);
                }
                foreach ($value as $record) {
                    $record->$tokens['recordTypeFieldName'] = $this;
                }
            }
        } else {
            $name = str_replace('_', ' ', $name);     
            $isNew = true;
            foreach ($this->getFieldValues() as $fieldValue) {
                if($fieldValue->getRecordTypeField()->getName() === $name) {
                    switch ($fieldValue->getRecordTypeField()->getFieldType()->getName()) {
                        case "Record":
                        case "Choice":
                            if($value !== null) {
                                if($value instanceof ArrayCollection) {
                                    foreach ($value as $obj) {
                                        $fieldValue->addValue($obj);
                                    }
                                } else {
                                    $fieldValue->addValue($value);
                                }
                            } else {
                                $fieldValue->getValue()->clear();
                            }
                            break;
                        case 'Table':
                            $this->setTableValue($fieldValue->getRecordTypeField(), $value, $fieldValue);
                            break;
                        case "File":
                        case "Image":
                        case "Audio":
                        case "Video":
                                if(is_string($value) && $value == 'delete') {
                                    $fieldValue->removeFile();
                                } else {
                                    $fieldValue->setFile($value);
                                }
                                break;
                        default:
                            $fieldValue->setValue($value);
                            break;
                    }
                    $isNew = false;
                }
            }
            if($isNew) {
                foreach($this->getRecordType()->getRecordTypeFields() as $recordTypeField) {
                    if($recordTypeField->getName() === $name) {
                        /* TODO: Complete type switch
                         * - Missing:
                         *  * Record
                         *  * Multiple Record
                         *  * Choice
                         */
                        switch ($recordTypeField->getFieldType()->getName()) {
                            case "String":
                                $fieldValue = new FieldValueString();
                                break;
                            case "Text":
                                $fieldValue = new FieldValueText();
                                break;
                            case "Integer":
                                $fieldValue = new FieldValueInteger();
                                break;
                             case "Float":
                                $fieldValue = new FieldValueFloat();
                                break;
                            case "Boolean":
                                $fieldValue = new FieldValueBoolean();
                                break;
                            case "Date":
                                $fieldValue = new FieldValueDate();
                                break;
                            case "Date Time":
                                $fieldValue = new FieldValueDateTime();
                                break;
                            case "Time":
                                $fieldValue = new FieldValueTime();
                                break;
                            case "Record":
                                $fieldValue = new FieldValueRecords();
                                break;
                            case "Choice":
                                $fieldValue = new FieldValueChoice();
                                break;
                            case "File":
                            case "Image":
                            case "Audio":
                            case "Video":
                                $fieldValue = new FieldValueFile();
                                break;
                            case "Table":
                                $fieldValue = new FieldValueTable();
                                break;
                            default:
                                $fieldValue = new FieldValueText();
                                break;
                        }
                        $fieldValue->setRecordTypeField($recordTypeField);
                        $fieldValue->setRecord($this);
                        switch ($recordTypeField->getFieldType()->getName()) {
                            case "File":
                            case "Image":
                            case "Audio":
                            case "Video":
                                if(is_string($value) && $value == 'delete') {
                                    $fieldValue->removeFile();
                                } else {
                                    $fieldValue->setFile($value);
                                }
                                break;
                            case "Record":
                            case "Choice":
                                if($value !== null) {
                                    if($value instanceof ArrayCollection) {
                                        foreach ($value as $obj) {
                                            $fieldValue->addValue($obj);
                                        }
                                    } else {
                                        $fieldValue->addValue($value);
                                    }
                                } else {
                                    $fieldValue->getValue()->clear();
                                }
                                break;
                            case "Table":
                                $this->setTableValue($recordTypeField, $value, $fieldValue);
                                break;
                            default:
                                $fieldValue->setValue($value);
                                break;
                        }
                        $this->addFieldValue($fieldValue);
                    }
                }
            }
        }
    }
    private function setTableValue($recordTypeField, $rawValue, $fieldValue) {
        /**
         * Just Delete Everything and Rebuild
         * TODO: This can be optimised if REALLY needed
         */
        $fieldValue->getRows()->clear();
        foreach ($rawValue as $row) {
            $tableRow = new TableRow();
            $colIt = $recordTypeField->getTableColumns()->getIterator();
            foreach ($row as $cell) {
                $tableCell = $cell;
                if($colIt->valid()) {
                    $tableCell->setColumn($colIt->current());
                    $colIt->next();
                }
                $tableCell->setRow($tableRow);
            }
            $fieldValue->addRow($tableRow);
        }
    }
    
    public function __get($name) {
        if(self::checkIfNameIsRecordReference($name)) {
            $tokens = self::getRecordReferenceTokens($name);
            if($tokens !== null) {
                $referencingRecords = new ArrayCollection();
                foreach ($this->fieldValueRecords as $fieldValueRecord) {
                    if($fieldValueRecord->getRecordTypeField()->getReferencedRecordType() === $this->getRecordType()
                    && $fieldValueRecord->getRecordTypeField()->getName() === $tokens['recordTypeFieldName']
                    && $fieldValueRecord->getRecordTypeField()->getRecordType()->getId() === $tokens['recordTypeId']) {
                        $referencingRecords->add($fieldValueRecord->getRecord());
                    }
                }
                return $referencingRecords;
            }
            return null;
        }
        $name = str_replace('_', ' ', $name);
        foreach ($this->getFieldValues() as $fieldValue) {
            if($fieldValue->getRecordTypeField()->getName() === $name) {
                switch ($fieldValue->getRecordTypeField()->getFieldType()->getName()) {
                    case "Record":
                    case "Choice":
                        if($fieldValue->getRecordTypeField()->getMultipleSelection()) {
                            return $fieldValue->getValue();
                        } else {
                            return $fieldValue->getSingleValue();
                        }
                    case "Table":
                        return $fieldValue->getRows();
                    default:
                        return $fieldValue->getValue();
                }
            }
        }
        return null;
    }
    public function getFieldValueFile($name) {
        foreach ($this->getFieldValues() as $fieldValue) {
            if($fieldValue->getRecordTypeField()->getName() === $name
            &&($fieldValue->getRecordTypeField()->getFieldType()->getName() === "File"
              ||$fieldValue->getRecordTypeField()->getFieldType()->getName() === "Image"
              ||$fieldValue->getRecordTypeField()->getFieldType()->getName() === "Audio"
              ||$fieldValue->getRecordTypeField()->getFieldType()->getName() === "Video")) {
                return $fieldValue;
            }
        }
        return null;
    }
    public function getValueAndType($name) {
        $result = array(
            'value' => null,
            'type'  => null
        );
        $result['value'] = $this->__get($name);
        foreach ($this->getFieldValues() as $fieldValue) {
            if($fieldValue->getRecordTypeField()->getName() === $name) {
                $result['type'] = $fieldValue->getRecordTypeField()->getFieldType()->getName();
                return $result;
            }
        }
        return $result;
    }
    //Make this block of methods a bit cleaner
    public function isFieldValueFile($name) {
        foreach ($this->getFieldValues() as $fieldValue) {
            if($fieldValue->getRecordTypeField()->getName() === $name
            &&($fieldValue->getRecordTypeField()->getFieldType()->getName() === "File"
              ||$fieldValue->getRecordTypeField()->getFieldType()->getName() === "Image"
              ||$fieldValue->getRecordTypeField()->getFieldType()->getName() === "Audio"
              ||$fieldValue->getRecordTypeField()->getFieldType()->getName() === "Video")) {
                return true;
            }
        }
        return false;
    }
    public function isFieldValueImage($name) {
        foreach ($this->getFieldValues() as $fieldValue) {
            if($fieldValue->getRecordTypeField()->getName() === $name
            &&($fieldValue->getRecordTypeField()->getFieldType()->getName() === "File"
             ||$fieldValue->getRecordTypeField()->getFieldType()->getName() === "Image"
             ||$fieldValue->getRecordTypeField()->getFieldType()->getName() === "Audio"
             ||$fieldValue->getRecordTypeField()->getFieldType()->getName() === "Video")) {
                return $fieldValue->getIsImage();
            }
        }
        return false;
    }
    public function isFieldValueAudio($name) {
        foreach ($this->getFieldValues() as $fieldValue) {
            if($fieldValue->getRecordTypeField()->getName() === $name
            &&($fieldValue->getRecordTypeField()->getFieldType()->getName() === "File"
             ||$fieldValue->getRecordTypeField()->getFieldType()->getName() === "Image"
             ||$fieldValue->getRecordTypeField()->getFieldType()->getName() === "Audio"
             ||$fieldValue->getRecordTypeField()->getFieldType()->getName() === "Video")) {
                return $fieldValue->getIsAudio();
            }
        }
        return false;
    }
    public function isFieldValueVideo($name) {
        foreach ($this->getFieldValues() as $fieldValue) {
            if($fieldValue->getRecordTypeField()->getName() === $name
            &&($fieldValue->getRecordTypeField()->getFieldType()->getName() === "File"
             ||$fieldValue->getRecordTypeField()->getFieldType()->getName() === "Image"
             ||$fieldValue->getRecordTypeField()->getFieldType()->getName() === "Audio"
             ||$fieldValue->getRecordTypeField()->getFieldType()->getName() === "Video")) {
                return $fieldValue->getIsVideo();
            }
        }
        return false;
    }

    /**
     * Remove tags
     *
     * @param \CrossingBorders\XBundle\Entity\Tag $tags
     */
    public function removeTag(\CrossingBorders\XBundle\Entity\Tag $tags)
    {
        $this->tags->removeElement($tags);
    }
    /**
     * Add recordTags
     *
     * @param \CrossingBorders\XBundle\Entity\RecordTag $recordTags
     * @return Record
     */
    public function addRecordTag(\CrossingBorders\XBundle\Entity\RecordTag $recordTags)
    {
        $recordTags->setRecord($this);
        $this->recordTags->add($recordTags);   
        return $this;
    }

    /**
     * Remove recordTags
     *
     * @param \CrossingBorders\XBundle\Entity\RecordTag $recordTags
     */
    public function removeRecordTag(\CrossingBorders\XBundle\Entity\RecordTag $recordTags)
    {
        $this->recordTags->removeElement($recordTags);
    }

    /**
     * Get recordTags
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRecordTags()
    {
        return $this->recordTags;
    }
    
    public function getCommaDelimitedTags()
    {
        $tagsArray = [];
        foreach ($this->getRecordTags() as $recordTag) {
            $tagsArray[] = $recordTag->getTag()->getName();
        }
        return implode(', ', $tagsArray);
    }
    /**
     * Add fieldValueRecords
     *
     * @param \CrossingBorders\XBundle\Entity\FieldValueRecords $fieldValueRecords
     * @return Record
     */
    public function addFieldValueRecord(\CrossingBorders\XBundle\Entity\FieldValueRecords $fieldValueRecords)
    {
        if (!$this->fieldValueRecords->contains($fieldValueRecords)) {
            $this->fieldValueRecords->add($fieldValueRecords);
        }
        return $this;
    }
    /**
     * Remove fieldValueRecords
     *
     * @param \CrossingBorders\XBundle\Entity\FieldValueRecords $fieldValueRecords
     */
    public function removeFieldValueRecord(\CrossingBorders\XBundle\Entity\FieldValueRecords $fieldValueRecords)
    {
        $fieldValueRecords->removeValue($this);
        $this->fieldValueRecords->removeElement($fieldValueRecords);
    }
    /**
     * Get fieldValueRecords
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFieldValueRecords() {
        return $this->fieldValueRecords;
    }
    
    /**
     * Add tableCells
     *
     * @param \CrossingBorders\XBundle\Entity\TableCell $tableCells
     * @return Record
     */
    public function addTableCell(\CrossingBorders\XBundle\Entity\TableCell $tableCells)
    {
        $this->tableCells[] = $tableCells;

        return $this;
    }

    /**
     * Remove tableCells
     *
     * @param \CrossingBorders\XBundle\Entity\TableCell $tableCells
     */
    public function removeTableCell(\CrossingBorders\XBundle\Entity\TableCell $tableCells)
    {
        $this->tableCells->removeElement($tableCells);
    }

    /**
     * Get tableCells
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTableCells()
    {
        return $this->tableCells;
    }
    
    /**
     * @Assert\Callback
     */
    public function validate(ExecutionContextInterface $context) {
        foreach ($this->getFieldValues() as $fieldValue) {
            $path = str_replace(' ', '_', $fieldValue->getRecordTypeField()->getName());
            $translationDomain = 'messages';
            switch ($fieldValue->getRecordTypeField()->getFieldType()->getName()) {
                case "Choice":
                case "Record":
                    if($fieldValue->getRecordTypeField()->getRequired()
                    && $fieldValue->getRecordTypeField()->getMultipleSelection()
                    && $fieldValue->getValue()->isEmpty()) {
                        $context->buildViolation('You must select at least one option')
                                ->setTranslationDomain($translationDomain)
                                ->atPath($path)->addViolation();
                    }
                    break;
                case "File":
                case "Image":
                case "Audio":
                case "Video":
                    if($fieldValue->getRecordTypeField()->getRequired()
                    && ($fieldValue->getIsStoredFileNull() || !$fieldValue->getExists())) {
                        $context->buildViolation('This file is required')
                                ->setTranslationDomain($translationDomain)
                                ->atPath($path)->addViolation();
                    }
                    break;
                case "Table":
                    if($fieldValue->getRecordTypeField()->getRequired()
                    && $fieldValue->getRows()->isEmpty()) {
                        $context->buildViolation('You must add at least one row')
                                ->setTranslationDomain($translationDomain)
                                ->atPath($path)
                                ->addViolation();
                    }
                    break;
                default: break;
            }
        }
    }
    public function __toString() {
        return $this->name;
    }
}