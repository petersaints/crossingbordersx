<?php
namespace CrossingBorders\XBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 */
class FieldValueRecords extends FieldValue {
    /**
     * @ORM\ManyToMany(targetEntity="CrossingBorders\XBundle\Entity\Record", inversedBy="fieldValueRecords")
     */
    protected $value;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->value = new \Doctrine\Common\Collections\ArrayCollection();
    }
    /**
     * Add value
     *
     * @param \CrossingBorders\XBundle\Entity\Record $value
     * @return FieldValueRecords
     */
    public function addValue(\CrossingBorders\XBundle\Entity\Record $value)
    {
        if(!$this->getRecordTypeField()->getMultipleSelection()) {
            $this->value->clear();
        }
        if (!$this->value->contains($value)) {
            $this->value->add($value);
        }
        return $this;
    }
    /**
     * Remove value
     *
     * @param \CrossingBorders\XBundle\Entity\Record $value
     */
    public function removeValue(\CrossingBorders\XBundle\Entity\Record $value)
    {
        $this->value->removeElement($value);
    }
    /**
     * Get value
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getValue()
    {
        return $this->value;
    }
    /**
     * Get value
     *
     * @return \CrossingBorders\XBundle\Entity\Record
     */
    public function getSingleValue()
    {
        if($this->value->count() > 0
        &&!$this->getRecordTypeField()->getMultipleSelection()) {
            return $this->value[0];
        } else {
            return null;
        }
    }
    public function getRecordType() {
        return $this->getRecordTypeField()->getRecordType()->getName();
    }
    public function getType() {
        return 'Records';
    }    
}