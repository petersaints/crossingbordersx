<?php

namespace CrossingBorders\XBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({
 *                          "Text" = "FieldValueText",
 *                          "String" = "FieldValueString",
 *                          "Integer" = "FieldValueInteger",
 *                          "Float" = "FieldValueFloat",
 *                          "Boolean" = "FieldValueBoolean",
 *                          "Date" = "FieldValueDate",
 *                          "Time" = "FieldValueTime",
 *                          "DateTime" = "FieldValueDateTime",
 *                          "Records" = "FieldValueRecords",
 *                          "Choice" = "FieldValueChoice",
 *                          "File" = "FieldValueFile",
 *                          "Table" = "FieldValueTable"
 *                      })
 */
abstract class FieldValue extends Base {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\ManyToOne(targetEntity="CrossingBorders\XBundle\Entity\RecordTypeField", inversedBy="fieldValues")
     * @ORM\JoinColumn(name="recordTypeField_id", referencedColumnName="id", nullable=false)
     */
    protected $recordTypeField;
    /**
     * @ORM\ManyToOne(targetEntity="CrossingBorders\XBundle\Entity\Record", inversedBy="fieldValues")
     * @ORM\JoinColumn(name="record_id", referencedColumnName="id", nullable=false)
     */
    protected $record;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set record
     *
     * @param \CrossingBorders\XBundle\Entity\Record $record
     * @return FieldValue
     */
    public function setRecord(\CrossingBorders\XBundle\Entity\Record $record = null)
    {
        $this->record = $record;

        return $this;
    }

    /**
     * Get record
     *
     * @return \CrossingBorders\XBundle\Entity\Record 
     */
    public function getRecord()
    {
        return $this->record;
    }
    
    /**
     * Get value
     *
     * @return value 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set recordTypeField
     *
     * @param \CrossingBorders\XBundle\Entity\RecordTypeField $recordTypeField
     * @return FieldValue
     */
    public function setRecordTypeField(\CrossingBorders\XBundle\Entity\RecordTypeField $recordTypeField = null)
    {
        $this->recordTypeField = $recordTypeField;

        return $this;
    }

    /**
     * Get recordTypeField
     *
     * @return \CrossingBorders\XBundle\Entity\RecordTypeField 
     */
    public function getRecordTypeField()
    {
        return $this->recordTypeField;
    }
    
    abstract public function getType();
}
