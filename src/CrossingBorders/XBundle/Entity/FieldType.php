<?php
namespace CrossingBorders\XBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class FieldType extends Base {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\Column(type="string", unique=true, nullable=false)
     */
    protected $name;
    /**
     * @ORM\OneToMany(targetEntity="CrossingBorders\XBundle\Entity\RecordTypeField", mappedBy="fieldType")
     */
    protected $recordTypeFields;
   
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->recordTypeFields = new \Doctrine\Common\Collections\ArrayCollection();
    }
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return FieldType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add recordTypeFields
     *
     * @param \CrossingBorders\XBundle\Entity\RecordTypeField $recordTypeFields
     * @return FieldType
     */
    public function addRecordTypeField(\CrossingBorders\XBundle\Entity\RecordTypeField $recordTypeFields)
    {
        $this->recordTypeFields[] = $recordTypeFields;
        return $this;
    }

    /**
     * Remove recordTypeFields
     *
     * @param \CrossingBorders\XBundle\Entity\RecordTypeField $recordTypeFields
     */
    public function removeRecordTypeField(\CrossingBorders\XBundle\Entity\RecordTypeField $recordTypeFields)
    {
        $this->recordTypeFields->removeElement($recordTypeFields);
    }

    /**
     * Get recordTypeFields
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRecordTypeFields()
    {
        return $this->recordTypeFields;
    }
}
