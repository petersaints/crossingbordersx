<?php
namespace CrossingBorders\XBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 */
class FieldValueInteger extends FieldValue {
    /**
     * @ORM\Column(type="integer")
     */
    protected $value;
    
    /**
     * Set value
     *
     * @param integer $value
     * @return FieldValueInteger
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return integer 
     */
    public function getValue()
    {
        return $this->value;
    }
    public function getType() {
        return 'Integer';
    }
}
