<?php
namespace CrossingBorders\XBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * Tag
 *
 * @ORM\Entity
 * @ORM\Table(name="tag",uniqueConstraints={@ORM\UniqueConstraint(name="tag_name_unique_idx", columns={"name"})})
 */
class Tag
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(name="name", type="string", length=255)
     */
    protected $name;
    /**
     * @ORM\OneToMany(targetEntity="CrossingBorders\XBundle\Entity\RecordTag", mappedBy="tag", orphanRemoval=true)
     * @ORM\OrderBy({"displayOrder" = "ASC"})
     */
    protected $recordTags;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->recordTags = new \Doctrine\Common\Collections\ArrayCollection();
    }
    public function __toString() {
        return $this->name;
    }
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Set name
     *
     * @param string $name
     * @return Tag
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Add recordTags
     *
     * @param \CrossingBorders\XBundle\Entity\RecordTag $recordTags
     * @return Tag
     */
    public function addRecordTag(\CrossingBorders\XBundle\Entity\RecordTag $recordTags)
    {
        $recordTags->addTag($this);
        $this->recordTags->add($recordTags);
        return $this;
    }

    /**
     * Remove recordTags
     *
     * @param \CrossingBorders\XBundle\Entity\RecordTag $recordTags
     */
    public function removeRecordTag(\CrossingBorders\XBundle\Entity\RecordTag $recordTags)
    {
        $this->recordTags->removeElement($recordTags);
    }

    /**
     * Get recordTags
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRecordTags()
    {
        return $this->recordTags;
    }
}
