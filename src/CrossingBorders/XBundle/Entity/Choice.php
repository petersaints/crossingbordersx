<?php
namespace CrossingBorders\XBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Choice {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $name;
    /**
     * @ORM\ManyToOne(targetEntity="CrossingBorders\XBundle\Entity\RecordTypeField", inversedBy="choices", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    protected $recordTypeField;
    /**
     * @ORM\Column(type="integer")
     */
    protected $displayOrder;
    
    /**
     * Constructor
     */
    public function __construct() {
        $this->displayOrder = 0;
    }
    
    public function __toString() {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Choice
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set displayOrder
     *
     * @param integer $displayOrder
     * @return Choice
     */
    public function setDisplayOrder($displayOrder)
    {
        $this->displayOrder = $displayOrder;

        return $this;
    }

    /**
     * Get displayOrder
     *
     * @return integer 
     */
    public function getDisplayOrder()
    {
        return $this->displayOrder;
    }

    /**
     * Set recordTypeField
     *
     * @param \CrossingBorders\XBundle\Entity\RecordTypeField $recordTypeField
     * @return Choice
     */
    public function setRecordTypeField(\CrossingBorders\XBundle\Entity\RecordTypeField $recordTypeField = null)
    {
        $this->recordTypeField = $recordTypeField;
        return $this;
    }

    /**
     * Get recordTypeField
     *
     * @return \CrossingBorders\XBundle\Entity\RecordTypeField 
     */
    public function getRecordTypeField()
    {
        return $this->recordTypeField;
    }
}