<?php
namespace CrossingBorders\XBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * Favorite
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Favorite
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\ManyToOne(targetEntity="CrossingBorders\SecurityBundle\Entity\User")
     */
    protected $user;
    /**
     * @ORM\ManyToOne(targetEntity="CrossingBorders\XBundle\Entity\FavoriteType", inversedBy="favorites")
     */
    protected $type;
    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $name;
    /**
     * @ORM\Column(type="text", nullable=false)
     */
    protected $url;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }
    /**
     * Set name
     *
     * @param string $name
     * @return Favorite
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }
    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Set url
     *
     * @param string $url
     * @return Favorite
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }
    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }
    /**
     * Set user
     *
     * @param \CrossingBorders\XBundle\Entity\FavoriteUser $user
     * @return Favorite
     */
    public function setUser(\CrossingBorders\SecurityBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }
    /**
     * Get user
     *
     * @return \CrossingBorders\SecurityBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
    /**
     * Set type
     *
     * @param \CrossingBorders\XBundle\Entity\FavoriteType $type
     * @return Favorite
     */
    public function setType(\CrossingBorders\XBundle\Entity\FavoriteType $type)
    {
        $this->type = $type;

        return $this;
    }
    /**
     * Get type
     *
     * @return \CrossingBorders\XBundle\Entity\FavoriteType 
     */
    public function getType()
    {
        return $this->type;
    }
}
