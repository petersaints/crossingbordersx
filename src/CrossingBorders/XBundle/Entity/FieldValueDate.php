<?php
namespace CrossingBorders\XBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 */
class FieldValueDate extends FieldValue {
    /**
     * @ORM\Column(type="date", nullable=true)
     */
    protected $value;
    /**
     * Set value
     *
     * @param \DateTime $value
     * @return FieldValueDate
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return \DateTime 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return FieldValueDate
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    public function getType() {
        return 'Date';
    }
}
