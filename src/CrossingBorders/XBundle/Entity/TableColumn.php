<?php

namespace CrossingBorders\XBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TableColumn
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class TableColumn
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    protected $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="displayOrder", type="integer")
     */
    protected $displayOrder;

    /**
     * @ORM\ManyToOne(targetEntity="CrossingBorders\XBundle\Entity\RecordTypeField", inversedBy="tableColumns", cascade={"persist"})
     * @ORM\JoinColumn(name="recordTypeField_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $recordTypeField;
    /**
     * @ORM\ManyToOne(targetEntity="CrossingBorders\XBundle\Entity\RecordType", inversedBy="tableColumns", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    protected $recordType;
    /**
     * @ORM\OneToMany(targetEntity="CrossingBorders\XBundle\Entity\TableCell", mappedBy="column", cascade={"persist"}, orphanRemoval=true)
     */
    protected $cells;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->cells = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return TableColumn
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set displayOrder
     *
     * @param integer $displayOrder
     * @return TableColumn
     */
    public function setDisplayOrder($displayOrder)
    {
        $this->displayOrder = $displayOrder;

        return $this;
    }

    /**
     * Get displayOrder
     *
     * @return integer 
     */
    public function getDisplayOrder()
    {
        return $this->displayOrder;
    }

    /**
     * Set recordTypeField
     *
     * @param \CrossingBorders\XBundle\Entity\RecordTypeField $recordTypeField
     * @return TableColumn
     */
    public function setRecordTypeField(\CrossingBorders\XBundle\Entity\RecordTypeField $recordTypeField = null)
    {
        $this->recordTypeField = $recordTypeField;

        return $this;
    }

    /**
     * Get recordTypeField
     *
     * @return \CrossingBorders\XBundle\Entity\RecordTypeField 
     */
    public function getRecordTypeField()
    {
        return $this->recordTypeField;
    }

    /**
     * Add cells
     *
     * @param \CrossingBorders\XBundle\Entity\TableCell $cells
     * @return TableColumn
     */
    public function addCell(\CrossingBorders\XBundle\Entity\TableCell $cells)
    {
        if (!$this->cells->contains($cells)) {
             $this->cells->add($cells);
        }
        return $this;
    }

    /**
     * Remove cells
     *
     * @param \CrossingBorders\XBundle\Entity\TableCell $cells
     */
    public function removeCell(\CrossingBorders\XBundle\Entity\TableCell $cells)
    {
        $this->cells->removeElement($cells);
    }

    /**
     * Get cells
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCells()
    {
        return $this->cells;
    }
    
    /**
     * Set recordType
     *
     * @param \CrossingBorders\XBundle\Entity\RecordType $recordType
     * @return Choice
     */
    public function setRecordType(\CrossingBorders\XBundle\Entity\RecordType $recordType = null)
    {
        $this->recordType = $recordType;

        return $this;
    }

    /**
     * Get recordType
     *
     * @return \CrossingBorders\XBundle\Entity\RecordType 
     */
    public function getRecordType()
    {
        return $this->recordType;
    }
    
    public function __toString() {
        return $this->name;
    }
}