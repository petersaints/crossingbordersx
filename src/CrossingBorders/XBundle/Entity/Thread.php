<?php
namespace CrossingBorders\XBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\CommentBundle\Entity\Thread as BaseThread;

/**
 * @ORM\Entity
 * @ORM\ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 */
class Thread extends BaseThread
{
    /**
     * @var string $id
     *
     * @ORM\Id
     * @ORM\Column(type="string")
     */
    protected $id;
    /**
     * @ORM\OneToMany(targetEntity="CrossingBorders\XBundle\Entity\Comment", mappedBy="thread", orphanRemoval=true)
     */
    protected $comments;
}
