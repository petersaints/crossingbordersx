<?php

namespace CrossingBorders\XBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FavoriteType
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class FavoriteType
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\OneToMany(targetEntity="CrossingBorders\XBundle\Entity\Favorite", mappedBy="type")
     */
    protected $favorites;
    /**
     * @ORM\Column(type="string", unique=true, nullable=false)
     */
    protected $name;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->favorites = new \Doctrine\Common\Collections\ArrayCollection();
    }
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Set name
     *
     * @param string $name
     * @return FavoriteType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }
    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Add favorites
     *
     * @param \CrossingBorders\XBundle\Entity\Favorite $favorites
     * @return FavoriteType
     */
    public function addFavorite(\CrossingBorders\XBundle\Entity\Favorite $favorites)
    {
        $this->favorites[] = $favorites;

        return $this;
    }
    /**
     * Remove favorites
     *
     * @param \CrossingBorders\XBundle\Entity\Favorite $favorites
     */
    public function removeFavorite(\CrossingBorders\XBundle\Entity\Favorite $favorites)
    {
        $this->favorites->removeElement($favorites);
    }
    /**
     * Get favorites
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFavorites()
    {
        return $this->favorites;
    }
}