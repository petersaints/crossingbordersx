<?php
namespace CrossingBorders\XBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 */
class FieldValueString extends FieldValue {
    /**
     * @ORM\Column(type="string")
     */
    protected $value;
  
    /**
     * Set value
     *
     * @param string $value
     * @return FieldValueString
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }
    public function getType() {
        return 'String';
    }
}
