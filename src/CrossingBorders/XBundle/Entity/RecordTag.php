<?php

namespace CrossingBorders\XBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RecordTag
 *
 * @ORM\Entity
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(name="recordtag_unique_record_tag_idx",
 *                               columns={"record_id", "tag_id"}) } )
 */
class RecordTag
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @var integer
     *
     * @ORM\Column(name="displayOrder", type="integer")
     */
    protected $displayOrder;
    /**
     * @ORM\ManyToOne(targetEntity="CrossingBorders\XBundle\Entity\Tag", inversedBy="recordTags")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $tag;
    /**
     * @ORM\ManyToOne(targetEntity="CrossingBorders\XBundle\Entity\Record", inversedBy="recordTags")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $record;
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set displayOrder
     *
     * @param integer $displayOrder
     * @return RecordTag
     */
    public function setDisplayOrder($displayOrder)
    {
        $this->displayOrder = $displayOrder;

        return $this;
    }

    /**
     * Get displayOrder
     *
     * @return integer 
     */
    public function getDisplayOrder()
    {
        return $this->displayOrder;
    }

    /**
     * Set tag
     *
     * @param \CrossingBorders\XBundle\Entity\Tag $tag
     * @return RecordTag
     */
    public function setTag(\CrossingBorders\XBundle\Entity\Tag $tag = null)
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * Get tag
     *
     * @return \CrossingBorders\XBundle\Entity\Tag 
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * Set record
     *
     * @param \CrossingBorders\XBundle\Entity\Record $record
     * @return RecordTag
     */
    public function setRecord(\CrossingBorders\XBundle\Entity\Record $record = null)
    {
        $this->record = $record;
        return $this;
    }
    /**
     * Get record
     *
     * @return \CrossingBorders\XBundle\Entity\Record 
     */
    public function getRecord()
    {
        return $this->record;
    }
}
