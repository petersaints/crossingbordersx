<?php
namespace CrossingBorders\XBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;

class TableRowType extends AbstractType {
    protected $columns;
    public function __construct($columns) {
        $this->columns = $columns;
    }
    public function buildForm(FormBuilderInterface $builder, array $options) {
        foreach ($this->columns as $index => $column) {
            $recordType = $column->getRecordType();
            if(is_null($recordType)) {
                $builder->add("$index", 'text', array('required' => false));
            } else {
                $builder->add("$index", 'entity', array('required'  => false,
                                                        'placeholder' => '',
                                                        'empty_data'  => null,
                                                        'class' => 'CrossingBordersXBundle:Record',
                                                        'property' => 'name',
                                                        'query_builder' => function(EntityRepository $er) use ($recordType) {
                                                                                return $er->createQueryBuilder('r')
                                                                                          ->where('r.recordType = :record_type')
                                                                                          ->orderBy('r.name', 'ASC')
                                                                                          ->setParameter('record_type', $recordType);
                                                                            }));
            }
        }
        $builder->addViewTransformer(new \CrossingBorders\XBundle\Form\DataTransformer\TableRowTransformer());
    }
    public function getParent() {
        return 'form';
    }
    public function getName() {
        return 'table_row';
    }
}