<?php
namespace CrossingBorders\XBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;

class IntervalSearchType extends AbstractType {
    protected $type;
    protected $options;
    public function __construct($type, $options = []) {
        $this->type = $type;
        $this->options = $options;
    }
    public function buildForm(FormBuilderInterface $builder, array $options) {
        parent::buildForm($builder, $options);
        $this->options['required'] = false;  
        $this->options['label'] = 'Equals';
        $builder->add("equals",$this->type,$this->options);
        $this->options['label'] = 'Minimum';
        $builder->add("minimum",$this->type,$this->options);
        $this->options['label'] = 'Maximum';
        $builder->add("maximum",$this->type,$this->options);
    }
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(
                                array(
                                        'subfield' => true,
                                     )
                              );
    }
    public function buildView(FormView $view, FormInterface $form, array $options) {
        parent::buildView($view, $form, $options);
        $view->vars = array_replace($view->vars, array(
                                                        'subfield' => $options['subfield'],
                                                      )
        );
    }
    public function getParent() {
        return 'form';
    }
    public function getName() {
        return 'interval_search';
    }
}