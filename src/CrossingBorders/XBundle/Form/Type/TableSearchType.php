<?php
namespace CrossingBorders\XBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Doctrine\ORM\EntityRepository;

class TableSearchType extends AbstractType {
    protected $columns;
    protected $options;
    public function __construct($columns, $options = []) {
        $this->columns = $columns;
        $this->options = $options;
    }
    public function buildForm(FormBuilderInterface $builder, array $options) {
        foreach ($this->columns as $column) {
                $columnName = str_replace(' ', '_',  $column->getName());
                $recordType = $column->getRecordType();
                if(is_null($recordType)) {
                    $builder->add($columnName,
                                  'text',
                                  array('required' => false, 'label' => $column->getName()));
                } else {
                    $builder->add($columnName,
                                  'entity',
                                  array('required'  => false,
                                                       'placeholder' => '',
                                                       'empty_data'  => null,
                                                       'class' => 'CrossingBordersXBundle:Record',
                                                       'property' => 'name',
                                                       'query_builder' => function(EntityRepository $er) use ($recordType) {
                                                                                return $er->createQueryBuilder('r')
                                                                                          ->where('r.recordType = :record_type')
                                                                                          ->orderBy('r.name', 'ASC')
                                                                                          ->setParameter('record_type', $recordType);
                                                        }));
                }
        }
    }
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(
                                array(
                                        'subfield' => true,
                                     )
                              );
    }
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        parent::buildView($view, $form, $options);
        $view->vars = array_replace($view->vars, array(
                                                        'subfield' => $options['subfield'],
                                                      )
        );
    }
    public function getParent() {
        return 'form';
    }
    public function getName() {
        return 'interval';
    }
}
