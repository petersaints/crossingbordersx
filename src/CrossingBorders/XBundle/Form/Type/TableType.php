<?php
namespace CrossingBorders\XBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;

class TableType extends AbstractType {
    protected $columns;
    public function __construct($columns) {
        $this->columns = $columns;
    }
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(
                                array(
                                        'columns' => $this->columns,
                                        'type' => new TableRowType($this->columns),
                                        'allow_add'    => true,
                                        'allow_delete' => true,
                                        'by_reference' => false,
                                        'prototype_name' => '__table__name__',
                                        'error_bubbling' => false
                                     )
                              );
    }
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        parent::buildView($view, $form, $options);
        $view->vars = array_replace($view->vars, array(
                                                        'type' => $options['type'],
                                                        'columns'  => $this->columns
                                                      )
                                   );
    }
    public function getParent() {
        return 'collection';
    }
    public function getName() {
        return 'table';
    }
}