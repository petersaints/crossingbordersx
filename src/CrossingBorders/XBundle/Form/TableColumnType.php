<?php
namespace CrossingBorders\XBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TableColumnType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('name')
                ->add('recordType')
                ->add('displayOrder', 'hidden');
    }
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'CrossingBorders\XBundle\Entity\TableColumn'
        ));
    }
    /**
     * @return string
     */
    public function getName() {
        return 'crossingborders_xbundle_tablecolumn';
    }
}
