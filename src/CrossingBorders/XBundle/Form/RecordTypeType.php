<?php
namespace CrossingBorders\XBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RecordTypeType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('name')
                ->add('recordTypeFields',
                      'collection',
                                    array(
                                            'type' => new RecordTypeFieldType(),
                                            'allow_add'    => true,
                                            'allow_delete' => true,
                                            'by_reference' => false
                                        )
                        );
    }
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'CrossingBorders\XBundle\Entity\RecordType',
            'cascade_validation' => true
        ));
    }
    /**
     * @return string
     */
    public function getName() {
        return 'crossingborders_xbundle_recordtype';
    }
}