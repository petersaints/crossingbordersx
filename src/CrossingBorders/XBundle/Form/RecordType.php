<?php
namespace CrossingBorders\XBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank as NotBlankValidator;
use Symfony\Component\Validator\Constraints\File as FileValidator;
//use Symfony\Component\Validator\Constraints\Image as ImageValidator;
use Doctrine\ORM\EntityRepository;

use CrossingBorders\XBundle\Entity\Record;
use CrossingBorders\XBundle\Entity\StoredFile;

class RecordType extends AbstractType
{
    const FLOAT_PRECISION = 4;

    protected $record;
    protected $em = null;
    public function __construct(\CrossingBorders\XBundle\Entity\Record $record, $em = null) {
        $this->record = $record;
        if($em !== null) {
            $this->em = $em;
        }
    }
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name')
                ->add('tags', 'text', array(
                                            'mapped' => false,
                                            'data' => $this->record->getCommaDelimitedTags(),
                                            'required' => false
                                           )
                     );
        $this->initFields($builder);        
    }
    private function initFields(FormBuilderInterface $builder) {
        if($this->record->getRecordType()) {
            //Fill the Fields
            foreach ($this->record->getRecordType()->getRecordTypeFields() as $recordTypeField) {
                $recordTypeFieldName = str_replace(' ', '_',  $recordTypeField->getName());
                //Adding Validation Constraints
                $constraints = [];
                if($recordTypeField->getRequired()) {
                    $constraints['NotBlank'] = new NotBlankValidator();
                }
                //Adding Options
                $options = array(
                                 'label'  => $recordTypeField->getName(),
                                 'required' => $recordTypeField->getRequired(),
                                 'constraints' => $constraints
                                );
                switch ($recordTypeField->getFieldType()->getName()) {
                    case "String":
                        $builder->add(
                                    $recordTypeFieldName,
                                    "text",
                                    $options
                                  );
                        break;
                    case "Text":
                        $builder->add(
                                    $recordTypeFieldName,
                                    "textarea",
                                    $options
                                  );
                        break;
                    case "Integer":
                        $builder->add(
                                    $recordTypeFieldName,
                                    "integer",
                                    $options
                                  );
                        break;
                     case "Float":
                        $options['precision'] = self::FLOAT_PRECISION;
                        $builder->add(
                                    $recordTypeFieldName,
                                    "number",
                                    $options
                                  );
                        break;
                     case "Boolean":
                        unset($constraints['NotBlank']);
                        $options['constraints'] = $constraints;
                        $options['required'] = false;
                        $builder->add(
                                    $recordTypeFieldName,
                                    "checkbox",
                                    $options
                                  );
                        break;
                    case "Date":
                        $options['input'] = 'datetime';
                        $options['widget'] = 'single_text';
                        $builder->add(
                                    $recordTypeFieldName,
                                    "date",
                                    $options
                                  );
                        break;
                    case "Date Time":
                        $options['input'] = 'datetime';
                        $options['date_widget'] = 'single_text';
                        $options['time_widget'] = 'single_text';
                        $builder->add(
                                    $recordTypeFieldName,
                                    "datetime",
                                    $options
                                  );
                        break;
                    case "Time":
                        $options['input'] = 'datetime';
                        $options['widget'] = 'single_text';
                        $builder->add(
                                    $recordTypeFieldName,
                                    "time",
                                    $options
                                  );
                        break;
                    case "Image":
                    case "Audio":
                    case "Video":
                    case "File":
                        $isNullFile = !$recordTypeField->getFieldValues()->isEmpty() && is_null($recordTypeField->getFieldValues()->first()->getStoredFile());
                        if($recordTypeField->getFieldType()->getName() === "Image" && $isNullFile) {
                             $constraints['Image'] = new FileValidator(
                                                                         array(
                                                                            'mimeTypes' => StoredFile::$imageMimeTypes,
                                                                            'mimeTypesMessage' => 'The mime type of the file is invalid ({{ type }}). Only image files are allowed.'
                                                                         )
                                                                 );
                        } else if($recordTypeField->getFieldType()->getName() === "Audio" && $isNullFile) {
                             $constraints['Audio'] = new FileValidator(
                                                                         array(
                                                                             'mimeTypes' => StoredFile::$audioMimeTypes,
                                                                             'mimeTypesMessage' => 'The mime type of the file is invalid ({{ type }}). Only audio files are allowed.'
                                                                          )
                                                                  );
                        } else if($recordTypeField->getFieldType()->getName() === "Video"  && $isNullFile) {
                            $constraints['Video'] = new FileValidator(
                                                                     array(
                                                                             'mimeTypes' => StoredFile::$videoMimeTypes,
                                                                             'mimeTypesMessage' => 'The mime type of the file is invalid ({{ type }}). Only video files are allowed.'
                                                                          )
                                                                  );
                        }
                        if($this->record->getId() !== null && $recordTypeField->getFieldValues()->count() > 0) {
                            unset($constraints['NotBlank']);
                            $options['constraints'] = $constraints;
                            $options['required'] = false;
                        }
                        $options['show_link'] = true;
                        $options['show_image'] = true;
                        $options['show_delete'] = true;
                        $builder->add(
                                    $recordTypeFieldName,
                                    "file",
                                    $options
                                  );
                        break;
                    case "Record":
                        $options['class'] = 'CrossingBordersXBundle:Record';
                        $options['property'] = 'name';
                        if($recordTypeField->getMultipleSelection()) {
                            $options['multiple'] = true;
                        }
                        $options['query_builder'] = function(EntityRepository $er) use ($recordTypeField) {
                                                        return $er->createQueryBuilder('r')
                                                                  ->leftJoin('r.recordType', 'rt')
                                                                  ->andWhere('rt = :record_type')
                                                                  ->orderBy('r.name', 'ASC')
                                                                  ->setParameter('record_type', $recordTypeField->getReferencedRecordType());
                                                    };
                        $builder->add(
                                    $recordTypeFieldName,
                                    "entity",
                                    $options
                                  );
                        break;
                    case "Choice":
                        $options['class'] = 'CrossingBordersXBundle:Choice';
                        $options['property'] = 'name';
                        $options['expanded'] = true;
                        if($recordTypeField->getMultipleSelection()) {
                            $options['multiple'] = true;
                        }
                        $options['query_builder'] = function(EntityRepository $er) use ($recordTypeField) {
                                                        return $er->createQueryBuilder('c')
                                                                  ->leftJoin('c.recordTypeField', 'rtf')
                                                                  ->andWhere('rtf = :record_type_field')
                                                                  ->orderBy('c.displayOrder', 'ASC')
                                                                  ->setParameter('record_type_field', $recordTypeField);
                                                    };
                        $builder->add(
                                    $recordTypeFieldName,
                                    "entity",
                                    $options
                                  );                      
                        break;
                    case "Table":
                        $builder->add($recordTypeFieldName,
                                      new Type\TableType($recordTypeField->getTableColumns()),
                                      $options
                                     );
                        break;
                    default:
                        $builder->add(
                                    $recordTypeFieldName,
                                    "text",
                                    $options
                                  );
                        break;
                }
            }
            /**
             * TODO:
             * Allow to manage relationships from the inverse side. A DQL query like this can be used to get stuff:
             */
            if($this->em !== null) {
                $query = $this->em ->createQuery('SELECT rtf from CrossingBordersXBundle:RecordTypeField rtf '
                                                .'WHERE rtf.referencedRecordType IS NOT NULL '
                                                .'AND rtf.referencedRecordType = :recordType');
                $query->setParameter(':recordType', $this->record->getRecordType());
                $referencedRecordTypeFields = $query->getResult();
                foreach ($referencedRecordTypeFields as $referencedRecordTypeField) {
                    $options = array(
                                        'label' => $referencedRecordTypeField->getRecordType()->getName()
                                                  .'<br />'
                                                  .$referencedRecordTypeField->getName(),
                                        'required' => false,
                                        'class' => 'CrossingBordersXBundle:Record',
                                        'property' => 'name',
                                        'multiple' => 'true',
                                        'query_builder' =>  function(EntityRepository $er) use ($referencedRecordTypeField) {
                                                                return $er->createQueryBuilder('r')
                                                                          ->leftJoin('r.recordType', 'rt')
                                                                          ->andWhere('rt = :record_type')
                                                                          ->orderBy('r.name', 'ASC')
                                                                          ->setParameter('record_type',
                                                                                         $referencedRecordTypeField->getRecordType());
                                                            }
                                    );
                    $recordTypeId = $referencedRecordTypeField->getRecordType()->getId();
                    $recordTypeFieldName = str_replace(' ', '_',  $referencedRecordTypeField->getName());
                    $builder->add(
                                    Record::$recordReferencePrefix.$recordTypeId.'___'.$recordTypeFieldName,
                                    "entity",
                                    $options
                                 );
                }
            }
        }
    }
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CrossingBorders\XBundle\Entity\Record'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'crossingborders_xbundle_record';
    }
}
