<?php
namespace CrossingBorders\XBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use CrossingBorders\XBundle\Entity\TableRow;
use CrossingBorders\XBundle\Entity\TableCell;
use CrossingBorders\XBundle\Entity\Record;


/** TODO: Remove if unused **/
class TableRowTransformer implements DataTransformerInterface {
    public function transform($value) {
        if(!is_null($value)) {
            $cells = array();
            foreach ($value->getCells() as $index => $cell) {
                if(is_null($cell->getRecord())) {
                    $cells["$index"] = $cell->getValue();
                } else {
                    $cells["$index"] = $cell->getRecord();
                }
            }
            return $cells;
        } else {
            return $value;
        }
    }
    public function reverseTransform($value) {
        $tableRow = new TableRow();
        foreach($value as $cell) {
            $tableCell = new TableCell();
            $tableCell->setRow($tableRow);
            if($cell instanceof Record) {
                $tableCell->setRecord($cell);
                $tableCell->setValue(null);
            } else {
                $tableCell->setRecord(null);
                $tableCell->setValue($cell);
            }
        }
       return $tableRow;
    }
}
