<?php
namespace CrossingBorders\XBundle\Form\DataTransformer;

use Doctrine\Common\Persistence\ObjectManager;
 
class RecordToIdTransformer extends EntityToIdTransformer {
    /**
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om) {
        parent::__construct($om);
        $this->setEntityClass("CrossingBorders\\XBundle\\Entity\\Record");
        $this->setEntityRepository("CrossingBordersXBundle:Record");
        $this->setEntityType("record");
    } 
}