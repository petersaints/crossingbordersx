<?php
namespace CrossingBorders\XBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class RecordWizardType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('recordType', 'entity', array(
                        'class' => 'CrossingBordersXBundle:RecordType',
                        'property' => 'name',
                        'query_builder' => function(EntityRepository $er) {
                                                return $er->createQueryBuilder('rt')
                                                          ->orderBy('rt.name', 'ASC');
                                            }
                    ))
                ->add('submit', 'submit', array('label' => 'Next'));
    }
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver){}
    /**
     * @return string
     */
    public function getName() {
        return 'crossingborders_xbundle_recordwizard';
    }
}
