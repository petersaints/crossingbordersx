<?php
namespace CrossingBorders\XBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
/**
 * Description of LabelType
 *
 * @author Pedro
 */
class FormLabelType extends AbstractType {
    public function getParent() {
        return 'text';
    }
    public function getName() {
        return 'formlabel';
    }
    /**
     * @param FormView $view
     * @param FormInterface $form
     * @param array $options
     */
    public function buildView(FormView $view, FormInterface $form, array $options) {
        $view->vars['plain'] = true;
        parent::buildView($view, $form, $options);
    }
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array('required' => false));
    }
}