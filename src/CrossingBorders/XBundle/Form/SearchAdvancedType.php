<?php
namespace CrossingBorders\XBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Doctrine\ORM\EntityRepository;

use CrossingBorders\XBundle\Entity\Record;

class SearchAdvancedType extends AbstractType {
    public static $maxRecursion = 3;
    protected $recordType;
    protected $em = null;
    protected $recursion;
    public function __construct(\CrossingBorders\XBundle\Entity\RecordType $recordType, $em = null, $recursion = 0) {
        $this->recordType = $recordType;
        $this->em = $em;
        $this->recursion = $recursion;
    }
    public function getRecordType() {
        return $this->recordType;
    }
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $defaultFieldOptions = array('required' => false);
        //Shown fields
        if($this->recursion == 0) {
            $builder->add('shownFieldsLabel', 'formlabel', array('label'  => 'Shown Fields'));
            $builder->add('shownFields', new ShownFieldsType($this->recordType), $defaultFieldOptions);
        }
        $builder->add('basicFields', 'formlabel', array('label'  => 'Basic Fields'));
        $builder->add('name', 'text', $defaultFieldOptions);
        $builder->add('tags', 'text', $defaultFieldOptions);
        
        $builder->add('advancedFields', 'formlabel', array('label'  => 'Advanced Fields'));
        foreach ($this->recordType->getRecordTypeFields() as $recordTypeField) {
            $name = str_replace(' ', '_',  $recordTypeField->getName());
            //Adding Options
            $options = array('label'  => $recordTypeField->getName(),
                             'required' => false);
            switch ($recordTypeField->getFieldType()->getName()) {
                case "String":
                    $builder->add(
                                $name,
                                "text",
                                $options
                              );
                    break;
                case "Text":
                    $builder->add(
                                $name,
                                "textarea",
                                $options
                              );
                    break;
                case "Integer":
                    $parentOptions = $options;
                    $builder->add(
                                $name,
                                new Type\IntervalSearchType("integer", $options),
                                $parentOptions
                              );
                    break;
                case "Float":
                    $parentOptions = $options;
                    $options['precision'] = 10;
                    $builder->add(
                                $name,
                                new Type\IntervalSearchType("number", $options),
                                $parentOptions
                              );
                    break;
                case "Boolean":
                    $options['choices'] = array('true' => 'True', 'false' => 'False');
                    $options['expanded'] = true;
                    $builder->add(
                                $name,
                                "choice",
                                $options
                              );
                    break;
                case "Date":
                    $parentOptions = $options;
                    $options['widget'] = 'single_text';
                    $builder->add(
                                $name,
                                new Type\IntervalSearchType("date", $options),
                                $parentOptions
                              );
                    break;
                case "Date Time":
                    $parentOptions = $options;
                    $options['date_widget'] = 'single_text';
                    $options['time_widget'] = 'single_text';
                    $builder->add(
                                $name,
                                new Type\IntervalSearchType("datetime", $options),
                                $parentOptions
                              );
                    break;
                case "Time":
                    $parentOptions = $options;
                    $options['widget'] = 'single_text';
                    $builder->add(
                                $name,
                                new Type\IntervalSearchType("time", $options),
                                $parentOptions
                              );
                    break;
                case "Image":
                case "Audio":
                case "Video":
                case "File":
                    $builder->add(
                                   $name,
                                   "text",
                                   $options
                                 );
                    break;
                case "Choice":
                    $options['class'] = 'CrossingBordersXBundle:Choice';
                    $options['property'] = 'name';
                    $options['expanded'] = true;
                    if($recordTypeField->getMultipleSelection()) {
                        $options['multiple'] = true;
                    }
                    $options['query_builder'] = function(EntityRepository $er) use ($recordTypeField) {
                                                    return $er->createQueryBuilder('c')
                                                              ->leftJoin('c.recordTypeField', 'rtf')
                                                              ->andWhere('rtf = :record_type_field')
                                                              ->orderBy('c.displayOrder', 'ASC')
                                                              ->setParameter('record_type_field',
                                                                            $recordTypeField);
                                                };
                    $builder->add(
                                $name,
                                "entity",
                                $options
                              );
                    break;
                case "Table":
                        $parentOptions = $options;
                        $builder->add(
                                      $name,
                                      new Type\TableSearchType($recordTypeField->getTableColumns(), $options),
                                      $parentOptions
                                     );
                        break;
                case "Record":
                    /**
                     * Recursively add another form for related record types
                     * I have limited to a certain degree of recursion to avoid infinite loops and UI problems
                     * TODO: {{Maybe}} also allow a simple dropdown selection like for referenc*ING* records [[BELOW]]?
                     */
                    if($this->recursion < self::$maxRecursion) {
                        $builder->add(
                                   $name,
                                   new SearchAdvancedType(
                                                            $recordTypeField->getReferencedRecordType(),
                                                            $this->em,
                                                            $this->recursion+1
                                                         ),
                                   $options
                                 );
                    }
                    break;
                default:
                    $builder->add(
                                   $name,
                                   "text",
                                   $options
                                 );
                    break;
            }
        }
        /**
         * TODO: {{Maybe}} add form sub-search for referenc*ING* like for a referenc*ED* record [[ABOVE]]
         */
        if($this->em !== null) {
            $query = $this->em ->createQuery('SELECT rtf from CrossingBordersXBundle:RecordTypeField rtf '
                                            .'WHERE rtf.referencedRecordType IS NOT NULL '
                                            .'AND rtf.referencedRecordType = :recordType');
            $query->setParameter(':recordType', $this->recordType);
            $referencedRecordTypeFields = $query->getResult();
            foreach ($referencedRecordTypeFields as $referencedRecordTypeField) {
                $options = array('label' => $referencedRecordTypeField->getRecordType()->getName()
                                .'<br />'.$referencedRecordTypeField->getName(),
                                'required' => false,
                                'class' => 'CrossingBordersXBundle:Record',
                                'property' => 'name',
                                'multiple' => 'true',
                                'query_builder' =>  function(EntityRepository $er) use ($referencedRecordTypeField) {
                                                        return $er->createQueryBuilder('r')
                                                                    ->leftJoin('r.recordType', 'rt')
                                                                    ->andWhere('rt = :record_type')
                                                                    ->orderBy('r.name', 'ASC')
                                                                    ->setParameter('record_type', $referencedRecordTypeField->getRecordType());
                                                    }
                                );
                $recordTypeId = $referencedRecordTypeField->getRecordType()->getId();
                $recordTypeFieldName = str_replace(' ', '_',  $referencedRecordTypeField->getName());
                $builder->add(Record::$recordReferencePrefix.$recordTypeId.'___'.$recordTypeFieldName,
                              "entity",
                              $options);
            }
        }
    }
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array('csrf_protection' => false, 'subfield' => true));
    }
    public function buildView(FormView $view, FormInterface $form, array $options) {
        parent::buildView($view, $form, $options);
        $view->vars = array_replace($view->vars, array('subfield' => $options['subfield']));
    }
    /**
     * @return string
     */
    public function getName() {
        return 'search_advanced';
    }
}