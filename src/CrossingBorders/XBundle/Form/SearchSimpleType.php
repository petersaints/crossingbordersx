<?php
namespace CrossingBorders\XBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SearchSimpleType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $defaultFieldOptions = array('required' => false);
        $builder->add('name', 'text', $defaultFieldOptions);
        $builder->add('fields', 'text', $defaultFieldOptions);
        $builder->add('recordType', 'text', $defaultFieldOptions);
        $builder->add('tags', 'text', $defaultFieldOptions);
    }
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver){
        $resolver->setDefaults(array('csrf_protection'   => false));
    }
    /**
     * @return string
     */
    public function getName() {
        return 'search_simple';
    }
}
