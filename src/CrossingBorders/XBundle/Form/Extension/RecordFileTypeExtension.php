<?php
namespace CrossingBorders\XBundle\Form\Extension;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use CrossingBorders\XBundle\Entity\Record;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;


class RecordFileTypeExtension extends AbstractTypeExtension{
    /**
     * Returns the name of the type being extended.
     *
     * @return string The name of the type being extended
     */
    public function getExtendedType() {
        return "file";
    }
    /**
     * Add the image_path option
     *
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setOptional(array(
                                     'show_link',
                                     'show_image',
                                     'show_delete'
                                    )
                              );
    }
    /**
     * Pass the image URL to the view
     *
     * @param FormView $view
     * @param FormInterface $form
     * @param array $options
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['show_link'] = array_key_exists('show_link', $options)?$options['show_link']:false;
        $view->vars['show_image'] = array_key_exists('show_image', $options)?$options['show_image']:false;
        $view->vars['show_delete'] = array_key_exists('show_delete', $options)?$options['show_delete']:false;;
        $parentData = $form->getParent()->getData();
        if (null !== $parentData && $parentData instanceof Record) {
            $fieldValueFile = $parentData->getFieldValueFile($view->vars['label']);
            if($fieldValueFile !== null && $fieldValueFile->getExists()) {
                try {
                    $view->vars['show_image'] = $view->vars['show_image'] && $fieldValueFile->getIsImage();
                    $view->vars['file_path'] = $fieldValueFile->getWebPathOriginal();
                    $view->vars['thumbnail_path'] = $fieldValueFile->getWebPathThumbnail();
                    $view->vars['filename'] = $fieldValueFile->getOriginalName();
                } catch (FileNotFoundException $e) {
                    $view->vars['show_link'] = false;
                    $view->vars['show_image'] = false;
                    $view->vars['show_delete'] = false;
                } 
            } else {
                $view->vars['show_link'] = false;
                $view->vars['show_image'] = false;
                $view->vars['show_delete'] = false;
            }
        }   
    }
}