<?php

namespace CrossingBorders\XBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use CrossingBorders\XBundle\Form\DataTransformer\RecordToIdTransformer;

class CommentType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $recordTransformer = new RecordToIdTransformer($options["em"]);
        $builder->add('text','textarea')
                ->add($builder->create('record', 'hidden')->addModelTransformer($recordTransformer));
    }
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CrossingBorders\XBundle\Entity\Comment',
        ));
        $resolver->setRequired(
                                array(
                                        'em'
                                     )
                              );
    }
    /**
     * @return string
     */
    public function getName()
    {
        return 'crossingborders_xbundle_comment';
    }
}
