<?php
namespace CrossingBorders\XBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;

class ShownFieldsType extends AbstractType {
    public static $maxRecursion = 3;
    public static $parentLabel = 'name';
    protected $recordType;
    protected $recursion;
    public function __construct(\CrossingBorders\XBundle\Entity\RecordType $recordType, $recursion = 0) {
        $this->recordType = $recordType;
        $this->recursion = $recursion;
    }    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        foreach ($this->recordType->getRecordTypeFields() as $recordTypeField) {
            $name = str_replace(' ', '_',  $recordTypeField->getName());
            if($recordTypeField->getFieldType()->getName() === "Record" && $this->recursion < self::$maxRecursion) {
                $builder->add($name, new ShownFieldsType($recordTypeField->getReferencedRecordType(),
                                                         $this->recursion+1),
                              array('label'  => $recordTypeField->getName(),
                                    'required' => false,
                                    'plain' => false,
                                    'subfield' => true)
                             );
            } else {
                if($this->recursion > 0 && !$builder->has('name')) {
                    $builder->add(ShownFieldsType::$parentLabel, 'checkbox');
                }
                $builder->add($name, 'checkbox',
                              array('label'  => $recordTypeField->getName(),
                                    'required' => false));
            }
        }
    }
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver){
        $resolver->setDefaults(array(
            'plain' => true,
            'subfield' => false
        ));
    }
    /**
     * @param FormView $view
     * @param FormInterface $form
     * @param array $options
     */
    public function buildView(FormView $view, FormInterface $form, array $options) {
        parent::buildView($view, $form, $options);
        $view->vars['subfield'] = $options['subfield'];
        $view->vars['plain'] = $options['plain'];
    }
    /**
     * @return string
     */
    public function getName() {
        return 'shown_fields';
    }
    public static function convertToArray(FormInterface $form) {
        $shownFields = [];
        foreach ($form as $key => $value) {
            $name = str_replace('_', ' ',  $key);
            if(count($value->all()) > 0) {
                $shownFields[$name] = ShownFieldsType::convertToArray($value);
            } else if($name !== ShownFieldsType::$parentLabel && $value->getData() === true) {
                $shownFields[$name] = $name;
            } else if($name === ShownFieldsType::$parentLabel && $value->getData() === false) {
                $shownFields = null;
            }
        }
        return $shownFields;
    }
}