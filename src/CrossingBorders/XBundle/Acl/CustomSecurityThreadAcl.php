<?php
namespace CrossingBorders\XBundle\Acl;

use FOS\CommentBundle\Acl\SecurityThreadAcl;
use Symfony\Component\Security\Acl\Model\AclInterface;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Symfony\Component\Security\Acl\Domain\RoleSecurityIdentity;

class CustomSecurityThreadAcl extends SecurityThreadAcl {
    protected function doInstallFallbackAcl(AclInterface $acl, MaskBuilder $builder) {
        $builder->add('iddqd');
        $acl->insertClassAce(new RoleSecurityIdentity('ROLE_SUPER_ADMIN'), $builder->get());
        $acl->insertClassAce(new RoleSecurityIdentity('ROLE_ADMIN'), $builder->get());

        $builder->reset();
        $builder->add('create');
        $builder->add('view');
        $acl->insertClassAce(new RoleSecurityIdentity('IS_AUTHENTICATED_ANONYMOUSLY'), $builder->get());

        $builder->reset();
        $builder->add('create');
        $builder->add('view');
        $acl->insertClassAce(new RoleSecurityIdentity('ROLE_USER'), $builder->get());
    }
}