<?php
namespace CrossingBorders\XBundle\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class LocaleEventListener
{
    public function onKernelRequest(GetResponseEvent $event)
    {
        /**
         * TODO: Make the locale selecting based on URL prefix.
         * At most, try to redirect to the URL prefix based on the browser's
         * prefered language when no prefix is defined.
         * 
         * $request = $event->getRequest();
           $locale = $request->getPreferredLanguage(array('en', 'pt'));
           $request->setLocale($locale);
           $request->getLocale();
         */       
    }
}
