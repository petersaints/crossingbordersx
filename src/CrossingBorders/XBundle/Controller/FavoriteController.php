<?php

namespace CrossingBorders\XBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use CrossingBorders\XBundle\Entity\Favorite;

/**
 * Favorite controller.
 * @Security("has_role('ROLE_USER')")
 * @Route("/favorite")
 */
class FavoriteController extends Controller
{
    /**
     * Lists all Favorite entities.
     * @Route("/{typeId}", name="favorite", requirements={"typeId" = "\d+"}, defaults={"typeId" = null})
     * @Method("GET")
     * @Template()
     */
    public function indexAction($typeId)
    {
        $type = null;
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQueryBuilder()
                    ->select('count(f.id)')
                    ->from('CrossingBordersXBundle:Favorite', 'f')
                    ->orderBy('f.name','ASC');
        if(!is_null($typeId)) {
            $query = $query->leftJoin('f.type', 'ft')
                           ->where('ft.id = :ftid')
                           ->setParameter('ftid',$typeId);
            
            $type = $em->getRepository('CrossingBordersXBundle:FavoriteType')->findOneBy(array('id' => $typeId));
        }
        $count = $query->getQuery()->getSingleScalarResult();
        return array('count' => $count,'type' => $type);
    }
    /**
     * Lists all Favorite entities.
     * @Route("/page/{page}/{typeId}", name="favorite_page", requirements={"page" = "\d+" ,"typeId" = "\d+"}, defaults={"typeId" = null, "page" = 1})
     * @Method("GET")
     * @Template()
     */
    public function favoritePageAction($page, $typeId)
    {
        $type = null;
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQueryBuilder()
                    ->select('f')
                    ->from('CrossingBordersXBundle:Favorite', 'f')
                    ->orderBy('f.name','ASC');
        if(!is_null($typeId)) {
            $query = $query->leftJoin('f.type', 'ft')
                           ->where('ft.id = :ftid')
                           ->setParameter('ftid',$typeId);
            
            $type = $em->getRepository('CrossingBordersXBundle:FavoriteType')->findOneBy(array('id' => $typeId));
        }
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $page,
            $this->container->hasParameter('favorite.favorites_per_page')?
            $this->container->getParameter('favorite.favorites_per_page'):20
        );
        return array('pagination' => $pagination,'type' => $type);
    }
    /**
     * List favorite types.
     *
     * @Route("/list/type", name="favorite_type_list")
     * @Method("GET")
     * @Template()
     */
    public function typeListAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('CrossingBordersXBundle:FavoriteType')->findBy(array(),array('id' => 'ASC'));
        return array('entities' => $entities);
    }
    /**
     * Lists all Favorite entities.
     *
     * @Route("/", name="favorite_add")
     * @Method("POST")
     * @Template()
     */
    public function addAction(Request $request)
    {
        if(!$this->get('form.csrf_provider')->isCsrfTokenValid('add-favorite', $request->get('csrfToken'))) {
           throw new \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException('CSRF token is invalid.');
        }
        $em = $this->getDoctrine()->getManager();
        $favorite = new Favorite();
        $favorite->setUser($this->getUser());
        $favorite->setName($request->get('name'));
        $favorite->setType($em->getRepository('CrossingBordersXBundle:FavoriteType')->findOneBy(array('name' => $request->get('type'))));
        $favorite->setUrl($request->get('url'));
        $em->persist($favorite);
        $em->flush();
        
        $response = new JsonResponse();
        $response->setData(array('status' => 'success'));
        return $response;
    }
    /**
     * Edits a Favorite entity.
     *
     * @Route("/{id}", name="favorite_edit")
     * @Method("PUT")
     */
    public function editAction(Request $request, $id)
    {
        if(!$this->get('form.csrf_provider')->isCsrfTokenValid('delete-favorite', $request->get('csrfToken'))) {
           throw new \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException('CSRF token is invalid.');
        }
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CrossingBordersXBundle:Favorite')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Favorite entity.');
        }
        $entity->setName($request->get('name'));
        $em->flush();
        
        $response = new JsonResponse();
        $response->setData(array('status' => 'success'));
        return $response;
    }
    /**
     * Deletes a Favorite entity.
     *
     * @Route("/{id}", name="favorite_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        if(!$this->get('form.csrf_provider')->isCsrfTokenValid('delete-favorite', $request->get('csrfToken'))) {
           throw new \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException('CSRF token is invalid.');
        }
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CrossingBordersXBundle:Favorite')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Favorite entity.');
        }
        $em->remove($entity);
        $em->flush();
        
        $response = new JsonResponse();
        $response->setData(array('status' => 'success'));
        return $response;
    }
}