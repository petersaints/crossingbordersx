<?php

namespace CrossingBorders\XBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use CrossingBorders\XBundle\Entity\RecordType;
use CrossingBorders\XBundle\Form\RecordTypeType;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * RecordType controller.
 *
 * @Route("/recordtype")
 */
class RecordTypeController extends Controller
{

    /**
     * Lists all RecordType entities.
     *
     * @Route("/page/{page}", name="recordtype", requirements={"page" = "\d+"}, defaults={"page" = 1})
     * @Method("GET")
     * @Template()
     */
    public function indexAction($page)
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQueryBuilder()
                    ->select('rt')
                    ->from('CrossingBordersXBundle:RecordType', 'rt')
                    ->orderBy('rt.name','ASC');
        
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $page,
            $this->container->hasParameter('recordtype.recordtypes_per_page')?
            $this->container->getParameter('recordtype.recordtypes_per_page'):20
        );
        // parameters to template
        return array(
                      'pagination' => $pagination,
                      'count' => $pagination->getTotalItemCount()
                    );
    }
    
    /**
     * Creates a new RecordType entity.
     *
     * @Route("/", name="recordtype_create")
     * @Method("POST")
     * @Template("CrossingBordersXBundle:RecordType:new.html.twig")
     * @Security("has_role('ROLE_EDITOR')")
     */
    public function createAction(Request $request)
    {
        $recordType = new RecordType();
        $form = $this->createCreateForm($recordType);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $fieldTypes = $recordType->getRecordTypeFields();
            $recordType->clearRecordTypeFields();
            $em->persist($recordType);
            $em->flush();
            foreach ($fieldTypes as $fieldType) {
                $fieldType->setRecordType($recordType);
                $em->persist($fieldType);
            }
            $em->flush();
            return $this->redirect($this->generateUrl('recordtype_show', array('id' => $recordType->getId())));
        }
        return array(
            'entity' => $recordType,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a RecordType entity.
    *
    * @param RecordType $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(RecordType $entity)
    {
        $form = $this->createForm(new RecordTypeType(), $entity, array(
            'action' => $this->generateUrl('recordtype_create'),
            'method' => 'POST',
        ));
        $form->add('submit', 'submit', array('label' => $this->get('translator')->trans('Create')));
        return $form;
    }

    /**
     * Displays a form to create a new RecordType entity.
     *
     * @Route("/new", name="recordtype_new")
     * @Method("GET")
     * @Template()
     * @Security("has_role('ROLE_EDITOR')")
     */
    public function newAction()
    {
        $entity = new RecordType();
        $form   = $this->createCreateForm($entity);
        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a RecordType entity.
     *
     * @Route("/{id}", name="recordtype_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CrossingBordersXBundle:RecordType')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find RecordType entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing RecordType entity.
     *
     * @Route("/{id}/edit", name="recordtype_edit")
     * @Method("GET")
     * @Template()
     * @Security("has_role('ROLE_EDITOR')")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CrossingBordersXBundle:RecordType')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find RecordType entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a RecordType entity.
    *
    * @param RecordType $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(RecordType $entity)
    {
        $form = $this->createForm(new RecordTypeType(), $entity, array(
            'action' => $this->generateUrl('recordtype_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => $this->get('translator')->trans('Update')));

        return $form;
    }
    /**
     * Edits an existing RecordType entity.
     *
     * @Route("/{id}", name="recordtype_update")
     * @Method("PUT")
     * @Template("CrossingBordersXBundle:RecordType:edit.html.twig")
     * @Security("has_role('ROLE_EDITOR')")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CrossingBordersXBundle:RecordType')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find RecordType entity.');
        }
        $originalRecordTypeFields = new ArrayCollection();
        $originalChoices = [];
        $originalTableColumns = [];
        // Create an ArrayCollection of the current objects in the database
        foreach ($entity->getRecordTypeFields() as $key => $recordTypeField) {
            $originalRecordTypeFields->add($recordTypeField);
            foreach ($recordTypeField->getChoices() as $choice) {
                if(!array_key_exists($key, $originalChoices)) {
                    $originalChoices[$key] = new ArrayCollection();
                }
                $originalChoices[$key]->add($choice);
            }
            foreach ($recordTypeField->getTableColumns() as $column) {
                if(!array_key_exists($key, $originalTableColumns)) {
                    $originalTableColumns[$key] = new ArrayCollection();
                }
                $originalTableColumns[$key]->add($column);
            }
        }
        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);
        if ($editForm->isValid()) {
            // remove the unvalid relationships
            foreach ($originalRecordTypeFields as $recordTypeField) {
                if (false === $entity->getRecordTypeFields()->contains($recordTypeField)){
                    $em->remove($recordTypeField);
                } else {
                    $key = $entity->getRecordTypeFields()->indexOf($recordTypeField);
                    if(array_key_exists($key, $originalChoices)) {
                        foreach($originalChoices[$key] as $choice) {
                            if (false === $recordTypeField->getChoices()->contains($choice)) {
                                $choice->setRecordTypeField(null);
                                $em->remove($choice);
                            }
                        }
                    }
                    if(array_key_exists($key, $originalTableColumns)) {
                        foreach($originalTableColumns[$key] as $column) {
                            if (false === $recordTypeField->getTableColumns()->contains($column)) {
                                $column->setRecordTypeField(null);
                                $em->remove($column);
                            }
                        }
                    }
                }
            }
            $em->flush();
            return $this->redirect($this->generateUrl('recordtype_edit', array('id' => $entity->getId())));
        }
        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a RecordType entity.
     *
     * @Route("/{id}", name="recordtype_delete")
     * @Method("DELETE")
     * @Security("has_role('ROLE_EDITOR')")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CrossingBordersXBundle:RecordType')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find RecordType entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('recordtype'));
    }

    /**
     * Creates a form to delete a RecordType entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('recordtype_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => $this->get('translator')->trans('Delete')))
            ->getForm()
        ;
    }
}
