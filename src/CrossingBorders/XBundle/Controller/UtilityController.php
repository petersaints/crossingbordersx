<?php
namespace CrossingBorders\XBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * Utility controller
 * @Route("/utility")
 */
class UtilityController extends Controller {
    /**
     * Export an URI to PDF (limited to "local" URIs)
     * @Route("/export/pdf", name="utility_export_pdf")
     * @Method("GET")
     */
    public function exportPdfAction(Request $request) {
        $url = urldecode($request->get('url'));
        if (strpos($url, 'http://') === 0) {
            throw new \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException('You can not provide an absolute URL');
        } else {
            $url = $request->getSchemeAndHttpHost().$request->getBaseUrl().$url;
            return new Response(
                $this->get('knp_snappy.pdf')->getOutput($url), 200,
                array('Content-Type'        => 'application/pdf',
                      'Content-Disposition' => 'filename="document.pdf"')
            );
        }
    }
}