<?php
namespace CrossingBorders\XBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use CrossingBorders\XBundle\Form\RecordWizardType;
use CrossingBorders\XBundle\Form\SearchSimpleType;
use CrossingBorders\XBundle\Form\SearchAdvancedType;
use CrossingBorders\XBundle\Form\ShownFieldsType;
use CrossingBorders\XBundle\Utilities\SearchUtilities;

/**
 * Gallery controller.
 *
 * @Route("/gallery")
 */
class GalleryController extends Controller {    
    /**
     * Lists all Record entities.
     *
     * @Route("/simple", name="gallery_simple")
     * @Method("GET")
     * @Template()
     */
    public function simpleAction() {
        return array(
            'form' => $this->createSimpleSearchForm()->createView()
        );
    }
    
    /**
     * Lists all Record entities.
     *
     * @Route("/simple/page/{page}", name="gallery_simple_results", requirements={"page" = "\d+"}, defaults={"page" = 1})
     * @Method("GET")
     * @Template()
     */
    public function simpleResultsAction($page, Request $request) {
        $form = $this->createSimpleSearchForm();
        $form->handleRequest($request);
        
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction();
            try {
                $query = SearchUtilities::simpleSearchBuildQuery($form, $em);
                $paginator  = $this->get('knp_paginator');
                $pagination = $paginator->paginate(
                    $query,
                    $page,
                    $this->container->hasParameter('record.records_per_page')?
                    $this->container->getParameter('record.records_per_page'):20
                );
                $em->getConnection()->commit();
                return $this->render('CrossingBordersXBundle:Gallery:simpleResults.html.twig',
                                        array(
                                              'pagination' => $pagination,
                                              'count' => $pagination->getTotalItemCount()
                                        )
                                    );
            } catch (Exception $e) {
                $em->getConnection()->rollback();
                throw $e;
            }
        }
        return $this->render('CrossingBordersXBundle:Gallery:simple.html.twig',
                                array(
                                        'form' => $form->createView()
                                )
                            );
    }
    //--- Advanced Search ---
    /**
     * @Route("/advanced/wizard", name="gallery_advanced_wizard")
     * @Method("GET")
     * @Template()
     */
    public function advancedWizardAction() {
        $form = $this->createForm(new RecordWizardType(), null, array(
            'action' => $this->generateUrl('gallery_advanced_wizard_post'),
            'method' => 'POST',
        ));        
        return array('form'   => $form->createView());
    }
    /**
     * @Route("/advanced/wizard", name="gallery_advanced_wizard_post")
     * @Method("POST")
     * @Template()
     */
    public function advancedWizardPostAction(Request $request) {
        $formWizard = $this->createForm(new RecordWizardType());
        $formWizard->handleRequest($request);
        $recordTypeId = $formWizard->get('recordType')->getData()->getId();
        
        $form = $this->createForm(new RecordWizardType(), null, array(
            'action' => $this->generateUrl('record_create', array('recordTypeId' => $recordTypeId)),
            'method' => 'POST',
        ));
        $form->handleRequest($request);
        if ($form->isValid()) {
            return $this->redirect($this->generateUrl('gallery_advanced', array('recordTypeId' => $recordTypeId)));
        }
        return array('form' => $form->createView());        
    }
    
    /**
     * @Route("/advanced/recordtype/{recordTypeId}", requirements={"recordTypeId" = "\d+"}, name="gallery_advanced")
     * @Method("GET")
     * @Template()
     */
    public function advancedAction($recordTypeId)
    {
        $em = $this->getDoctrine()->getManager();
        $recordType = $em->getRepository('CrossingBordersXBundle:RecordType')->find($recordTypeId);
        if(is_null($recordType)) {
            throw $this->createNotFoundException("The specified record type was not found");
        }
        $form   = $this->createAdvancedSearchForm($recordType);
        return array(
            'recordType' => $recordType,
            'form'   => $form->createView(),
        );
    }
    /**
     * Lists all Record entities.
     *
     * @Route("/advanced/recordtype/{recordTypeId}/results", name="gallery_advanced_results", requirements={"recordTypeId" = "\d+"})
     * @Method("GET")
     * @Template()
     */
    public function advancedResultsAction($recordTypeId, Request $request) {
        $em = $this->getDoctrine()->getManager();
        $em->getConnection()->beginTransaction();
        try {
            $recordType = $em->getRepository('CrossingBordersXBundle:RecordType')->find($recordTypeId);
            if(is_null($recordType)) {
                throw $this->createNotFoundException("The specified record type was not found");
            }
            $form = $this->createAdvancedSearchForm($recordType);
            $form->handleRequest($request);
            if ($form->isValid()) {
                //Advanced search logic implemented in this function
                $query = SearchUtilities::advancedSearchBuildQuery($form, $recordType, $em);
                $resultCount = count($query->getQuery()->getResult());
                $em->getConnection()->commit();
                $shownFields = array();
                if($form->has('shownFields')) {
                    $shownFields = ShownFieldsType::convertToArray($form->get('shownFields'));
                }
                return $this->render('CrossingBordersXBundle:Gallery:advancedResults.html.twig',
                                        array(
                                                'count' => $resultCount,
                                                'shownFields' => $shownFields
                                        )
                                    );
            }
        } catch (Exception $e) {
            $em->getConnection()->rollback();
            throw $e;
        }
        return $this->render('CrossingBordersXBundle:Gallery:advanced.html.twig',
                                array(
                                        'recordType' => $recordType,
                                        'form' => $form->createView()
                                )
                            );
    }
    /**
     * @Route("/advanced/results/recordtype/{recordTypeId}/page/{page}", name="gallery_advanced_results_page", requirements={"recordTypeId" = "\d+", "page" = "\d+"}, defaults={"page" = 1})
     * @Method("GET")
     * @Template()
     */
    public function advancedResultsPageAction($recordTypeId, $page, Request $request) {
        $em = $this->getDoctrine()->getManager();
        $em->getConnection()->beginTransaction();
        try {
            $recordType = $em->getRepository('CrossingBordersXBundle:RecordType')->find($recordTypeId);
            if(is_null($recordType)) {
                throw $this->createNotFoundException("The specified record type was not found");
            }
            $form = $this->createAdvancedSearchForm($recordType);
            $form->handleRequest($request);
            if ($form->isValid()) {
                //Advanced search logic implemented in this function
                $query = SearchUtilities::advancedSearchBuildQuery($form, $recordType, $em);
                $paginator  = $this->get('knp_paginator');
                $pagination = $paginator->paginate(
                    $query,
                    $page,
                    $this->container->hasParameter('record.records_per_page')?
                          $this->container->getParameter('record.records_per_page'):
                          20
                );
                $em->getConnection()->commit();
                $shownFields = array();
                if($form->has('shownFields')) {
                    $shownFields = ShownFieldsType::convertToArray($form->get('shownFields'));
                }
                return $this->render('CrossingBordersXBundle:Gallery:advancedResultsPage.html.twig',
                                        array(
                                                'pagination' => $pagination,
                                                'count' => $pagination->getTotalItemCount(),
                                                'shownFields' => $shownFields
                                        )
                                    );
            }
        } catch (Exception $e) {
            $em->getConnection()->rollback();
            throw $e;
        }
        throw $this->createNotFoundException('Invalid search parameters');
    }
    private function createSimpleSearchForm() {
        $form = $this->createForm(new SearchSimpleType(), null, array(
            'action' => $this->generateUrl('gallery_simple_results'),
            'method' => 'GET'
        ));
        $form->add('submit', 'submit', array('label' => $this->get('translator')->trans('Search')));
        return $form;
    } 
    private function createAdvancedSearchForm($recordType) {
        $form = $this->createForm(new SearchAdvancedType($recordType, $this->getDoctrine()->getManager()), null, array(
            'action' => $this->generateUrl('gallery_advanced_results', array('recordTypeId' => $recordType->getId())),
            'method' => 'GET'
        ));
        $form->add('submit', 'submit', array('label' => $this->get('translator')->trans('Search')));
        return $form;
    }
}
