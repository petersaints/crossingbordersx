<?php
namespace CrossingBorders\XBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use CrossingBorders\XBundle\Form\ContactType;

class DefaultController extends Controller {
    public function indexAction(Request $request) {
        return $this->render('CrossingBordersXBundle:Default:index.html.twig');
    }
    public function aboutAction(Request $request) {
        $version = $this->container->hasParameter('version')?
                   $this->container->getParameter('version'):
                   '1.0';
        return $this->render('CrossingBordersXBundle:Default:about.html.twig', array('version' => $version));
    }
    public function contactAction(Request $request) {
        $form = $this->createForm(new ContactType(), null, array(
            'action' => $this->generateUrl('contact'),
            'method' => 'POST',
        ));
        $emailAddress = $this->container->hasParameter('contact.email')?
                        $this->container->getParameter('contact.email'):
                        'petersaints+crossingbordersx@gmail.com';
        if ($request->isMethod('POST')) {
            $form->bind($request);
            if ($form->isValid()) {
                $message = \Swift_Message::newInstance()
                    ->setSubject($form->get('subject')->getData())
                    ->setFrom($form->get('email')->getData())
                    ->setTo($emailAddress)
                    ->setBody(
                        $this->renderView(
                            'CrossingBordersXBundle:Mail:contact.html.twig',
                            array(
                                'ip' => $request->getClientIp(),
                                'name' => $form->get('name')->getData(),
                                'email' => $form->get('email')->getData(),
                                'subject' => $form->get('subject')->getData(),
                                'message' => $form->get('message')->getData()
                            )
                        ),'text/html');
                $this->get('mailer')->send($message);
                $request->getSession()->getFlashBag()->add('success', $this->get('translator')->trans('Your email has been sent! Thank you!'));
                return $this->redirect($this->generateUrl('contact'));
            }
        }
        return $this->render('CrossingBordersXBundle:Default:contact.html.twig', array('form' => $form->createView(), 'emailAddress' => $emailAddress));
    }
}