<?php
namespace CrossingBorders\XBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use CrossingBorders\XBundle\Entity\Record;
use CrossingBorders\XBundle\Entity\Tag;
use CrossingBorders\XBundle\Entity\RecordTag;
use CrossingBorders\XBundle\Form\RecordType;
use CrossingBorders\XBundle\Form\RecordWizardType;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Exception\AclNotFoundException;

/**
 * Record controller.
 *
 * @Route("/record")
 */
class RecordController extends Controller
{

    /**
     * Lists all Record entities.
     *
     * @Route("/page/{page}", name="record", requirements={"page" = "\d+"}, defaults={"page" = 1})
     * @Method("GET")
     * @Template()
     */
    public function indexAction($page)
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQueryBuilder()
                    ->select('r')
                    ->from('CrossingBordersXBundle:Record', 'r')
                    ->leftJoin('r.recordType', 'rt')
                    ->addSelect('rt')
                    ->orderBy('r.name','ASC');
        
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $page,
            $this->container->hasParameter('record.records_per_page')?
            $this->container->getParameter('record.records_per_page'):20
        );
        return array(
            'pagination' => $pagination,
            'count' => $pagination->getTotalItemCount()
        );
    }

    /**
     * Wizard to create a new Record entity.
     *
     * @Route("/wizard", name="record_wizard_create")
     * @Method("POST")
     * @Template("CrossingBordersXBundle:Record:new.html.twig")
     * @Security("has_role('ROLE_EDITOR')")
     */
    public function wizardCreateAction(Request $request)
    {
        $formWizard = $this->createForm(new RecordWizardType());
        $formWizard->handleRequest($request);
        $recordTypeId = $formWizard->get('recordType')->getData()->getId();
        
        $form = $this->createForm(new RecordWizardType(), null, array(
            'action' => $this->generateUrl('record_create', array('recordTypeId' => $recordTypeId)),
            'method' => 'POST',
        ));
        $form->handleRequest($request);
        if ($form->isValid()) {
            return $this->redirect($this->generateUrl('record_new', array('recordTypeId' => $recordTypeId)));
        }
        return array('form' => $form->createView());
    }
    
    /**
     * Creates a new Record entity.
     *
     * @Route("/recordtype/{recordTypeId}", requirements={"recordTypeId" = "\d+"}, name="record_create")
     * @Method("POST")
     * @Template("CrossingBordersXBundle:Record:new.html.twig")
     * @Security("has_role('ROLE_EDITOR')")
     */
    public function createAction($recordTypeId, Request $request)
    {
        $entity = new Record();
        $em = $this->getDoctrine()->getManager();
        $recordType = $em->getRepository('CrossingBordersXBundle:RecordType')->find($recordTypeId);
        if(is_null($recordType)) {
            throw $this->createNotFoundException("The specified record type was not found");
        }
        $entity->setRecordType($recordType);
        
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $this->setCommaDelimitedTags($entity, $form->get('tags')->getData());
            $em->persist($entity);
            $em->flush();
            return $this->redirect($this->generateUrl('record_show', array('id' => $entity->getId())));
        }
        
        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a Record entity.
    *
    * @param Record $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Record $entity)
    {
        $form = $this->createForm(new RecordType($entity, $this->getDoctrine()->getManager()), $entity, array(
            'action' => $this->generateUrl('record_create', array('recordTypeId' => $entity->getRecordType()->getId())),
            'method' => 'POST',
        ));
        $form->add('submit', 'submit', array('label' => $this->get('translator')->trans('Create')));
        return $form;
    }

    /**
     * Displays a form to create a new Record entity.
     *
     * @Route("/wizard", name="record_wizard")
     * @Method("GET")
     * @Template()
     * @Security("has_role('ROLE_EDITOR')")
     */
    public function wizardAction()
    {
        $form = $this->createForm(new RecordWizardType(), null, array(
            'action' => $this->generateUrl('record_wizard_create'),
            'method' => 'POST',
        ));        
        return array('form'   => $form->createView());
    }
    
    /**
     * Displays a form to create a new Record entity.
     *
     * @Route("/new/recordtype/{recordTypeId}", requirements={"recordTypeId" = "\d+"}, name="record_new")
     * @Method("GET")
     * @Template()
     * @Security("has_role('ROLE_EDITOR')")
     */
    public function newAction($recordTypeId)
    {
        $em = $this->getDoctrine()->getManager();
        $recordType = $em->getRepository('CrossingBordersXBundle:RecordType')->find($recordTypeId);
        if(is_null($recordType)) {
            throw $this->createNotFoundException("The specified record type was not found");
        }
        $entity = new Record();
        $entity->setRecordType($recordType);
        
        $form   = $this->createCreateForm($entity);
        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Record entity.
     *
     * @Route("/{id}", name="record_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CrossingBordersXBundle:Record')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Record entity.');
        }
        $deleteForm = $this->createDeleteForm($id);
        
        $fieldValues = $em->createQueryBuilder()
                          ->select('fv')
                          ->from('CrossingBordersXBundle:FieldValue', 'fv')
                          ->leftJoin('fv.record', 'r')
                          ->leftJoin('fv.recordTypeField', 'rtf')
                          ->where('r.id = :record_id')
                          ->orderBy('rtf.displayOrder','ASC')
                          ->setParameter('record_id', $id)
                          ->getQuery()
                          ->getResult();    
        return array(
            'entity'      => $entity,
            'fieldValues' => $fieldValues,
            'delete_form' => $deleteForm->createView()
        );
    }

    /**
     * Displays a form to edit an existing Record entity.
     *
     * @Route("/{id}/edit", name="record_edit")
     * @Method("GET")
     * @Template()
     * @Security("has_role('ROLE_EDITOR')")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CrossingBordersXBundle:Record')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Record entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Record entity.
    *
    * @param Record $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Record $entity)
    {
        $form = $this->createForm(new RecordType($entity, $em = $this->getDoctrine()->getManager()), $entity, array(
            'action' => $this->generateUrl('record_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));
        $form->add('submit', 'submit', array('label' => $this->get('translator')->trans('Update')));
        return $form;
    }
    /**
     * Edits an existing Record entity.
     *
     * @Route("/{id}", name="record_update")
     * @Method({"PUT"})
     * @Template("CrossingBordersXBundle:Record:edit.html.twig")
     * @Security("has_role('ROLE_EDITOR')")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        $entity = $em->getRepository('CrossingBordersXBundle:Record')->find($id);
        
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Record entity.');
        }
        
        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);
        
        if ($editForm->isValid()) {
            $this->setCommaDelimitedTags($entity, $editForm->get('tags')->getData());
            $em->flush();
            return $this->redirect($this->generateUrl('record_edit', array('id' => $id)));
        }
        
        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Record entity.
     *
     * @Route("/{id}", name="record_delete")
     * @Method("DELETE")
     * @Security("has_role('ROLE_EDITOR')")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CrossingBordersXBundle:Record')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Record entity.');
            }
            //Remove comments
            $thread = $em->getRepository('CrossingBordersXBundle:Thread')->find('record'.$id);
            $em->remove($thread);
            //Remove record
            $em->remove($entity);
            //Flush/Commit
            $em->flush();
        }
        return $this->redirect($this->generateUrl('record'));
    }

    /**
     * Creates a form to delete a Record entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('record_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => $this->get('translator')->trans('Delete')))
            ->getForm()
        ;
    }
    private function setCommaDelimitedTags($record, $commaDelimitedTags) {
        if($commaDelimitedTags !== null) {
            $em = $this->getDoctrine()->getManager();
            $individualTags = array_unique(array_map('trim', explode(",", $commaDelimitedTags)));
            foreach ($individualTags as $key => $individualTag) {
                $foundTag = $em->getRepository('CrossingBordersXBundle:Tag')->findOneByName($individualTag);
                if($foundTag) {
                   $tag = $foundTag; 
                } else {
                    $tag = new Tag();
                    $tag->setName($individualTag);
                    $em->persist($tag);
                }
                //Check if the tag is already associated with the record
                $filteredRecordTags = $record->getRecordTags()->filter(
                                                                        function($entry) use ($individualTag) {
                                                                           return $entry->getTag()->getName() === $individualTag;
                                                                        }
                                                                      );
                if($filteredRecordTags->isEmpty()) {
                    $recordTag = new RecordTag();
                } else {
                    $recordTag = $filteredRecordTags->first();
                }
                $recordTag->setTag($tag);
                $recordTag->setDisplayOrder($key);
                $record->addRecordTag($recordTag);
            }
        }
        //Deal with removed tags
        foreach ($record->getRecordTags() as $currentRecordTag) {
            if(!in_array($currentRecordTag->getTag()->getName(), $individualTags)) {
                $record->removeRecordTag($currentRecordTag);
            }
        }
    }
}