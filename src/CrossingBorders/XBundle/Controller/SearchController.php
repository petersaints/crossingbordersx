<?php
namespace CrossingBorders\XBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use CrossingBorders\XBundle\Form\RecordWizardType;
use CrossingBorders\XBundle\Form\SearchSimpleType;
use CrossingBorders\XBundle\Form\SearchAdvancedType;
use CrossingBorders\XBundle\Form\ShownFieldsType;
use CrossingBorders\XBundle\Utilities\SearchUtilities;
/**
 * Search controller.
 * 
 * @Route("/search")
 */
class SearchController extends Controller {
    
    //--- Simple Search ---
    /**
     * Lists all Record entities.
     *
     * @Route("/simple", name="search_simple")
     * @Method("GET")
     * @Template()
     */
    public function simpleAction() {
        return array(
            'form' => $this->createSimpleSearchForm()->createView()
        );
    }
    /**
     * Lists all Record entities.
     *
     * @Route("/simple/results/page/{page}", name="search_simple_results", requirements={"page" = "\d+"}, defaults={"page" = 1})
     * @Method("GET")
     * @Template()
     */
    public function simpleResultsAction($page, Request $request) {
        $form = $this->createSimpleSearchForm();
        $form->handleRequest($request);
        
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction();
            try {
                $query = SearchUtilities::simpleSearchBuildQuery($form, $em);
                $paginator  = $this->get('knp_paginator');
                $pagination = $paginator->paginate(
                    $query,
                    $page,
                    $this->container->hasParameter('recordtype.recordtypes_per_page')?
                    $this->container->getParameter('recordtype.recordtypes_per_page'):20,
                    array('distinct' => true)
                );
                $em->getConnection()->commit();
                return $this->render('CrossingBordersXBundle:Record:index.html.twig',
                                        array(
                                              'pagination' => $pagination,
                                              'count' => $pagination->getTotalItemCount()
                                        )
                                    );
            } catch (Exception $e) {
                $em->getConnection()->rollback();
                throw $e;
            }
        }
        return $this->render('CrossingBordersXBundle:Search:simple.html.twig',
                                array(
                                        'form' => $form->createView()
                                )
                            );
    }
    //--- Advanced Search ---
    /**
     * @Route("/advanced/wizard", name="search_advanced_wizard")
     * @Method("GET")
     * @Template()
     */
    public function advancedWizardAction() {
        $form = $this->createForm(new RecordWizardType(), null, array(
            'action' => $this->generateUrl('search_advanced_wizard_post'),
            'method' => 'POST',
        ));        
        return array('form'   => $form->createView());
    }
    /**
     * @Route("/advanced/wizard", name="search_advanced_wizard_post")
     * @Method("POST")
     * @Template()
     */
    public function advancedWizardPostAction(Request $request) {
        $formWizard = $this->createForm(new RecordWizardType());
        $formWizard->handleRequest($request);
        $recordTypeId = $formWizard->get('recordType')->getData()->getId();
        
        $form = $this->createForm(new RecordWizardType(), null, array(
            'action' => $this->generateUrl('record_create', array('recordTypeId' => $recordTypeId)),
            'method' => 'POST',
        ));
        $form->handleRequest($request);
        if ($form->isValid()) {
            return $this->redirect($this->generateUrl('search_advanced', array('recordTypeId' => $recordTypeId)));
        }
        return array('form' => $form->createView());        
    }
    /**
     * @Route("/advanced/recordtype/{recordTypeId}", requirements={"recordTypeId" = "\d+"}, name="search_advanced")
     * @Method("GET")
     * @Template()
     */
    public function advancedAction($recordTypeId)
    {
        $em = $this->getDoctrine()->getManager();
        $recordType = $em->getRepository('CrossingBordersXBundle:RecordType')->find($recordTypeId);
        if(is_null($recordType)) {
            throw $this->createNotFoundException("The specified record type was not found");
        }
        $form   = $this->createAdvancedSearchForm($recordType);
        return array(
            'recordType' => $recordType,
            'form'   => $form->createView(),
        );
    }
    /**
     * Lists all Record entities.
     *
     * @Route("/advanced/recordtype/{recordTypeId}/results/page/{page}", name="search_advanced_results", requirements={"recordTypeId" = "\d+", "page" = "\d+"}, defaults={"page" = 1})
     * @Method("GET")
     * @Template()
     */
    public function advancedResultsAction($recordTypeId, $page, Request $request) {
        $em = $this->getDoctrine()->getManager();
        $em->getConnection()->beginTransaction();
        try {
            $recordType = $em->getRepository('CrossingBordersXBundle:RecordType')->find($recordTypeId);
            if(is_null($recordType)) {
                throw $this->createNotFoundException("The specified record type was not found");
            }
            $form = $this->createAdvancedSearchForm($recordType);
            $form->handleRequest($request);
            if ($form->isValid()) {
                //Advanced search logic implemented in this function
                $query = SearchUtilities::advancedSearchBuildQuery($form, $recordType, $em);
                $paginator  = $this->get('knp_paginator');
                $pagination = $paginator->paginate(
                    $query,
                    $page,
                    $this->container->hasParameter('recordtype.recordtypes_per_page')?
                    $this->container->getParameter('recordtype.recordtypes_per_page'):20,
                    array('distinct' => true)
                );
                $em->getConnection()->commit();
                $shownFields = [];
                if($form->has('shownFields')) {
                    $shownFields = ShownFieldsType::convertToArray($form->get('shownFields'));
                }
                return $this->render('CrossingBordersXBundle:Search:advancedResults.html.twig',
                                        array(
                                                'pagination' => $pagination,
                                                'count' => $pagination->getTotalItemCount(),
                                                'shownFields' => $shownFields
                                             )
                                    );
            }
        } catch (Exception $e) {
            $em->getConnection()->rollback();
            throw $e;
        }
        return $this->render('CrossingBordersXBundle:Search:advanced.html.twig',
                                array(
                                        'recordType' => $recordType,
                                        'form' => $form->createView()
                                )
                            );
    }
    private function createSimpleSearchForm() {
        $form = $this->createForm(new SearchSimpleType(), null, array(
            'action' => $this->generateUrl('search_simple_results'),
            'method' => 'GET'
        ));
        $form->add('submit', 'submit', array('label' => $this->get('translator')->trans('Search')));
        return $form;
    } 
    private function createAdvancedSearchForm($recordType) {
        $form = $this->createForm(new SearchAdvancedType($recordType, $this->getDoctrine()->getManager()), null, array(
            'action' => $this->generateUrl('search_advanced_results', array('recordTypeId' => $recordType->getId())),
            'method' => 'GET'
        ));
        $form->add('submit', 'submit', array('label' => $this->get('translator')->trans('Search')));
        return $form;
    }
}